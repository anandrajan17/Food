package com.createfirst.company.foodpals;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shilpa on 02-06-2018.
 */

public class GoogleNextPage implements Parcelable{

    protected GoogleNextPage(Parcel in) {
        next_page_token = in.readString();
    }

    public static final Creator<GoogleNextPage> CREATOR = new Creator<GoogleNextPage>() {
        @Override
        public GoogleNextPage createFromParcel(Parcel in) {
            return new GoogleNextPage(in);
        }

        @Override
        public GoogleNextPage[] newArray(int size) {
            return new GoogleNextPage[size];
        }
    };

    public GoogleNextPage() {

    }

    public String getNext_page_token() {
        return next_page_token;
    }

    public void setNext_page_token(String next_page_token) {
        this.next_page_token = next_page_token;
    }

    /*public String getStatus() {
        return status;
    }*/

//    public void setStatus(String status) {
//        this.status = status;
//    }

    //String status;
    String next_page_token;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       // dest.writeString(status);
        dest.writeString(next_page_token);
    }
}
