package com.createfirst.company.foodpals;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;

import java.util.Arrays;


public class fb_log extends AppCompatActivity {
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mdatabase;
    public String user_id;
    public String email_id;
    Profile profile;
    String name;
    private static final String TAG = "fb_log";
   FirebaseUser current_user;
    // FirebaseUser email_id;

    private Button mFacebook_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(com.createfirst.company.foodpals.R.layout.activity_fb_log);
        mAuth = FirebaseAuth.getInstance();
        mFacebook_btn = (Button) findViewById(com.createfirst.company.foodpals.R.id.facebook_btn) ;
        /*current_user = FirebaseAuth.getInstance().getCurrentUser();*/
        //email_id = FirebaseAuth.getInstance().getCurrentUser();

        new NoInternet().check_internet(fb_log.this);


        callbackManager = CallbackManager.Factory.create();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldToken, AccessToken currentToken) {

            }
        };

        /*profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

               // if(current_user != null) {

                //current_user = FirebaseAuth.getInstance().getCurrentUser();
                    nextActivity(newProfile);

              //  }else{


           // }
            }
        };*/

        accessTokenTracker.startTracking();



        mFacebook_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                new NoInternet().check_internet(fb_log.this);



//                NoInternet no_internet = new NoInternet(fb_log.this);
//                no_internet.show();

                LoginManager.getInstance().logInWithReadPermissions(fb_log.this, Arrays.asList("email", "public_profile"));
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        Log.d(TAG, "facebook:onSuccess:" + loginResult);
                        handleFacebookAccessToken(loginResult.getAccessToken());


                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "facebook:onCancel");
                    }

                    @Override
                    public void onError(FacebookException error) {

                        Log.d(TAG, "facebook:onerror:" + error);

                    }
                });
            }
        });



     try {
         mAuthListener = new FirebaseAuth.AuthStateListener() {


             @Override
             public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                 FirebaseUser currentUser = firebaseAuth.getCurrentUser();

                 if (currentUser != null) {
                     name = currentUser.getDisplayName();
                     Toast.makeText(fb_log.this, " first toast", Toast.LENGTH_LONG).show();
                 } else {
                     Toast.makeText(fb_log.this, "something went wrong", Toast.LENGTH_LONG).show();
                 }

             }
         };
     }catch (NullPointerException e){
         e.printStackTrace();
     }
    }





    private void handleFacebookAccessToken(AccessToken Token) {
        Log.d(TAG, "handleFacebookAccessToken:" + Token);

      try {
          AuthCredential authcredential = FacebookAuthProvider.getCredential(Token.getToken());
          mAuth.signInWithCredential(authcredential).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
              @Override
              public void onComplete(@NonNull Task<AuthResult> task) {
                  if (task.isSuccessful()) {

                      FirebaseUser current_user = mAuth.getCurrentUser();
                      profile = Profile.getCurrentProfile();
                      //FirebaseUser current_user = FirebaseAuth.().getCurrentUser();

                      if (profile != null && current_user != null) {
                          nextActivity(profile, current_user);

                      } else {
                          profileTracker = new ProfileTracker() {
                              @Override
                              protected void onCurrentProfileChanged(Profile oldProfile, Profile newProfile) {

                                  Log.v("facebook - profile", newProfile.getFirstName());
                                  profileTracker.stopTracking();

                              }
                          };
                      }

                      Log.d(TAG, "SignInWithCredential:onComplete:" + task.isSuccessful());
                  } else {
                      Log.w(TAG, "signInWithCredential:failure", task.getException());
                      Toast.makeText(fb_log.this, "Authentication failed.",
                              Toast.LENGTH_SHORT).show();
                  }

              }
          });
      } catch(NullPointerException e){
          e.printStackTrace();
      }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //facebook Login
        // profile = Profile.getCurrentProfile();
       // nextActivity(profile, current_user);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    protected void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
        // profileTracker.stopTracking();
    }

    @Override
    public void onStart() {
        super.onStart();
       FirebaseUser current_user = mAuth.getCurrentUser();
        profile = Profile.getCurrentProfile();
        if( profile != null && current_user != null){
            nextActivity(profile, current_user);
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent data) {
        super.onActivityResult(requestCode, responseCode, data);
        callbackManager.onActivityResult(requestCode, responseCode, data);
    }


    private void nextActivity(Profile profile, FirebaseUser current_user) {

        try {
            if (profile != null && current_user != null) {
                user_id = current_user.getUid().toString();
                email_id = current_user.getEmail().toString();
                Intent main = new Intent(fb_log.this, MainActivity.class);
                main.putExtra("name_fb", profile.getFirstName());
                main.putExtra("surname", profile.getLastName());
                main.putExtra("middlename", profile.getMiddleName());
                main.putExtra("userid", profile.getId());
                main.putExtra("imageUrl", profile.getProfilePictureUri(200, 200).toString());
                main.putExtra("facebook_uri", profile.getLinkUri());
                main.putExtra("Firebase_User_id", user_id);
                main.putExtra("Email_id", email_id);
                main.putExtra("Full_name", profile.getName());
                startActivity(main);
            } else {
                Toast.makeText(fb_log.this, email_id, Toast.LENGTH_SHORT).show();
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


}


