package com.createfirst.company.foodpals;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.createfirst.company.foodpals.mRecycler.CustomFilter;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Shilpa on 25-03-2018.
 */

public class ZomatoResAdapter extends RecyclerView.Adapter<ZomatoResAdapter.MyViewHolder>/* implements Filterable*/ {
    private Context context;
    CustomFilter filter1;
    public List<ZomatoParameter> albumList;
    private DatabaseReference dCurrent_user;
    private FirebaseAuth mAuth;
    public double resLat, resLong;

    public String name_fb;
    public  String surname ;
    public  String imageUrl ;
    public  String middlename ;
    public  String Facebook_id ;
    public  String Firebase_user_id ;
    public  String Facebook_uri ;
    public  String user_email_id ;
    public  String Full_name ;



    private ShimmerFrameLayout mShimmerViewContainer;




    private static final String LOG_TAG = "ZomatoResAdapter";


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName1,mAdressTextView1,mcuisines,mMoney1;
        public CardView res_cardview;
        public Button mRating,mHost;
        public ImageView resImageView, thumbnail, overflow;

        private DatabaseReference mdatabase;
        private ShimmerFrameLayout resimageshimmer;


        public MyViewHolder(View view) {
            super(view);
            /*title = (TextView) view.findViewById(R.id.title);
            count = (TextView) view.findViewById(R.id.count);
            thumbnail = (ImageView) view.findViewById(R.id.thumbnail);
            overflow = (ImageView) view.findViewById(R.id.overflow);*/
             res_cardview = (CardView) view.findViewById(com.createfirst.company.foodpals.R.id.z_card_view);
            resImageView = (ImageView) view.findViewById(com.createfirst.company.foodpals.R.id.z_resturant_imageview);
            txtName1 =(TextView) view.findViewById(com.createfirst.company.foodpals.R.id.z_resturant_name);

           mAdressTextView1 = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.z_locality);
           mcuisines = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.z_cuisines);
           mRating = (Button) view.findViewById(com.createfirst.company.foodpals.R.id.z_rating);
          mMoney1 = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.z_avg_price);
          mHost = (Button) view.findViewById(com.createfirst.company.foodpals.R.id.z_host_button);

            resimageshimmer = view.findViewById(com.createfirst.company.foodpals.R.id.z_shimmer_image);

          // mShimmerViewContainer = view.findViewById(R.id.resturantcard_shimmer);

            //mShimmerViewContainer.startShimmerAnimation();


        }
    }




    public ZomatoResAdapter(Context context, List<ZomatoParameter> albumList) {
        this.context = context;
        this.albumList = albumList;
        Log.i("My Data", String.format("ZomatoResAdapter'%s'", albumList));

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.createfirst.company.foodpals.R.layout.card_zomato, parent, false);
        return new MyViewHolder(itemView);
    }

   /* @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }*/

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final ZomatoParameter testing = albumList.get(position);
        /*holder.title.setText(album.getName());
        holder.count.setText(album.getNumOfSongs() + " songs");*/
        Log.i("Zomato restaurant list", String.format("ZomatoResAdapterProduct'%s'", testing));



        if (testing.getFeatured_image().isEmpty()) {

            holder.resImageView.setImageResource(com.createfirst.company.foodpals.R.drawable.default_resturant_icon);
//
//            Picasso.with(context).load(testing.getFeatured_image())
//                    .placeholder(R.drawable.default_resturant_icon);
        } else{
            Picasso.with(context).load(testing.getFeatured_image( ))
                    .placeholder(com.createfirst.company.foodpals.R.drawable.default_resturant_icon)
                    .fit().centerCrop()
                    .into(holder.resImageView);
        }

//        Picasso.with(context).load(testing.getFeatured_image())
//                .placeholder(R.drawable.default_resturant_icon)
//
//                .fit().centerCrop()
//                .into(holder.resImageView);





        holder.txtName1.setText(testing.getName());

//        Log.i("Cusines", String.format("ZomatoResAdapterName'%s'", testing.getName()));
//
        holder.mAdressTextView1.setText(testing.getLocality().toString());
       Log.i("locality", String.format("ZomatoResAdapterName'%s'", testing.getLocality()));
        holder.mcuisines.setText(testing.getCuisines().toString());
        if (testing.getAggregate_rating() != null) {
            holder.mRating.setText(testing.getAggregate_rating().toString());
            //holder.mRating.setText(testing.getAggregate_rating().toString());
        } else {
            holder.mRating.setText(0 + "/5");
        }
//
//
        holder.mMoney1.setText("₹ "+testing.getAverage_cost_for_two() + " for "+ " two people ");

        if(testing.getLat()!=null){
            resLat = testing.getLat();
        }
        if(testing.getLng()!=null){
            resLong = testing.getLng();
        }

       // holder.mShimmerViewContainer.stopShimmerAnimation();


        //Code for putting price level to show $ sign


//        if (products.getAverage_cost_for_two() != 0) {
//            for (int i = 0; i < products.getPrice_level(); i++) {
//                holder.mMoney.append("₹" + "  " + products.getPrice_level() + "  " + "FOR" + "  " + "TWO");
//            }
//        } else {
//            holder.mMoney.setText("₹" + "  " + products.getPrice_level() + "  " + "FOR" + "  " + "TWO");
//        }





//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            StateListAnimator stateListAnimator = AnimatorInflater
//                    .loadStateListAnimator(context, R.animator.lift_on_touch);
//            holder.res_cardview.setStateListAnimator(stateListAnimator);
//        }





        holder.mHost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                //Animation code need to fix later
                Activity mContext = (Activity) holder.itemView.getContext();
                holder.mHost.setAnimation(AnimationUtils.loadAnimation(mContext, com.createfirst.company.foodpals.R.anim.card_zoom_in));

                Bundle bundle = new Bundle();

                String RestaurantName = holder.txtName1.getText().toString();
                String ResImage = testing.getFeatured_image();
                String LocName = holder.mAdressTextView1.getText().toString();
                String ResCuisines = holder.mcuisines.getText().toString();
                String Ratings = holder.mRating.getText().toString();
                String Money = holder.mMoney1.getText().toString();
                bundle.putString("name", RestaurantName);
                bundle.putString("imageRes", ResImage);
                bundle.putString("locationname", LocName);
                bundle.putString("resCuisines", ResCuisines);
                bundle.putString("ratings", Ratings);
                bundle.putString("money", Money);

                resturant_popup(bundle);
            }


            public void resturant_popup(Bundle bundle) {
                TextView txtclose;
                Button resturant_host;
                TextView resturant_name;

                TextView resturant_address_new;
                TextView resturant_;
                Button resturant_rating;
                TextView resturant_cost_for_two;
                TextView resturant_cusine_type;
                final ImageView resturant_image;



                final String RestName = bundle.getString("name");
                final String RestRating = bundle.getString("ratings");
                final String RestAddress = bundle.getString("locationname");
                //String RestOpennow = bundle.getString("openNow");
                final String RestImage = bundle.getString("imageRes");
                final String Restpricerange = bundle.getString("money");
                final String RestCusines = bundle.getString("resCuisines");

                mAuth = FirebaseAuth.getInstance();
                // current_online_user_id  = FirebaseAuth.getInstance().getCurrentUser();
                String mcurrent_online_user_id = mAuth.getCurrentUser().getUid();
                dCurrent_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mcurrent_online_user_id);



                final Dialog myDialog = new Dialog(ZomatoResAdapter.this.context);

                //Initializing the Pop up codes

                myDialog.setContentView(com.createfirst.company.foodpals.R.layout.popup_zomato);
                txtclose =(TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.txtclose);
                resturant_host = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_host);
                resturant_name =(TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_name);
                resturant_rating =(Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_rating);
                resturant_image = (ImageView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_image);
                resturant_address_new  = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_address);
                resturant_cusine_type = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cusine_type);
                resturant_cost_for_two = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cost_for_two);

                dCurrent_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        name_fb = dataSnapshot.child("First_Name").getValue().toString();
                        surname = dataSnapshot.child("Last_name").getValue().toString();
                        imageUrl = dataSnapshot.child("Profile_image").getValue().toString();
                        middlename = dataSnapshot.child("Middle_Name").getValue().toString();
                        Facebook_id = dataSnapshot.child("Facebook_id").getValue().toString();
                        Firebase_user_id = dataSnapshot.child("Firebase_id").getValue().toString();
                        user_email_id = dataSnapshot.child("Email_id").getValue().toString();
                        Facebook_uri = dataSnapshot.child("Facebook_uri").getValue().toString();



                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



                resturant_host.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent hosting_page = new Intent(ZomatoResAdapter.this.context, hostinf_resturant.class);

                        //User Details
                        hosting_page.putExtra("surname", surname);
                        hosting_page.putExtra("imageUrl", imageUrl);
                        hosting_page.putExtra("middlename", middlename);
                        hosting_page.putExtra("Facebook_id", Facebook_id);
                        hosting_page.putExtra("Firebase_user_id", Firebase_user_id);
                        hosting_page.putExtra("Facebook_uri", Facebook_uri);
                        hosting_page.putExtra("user_email_id", user_email_id);
                        hosting_page.putExtra("name_fb", name_fb);

                        // Resturant details
                        hosting_page.putExtra("resturant_image", RestImage);
                        hosting_page.putExtra("resturant_name", RestName);
                        hosting_page.putExtra("resturant_cusine", RestCusines);
                        hosting_page.putExtra("resturant_address_new", RestAddress);
                        hosting_page.putExtra("resturant_pricerange", Restpricerange);
                        hosting_page.putExtra("resturant_rating", RestRating);
                        hosting_page.putExtra("resLat",resLat);
                        hosting_page.putExtra("resLong",resLong);


                        context.startActivity(hosting_page);

                        myDialog.dismiss();

                    }
                });




//Assigning the Data to the Pop up
                resturant_rating.setText(RestRating);
                resturant_name.setText(RestName);
                resturant_address_new.setText(RestAddress);
                resturant_cusine_type.setText(RestCusines);
                resturant_cost_for_two.setText(Restpricerange);

             try {
                 //Setting the image
                 Picasso.with(ZomatoResAdapter.this.context).load(RestImage)
                         .fit()
                         .centerCrop()
                         .into(resturant_image);
             }catch (IllegalArgumentException e){
                 e.printStackTrace();
             }

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
                myDialog.show();
            }
        });


        holder.res_cardview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Animation code need to fix later
                Activity mContext = (Activity) holder.itemView.getContext();
                holder.mHost.setAnimation(AnimationUtils.loadAnimation(mContext, com.createfirst.company.foodpals.R.anim.card_zoom_in));

                Bundle bundle = new Bundle();

                String RestaurantName = holder.txtName1.getText().toString();
                String ResImage = testing.getFeatured_image();
                String LocName = holder.mAdressTextView1.getText().toString();
                String ResCuisines = holder.mcuisines.getText().toString();
                String Ratings = holder.mRating.getText().toString();
                String Money = holder.mMoney1.getText().toString();
                bundle.putString("name", RestaurantName);
                bundle.putString("imageRes", ResImage);
                bundle.putString("locationname", LocName);
                bundle.putString("resCuisines", ResCuisines);
                bundle.putString("ratings", Ratings);
                bundle.putString("money", Money);

                resturant_popup(bundle);
            }


            public void resturant_popup(Bundle bundle) {
                TextView txtclose;
                Button resturant_host;
                TextView resturant_name;

                TextView resturant_address_new;
                TextView resturant_;
                Button resturant_rating;
                TextView resturant_cost_for_two;
                TextView resturant_cusine_type;
                final ImageView resturant_image;



                final String RestName = bundle.getString("name");
                final String RestRating = bundle.getString("ratings");
                final String RestAddress = bundle.getString("locationname");
                //String RestOpennow = bundle.getString("openNow");
                final String RestImage = bundle.getString("imageRes");
                final String Restpricerange = bundle.getString("money");
                final String RestCusines = bundle.getString("resCuisines");

                mAuth = FirebaseAuth.getInstance();
                // current_online_user_id  = FirebaseAuth.getInstance().getCurrentUser();
                String mcurrent_online_user_id = mAuth.getCurrentUser().getUid();
                dCurrent_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mcurrent_online_user_id);



                final Dialog myDialog = new Dialog(ZomatoResAdapter.this.context);

                //Initializing the Pop up codes

                myDialog.setContentView(com.createfirst.company.foodpals.R.layout.popup_zomato);
                txtclose =(TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.txtclose);
                resturant_host = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_host);
                resturant_name =(TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_name);
                resturant_rating =(Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_rating);
                resturant_image = (ImageView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_image);
                resturant_address_new  = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_address);
                resturant_cusine_type = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cusine_type);
                resturant_cost_for_two = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cost_for_two);

                dCurrent_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        name_fb = dataSnapshot.child("First_Name").getValue().toString();
                        surname = dataSnapshot.child("Last_name").getValue().toString();
                        imageUrl = dataSnapshot.child("Profile_image").getValue().toString();
                        middlename = dataSnapshot.child("Middle_Name").getValue().toString();
                        Facebook_id = dataSnapshot.child("Facebook_id").getValue().toString();
                        Firebase_user_id = dataSnapshot.child("Firebase_id").getValue().toString();
                        user_email_id = dataSnapshot.child("Email_id").getValue().toString();
                        Facebook_uri = dataSnapshot.child("Facebook_uri").getValue().toString();



                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



                resturant_host.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent hosting_page = new Intent(ZomatoResAdapter.this.context, hostinf_resturant.class);

                        //User Details
                        hosting_page.putExtra("surname", surname);
                        hosting_page.putExtra("imageUrl", imageUrl);
                        hosting_page.putExtra("middlename", middlename);
                        hosting_page.putExtra("Facebook_id", Facebook_id);
                        hosting_page.putExtra("Firebase_user_id", Firebase_user_id);
                        hosting_page.putExtra("Facebook_uri", Facebook_uri);
                        hosting_page.putExtra("user_email_id", user_email_id);
                        hosting_page.putExtra("name_fb", name_fb);

                        // Resturant details
                        hosting_page.putExtra("resturant_image", RestImage);
                        hosting_page.putExtra("resturant_name", RestName);
                        hosting_page.putExtra("resturant_cusine", RestCusines);
                        hosting_page.putExtra("resturant_address_new", RestAddress);
                        hosting_page.putExtra("resturant_pricerange", Restpricerange);
                        hosting_page.putExtra("resturant_rating", RestRating);
                        hosting_page.putExtra("resLat",resLat);
                        hosting_page.putExtra("resLong",resLong);


                        context.startActivity(hosting_page);

                        myDialog.dismiss();

                    }
                });




//Assigning the Data to the Pop up
                resturant_rating.setText(RestRating);
                resturant_name.setText(RestName);
                resturant_address_new.setText(RestAddress);
                resturant_cusine_type.setText(RestCusines);
                resturant_cost_for_two.setText(Restpricerange);

                try {
                    //Setting the image
                    Picasso.with(ZomatoResAdapter.this.context).load(RestImage)
                            .fit()
                            .centerCrop()
                            .into(resturant_image);
                }catch (IllegalArgumentException e){
                    e.printStackTrace();
                }

                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
                myDialog.show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return albumList.size();
    }
    //RETURN FILTER OBJ


    /*  @Override
    public Filter getFilter() {
        if(filter1==null)
        {
            filter1=new CustomFilter(albumList1,this);
        }

        return filter1;
    }*/
}


