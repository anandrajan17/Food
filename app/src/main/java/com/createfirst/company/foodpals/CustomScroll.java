package com.createfirst.company.foodpals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by Shilpa on 14-05-2017.
 */

public class CustomScroll extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    setContentView(com.createfirst.company.foodpals.R.layout.content_scrollview);

        Intent intent = getIntent();

        /*Bitmap bitmap = (Bitmap) intent.getParcelableExtra("picture");*/

        String ResImage = intent.getStringExtra("imageRes");
        String RestName = intent.getStringExtra("name");
        String LocName = intent.getStringExtra("locationname");
       String OpenNow = intent.getStringExtra("openNow");
        String Ratings = intent.getStringExtra("ratings");
        String Money = intent.getStringExtra("money");


        ImageView image = (ImageView) findViewById(com.createfirst.company.foodpals.R.id.imageViewRes);
        //image.setImageBitmap(bitmap);

        Picasso.with(this).load(ResImage)
                .fit()
                .centerCrop()
                .into(image);

        TextView name_scroll = (TextView)findViewById(com.createfirst.company.foodpals.R.id.txtNameRes);
        name_scroll.setText(RestName);

        TextView Loc_scroll = (TextView)findViewById(com.createfirst.company.foodpals.R.id.txtLocationRes);
        Loc_scroll.setText(LocName);

        TextView open_now = (TextView)findViewById(com.createfirst.company.foodpals.R.id.txtOpenNowRes);
        open_now.setText(OpenNow);

        TextView rating_res = (TextView)findViewById(com.createfirst.company.foodpals.R.id.rating1Res);
        rating_res.setText(Ratings);

        TextView money_res = (TextView)findViewById(com.createfirst.company.foodpals.R.id.price_levelRes);
        money_res.setText(Money);

}
}