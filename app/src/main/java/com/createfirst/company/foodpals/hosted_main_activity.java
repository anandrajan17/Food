package com.createfirst.company.foodpals;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ServerValue;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.view.View.VISIBLE;


/**
 * Created by anandhaa on 26/08/17.
 */
public class hosted_main_activity extends AppCompatActivity implements req_reciever_adapter.OnClickInAdapter, req_reciever_adapter.onClickReject {

    //User for Firebases
    public String name_fb;
    public String surname;
    public String imageUrl;
    public String middlename;
    public String user_facebook_id;
    public String event_creator_id;
    public String user_facebook_uri;
    public String user_email_id;
    public String data_Venue;
    public String data_additional_detail;
    public String data_date;
    public String data_time;
    public String data_resturant_name;
    public String data_resturant_address;
    public String data_gender;


    //adapter
    private req_reciever_adapter new_adapter;


    private TextView mProfilefirstname, mProfilesurname, mProfilemiddlename, mHosted_time, mhosted_date, mHosted_venue, mHosted_info, mHosted_request_count;
    private CircleImageView mProfileImage;
    private Button manage;

    private ProgressDialog mProgressDialogue;

    private String mCurrent_State;
    private FirebaseUser mCurrent_user;


    private DatabaseReference mFriendsReqDatabase;
    private DatabaseReference mFriendDatabase;
    private DatabaseReference req_userDB;
    private DatabaseReference mUserDatabase;
    private DatabaseReference mUsersDatabase;
    private DatabaseReference mSenderDatabase;
    private DatabaseReference mNotificationDatabase;
    private DatabaseReference mRootref;
    private DatabaseReference mcurrent_user_online;
    private DatabaseReference mfriend_user_online;
    private FirebaseAuth mAuth;


    public String event_id;
    public String current_user_id;
    public String sender_user_id;
    public String sent_user_id;


    public View mview;


    String first_username;


    private FloatingActionButton share_button;
    private FloatingActionButton mProfileSendReqBtn;
    private FloatingActionButton getmProfileDeclineReqBtn;
    private FloatingActionButton mChat;

    //Array List for Name and profile pic adn user id
    private ArrayList<String> friend_mNames = new ArrayList<>();
    private ArrayList<String> friend_mProfilepic = new ArrayList<>();

    //String for storing the image values
    private String friend_name;
    private String friend_image;
    private String friend_user_id;

    //Array List for Req layout
    private ArrayList<String> req_mNames = new ArrayList<>();
    private ArrayList<String> req_mProfilepic = new ArrayList<>();
    private ArrayList<String> req_mUser_id = new ArrayList<>();


    //To get name and profile picture for Req layout

    private String req_profile_image;
    private String req_user_name;
    private String req_user_id;


    private static final String TAG = "hosted_main_activity";

    //Code to Remove and show Manage
    private LinearLayout layoutToAdd;
    private LinearLayout layoutToremove;
    private LinearLayout roaster_layout_hide;


    Button req_accept;

    View inflating_recycler;
    LayoutInflater inflater;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.event_hosted_mainpage);


        event_id = getIntent().getStringExtra("event_id");
        mAuth = FirebaseAuth.getInstance();

        Toolbar mChatToolbar = findViewById(com.createfirst.company.foodpals.R.id.event_toolbar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mChatToolbar.setTitleTextColor(Color.WHITE);


        //Back arrow colour
        Drawable backArrow = getResources().getDrawable(com.createfirst.company.foodpals.R.drawable.ic_arrow_back_black_24dp);
        backArrow.setColorFilter(getResources().getColor(com.createfirst.company.foodpals.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);


        //Floating button
        share_button = findViewById(com.createfirst.company.foodpals.R.id.hosted_share);
        mProfileSendReqBtn = findViewById(com.createfirst.company.foodpals.R.id.hosted_frd_req_send);
        getmProfileDeclineReqBtn = findViewById(com.createfirst.company.foodpals.R.id.hosted_frd_req_decline);
        mChat = findViewById(com.createfirst.company.foodpals.R.id.chat);


        //code to call native share
        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

            }
        });


        //Code to change the layout for the current user
        manage = findViewById(com.createfirst.company.foodpals.R.id.hosted_event_manage);


        layoutToremove = findViewById(com.createfirst.company.foodpals.R.id.replacing_inflator);
        layoutToAdd = findViewById(com.createfirst.company.foodpals.R.id.changing_layout);
        roaster_layout_hide = findViewById(com.createfirst.company.foodpals.R.id.roaster_layout);


        //Initializing Inflator code
        inflater = LayoutInflater
                .from(getApplicationContext());

        inflating_recycler = inflater.inflate(com.createfirst.company.foodpals.R.layout.req_reciever_recycler_view, null);
        layoutToremove.addView(inflating_recycler);
        layoutToremove.setVisibility(View.GONE);


        //Manage button to Inflate Layout of the people who have sent a req

        manage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                LayoutInflater inflater = LayoutInflater
//                        .from(getApplicationContext());
//                View view = inflater.inflate(R.layout.req_reciever_recycler_view, null);
//                layoutToAdd.setVisibility(View.GONE);
//                layoutToremove.addView(view);
//
//                req_get_sender_details();

                layoutchange();


            }
        });


// Data base pointing
        mRootref = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Event").child(event_id);
        mFriendsReqDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Friend_req");
        mFriendDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Friend");
        mSenderDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Sender");
        mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("notifications");
        mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
        mfriend_user_online = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User");
        req_userDB = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Request_user");

        //   mcurrent_user_online = FirebaseDatabase.getInstance().getReference().child("User").child(user_id);

        //Declaring the ids

        current_user_id = mCurrent_user.getUid().toString();


        //Declaring buttons and text fields

        mProfilefirstname = findViewById(com.createfirst.company.foodpals.R.id.hosted_username);
        mProfileImage = findViewById(com.createfirst.company.foodpals.R.id.hosted_profile_image);
        mHosted_time = findViewById(com.createfirst.company.foodpals.R.id.hosted_time);
        mhosted_date = findViewById(com.createfirst.company.foodpals.R.id.hosted_date);
        mHosted_venue = findViewById(com.createfirst.company.foodpals.R.id.hosted_venue);
        mHosted_info = findViewById(com.createfirst.company.foodpals.R.id.hosted_info);
        mHosted_request_count = findViewById(com.createfirst.company.foodpals.R.id.hosted_request_count);


        //Hiding the decline button
        getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
        getmProfileDeclineReqBtn.setEnabled(false);


        //First state
        mCurrent_State = "not_friends";


        //Loading icon
        mProgressDialogue = new ProgressDialog(this);
        mProgressDialogue.setTitle("Loading User Data");
        mProgressDialogue.setMessage("Please wait While we Load the Event");
        mProgressDialogue.setCanceledOnTouchOutside(false);
        mProgressDialogue.show();


        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                try {
                    name_fb = dataSnapshot.child("name_fb").getValue().toString();
                    surname = dataSnapshot.child("surname").getValue().toString();
                    imageUrl = dataSnapshot.child("imageUrl").getValue().toString();
                    middlename = dataSnapshot.child("middlename").getValue().toString();
                    user_facebook_id = dataSnapshot.child("user_facebook_id").getValue().toString();
                    event_creator_id = dataSnapshot.child("firebase_user_id").getValue().toString();
                    user_facebook_uri = dataSnapshot.child("user_facebook_uri").getValue().toString();
                    user_email_id = dataSnapshot.child("user_email_id").getValue().toString();
                    data_Venue = dataSnapshot.child("data_Venue").getValue().toString();
                    data_additional_detail = dataSnapshot.child("data_additional_detail").getValue().toString();
                    data_date = dataSnapshot.child("data_date").getValue().toString();
                    data_time = dataSnapshot.child("data_time").getValue().toString();
                    data_resturant_name = dataSnapshot.child("data_resturant_name").getValue().toString();
                    data_resturant_address = dataSnapshot.child("data_resturant_address").getValue().toString();
                    data_gender = dataSnapshot.child("data_gender").getValue().toString();

                } catch (NullPointerException e) {
                    e.printStackTrace();
                }


                if (current_user_id.equals(event_creator_id)) {


                    mProfileSendReqBtn.setVisibility(View.INVISIBLE);
                    mProfileSendReqBtn.setEnabled(false);
                    getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                    getmProfileDeclineReqBtn.setEnabled(false);
                    /*mChat.setEnabled(true);
                    mChat.setVisibility(VISIBLE);*/

                } else {
                    roaster_layout_hide.setVisibility(View.GONE);
                }


                mfriend_user_online = FirebaseDatabase.getInstance().getReference().child("User");

                mfriend_user_online.child(event_creator_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        if (dataSnapshot.hasChild("online")) {
                            String userOnline = dataSnapshot.child("online").getValue().toString();
                            setUserOnline(userOnline);

                        } else {

                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                //Setting User Profile name
                mProfilefirstname.setText("" + name_fb + " " + middlename + " " + surname);
                mhosted_date.setText(data_date);
                mHosted_time.setText(data_time);
                mHosted_venue.setText(data_resturant_name);
                mHosted_info.setText(data_additional_detail);

                //setting user profile picture
                Picasso.with(hosted_main_activity.this).load(imageUrl).placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(mProfileImage);


                //calling method to pod
                friends_details();

                mSenderDatabase.child(event_id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        mHosted_request_count.setText("" + dataSnapshot.getChildrenCount() + "Request");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                //-------------------------------- Friends List/Request Feature ---------------------------------------- checks up on oncreate

                mFriendsReqDatabase.child(event_id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.hasChild(current_user_id)) {
                            String request_type = dataSnapshot.child(current_user_id).child("request_type").getValue().toString();

                            if (request_type.equals("received")) {
                                mCurrent_State = "req_received";

                                getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                getmProfileDeclineReqBtn.setEnabled(false);
                                mProfileSendReqBtn.setVisibility(View.INVISIBLE);
                                mProfileSendReqBtn.setEnabled(false);
                               /* mChat.setEnabled(false);
                                mChat.announceForAccessibility("Can't chat till Event host accept's your friend request");
                             */
                                mChat.setVisibility(VISIBLE);


                            } else if (request_type.equals("sent")) {

                                mCurrent_State = "req_sent";


                                getmProfileDeclineReqBtn.setVisibility(VISIBLE);
                                getmProfileDeclineReqBtn.setEnabled(true);
                                /*mChat.setEnabled(false);
                                mChat.announceForAccessibility("Can't chat till Event host accept's your friend request");
                               */
                                mChat.setVisibility(VISIBLE);
                            }
                            mProgressDialogue.dismiss();
                        } else {
                            mFriendDatabase.child(event_id).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.hasChild(current_user_id)) {

                                        mCurrent_State = "friends";

                                        getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                        getmProfileDeclineReqBtn.setEnabled(false);
                                        mProfileSendReqBtn.setVisibility(View.INVISIBLE);
                                        mProfileSendReqBtn.setEnabled(false);
                                        mChat.setEnabled(true);
                                        mChat.setVisibility(VISIBLE);

                                    }
                                    mProgressDialogue.dismiss();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }

                        mProgressDialogue.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        //Friend Req Send button
        mProfileSendReqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProfileSendReqBtn.setEnabled(false);

                // Not Friends State

                if (mCurrent_State.equals("not_friends")) {

                    DatabaseReference newNotficationref = mRootref.child("notifications").child(event_id).push();
                    String newNofticaionID = event_creator_id;
                    HashMap<String, String> notificationData = new HashMap<String, String>();
                    notificationData.put(current_user_id, "from");
//                    notificationData.put("type", "request");

                    Map requestMap = new HashMap();
                    requestMap.put("Friend_req/" + event_id + "/" + current_user_id + "/request_type", "sent");
                    requestMap.put("Friend_req/" + event_id + "/" + event_creator_id + "/request_type", "received");
                    // requestMap.put("Sender/" + event_id+ "/"+ "/Sender", current_user_id );

                    requestMap.put("Sender/" + event_id + "/" + current_user_id, current_user_id);
                    requestMap.put("notifications/" + event_id + "/" + newNofticaionID, notificationData);

                    mRootref.updateChildren(requestMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            mProfileSendReqBtn.setVisibility(View.INVISIBLE);
                            mProfileSendReqBtn.setEnabled(false);
                            mCurrent_State = "req_sent";
                            getmProfileDeclineReqBtn.setVisibility(VISIBLE);
                            getmProfileDeclineReqBtn.setEnabled(true);
                           /* mChat.setEnabled(false);
                            mChat.announceForAccessibility("Can't chat till Event host accept's your friend request");
                           */
                            mChat.setVisibility(VISIBLE);


                            if (databaseError != null) {
                                Toast.makeText(hosted_main_activity.this, "Error sending Request", Toast.LENGTH_SHORT);

                            }

                        }


                    });

                }


// Decline button to delete the entry in Friend Req and sender table
                getmProfileDeclineReqBtn.setOnClickListener(
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // Cancel Request sent
                                if (mCurrent_State.equals("req_sent")) {
                                    mFriendsReqDatabase.child(event_id).child(current_user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {


                                            mSenderDatabase.child(event_id).child(current_user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                    mProfileSendReqBtn.setEnabled(true);
                                                    mProfileSendReqBtn.setVisibility(VISIBLE);
                                                    mCurrent_State = "not_friends";
                                                    getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                                    getmProfileDeclineReqBtn.setEnabled(false);
                                                    //mChat.setEnabled(false);
                                                    // mChat.announceForAccessibility("Can't chat till Event host accept's your friend request");
                                                    mChat.setVisibility(VISIBLE);

                                                }
                                            });


                                        }
                                    });
                                }


                            }
                        });


                //------------------------- REQUEST RECEVIED STATE--------------------------------------------------------

                if (mCurrent_State.equals("req_received")) {
                    mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Sender").child(event_id);


//                    Map friendsMap = new HashMap();
//                    friendsMap.put("Friends/"+ user_id + "/" + mCurrent_user.getUid()+ "/"+ sent_user_id +"/date" , ServerValue.TIMESTAMP);


                    mUserDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            String sent_user_id = dataSnapshot.child("Sender").getValue().toString();


                            mFriendDatabase.child(event_id).child(current_user_id).child(sent_user_id).setValue(ServerValue.TIMESTAMP).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    mFriendsReqDatabase.child(event_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                            mFriendsReqDatabase.child(event_id).child("sender").child(current_user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                    mSenderDatabase.child(event_id).child("Sender").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            mProfileSendReqBtn.setEnabled(true);
                                                            mCurrent_State = "friends";
                                                            getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                                            getmProfileDeclineReqBtn.setEnabled(false);
                                                        }
                                                    });
                                                }
                                            });

                                        }
                                    });


                                }
                            });
                            // ...
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // ...
                        }
                    });


                }


                if (mCurrent_State.equals("friends")) {

                    Map unfriendMap = new HashMap();
                    unfriendMap.put("Friend/" + event_id + "/" + current_user_id, null);
                    unfriendMap.put("Friend/" + current_user_id + "/" + event_id, null);

                    mRootref.updateChildren(unfriendMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            if (databaseError == null) {


                                mCurrent_State = "not_friends";


                                getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                getmProfileDeclineReqBtn.setEnabled(false);

                            } else {
                                String error = databaseError.getMessage();
                                Toast.makeText(hosted_main_activity.this, error, Toast.LENGTH_SHORT).show();
                            }
                            mProfileSendReqBtn.setEnabled(true);

                        }
                    });


                }

            }
        });


        //code to open chat screen

      /*  if (mCurrent_State.equals("not_friends") ) {
            Toast.makeText(hosted_main_activity.this,"Cannot chat till Event's Host accepts your Friend Request", Toast.LENGTH_LONG).show();
            mChat.setEnabled(false);
        }
            else if(current_user_id.equals(event_creator_id)){*/

        mChat.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (mCurrent_State.equals("not_friends") || mCurrent_State.equals("req_sent")) {
                    Toast.makeText(hosted_main_activity.this, "Cannot chat till Event's Host accepts your Friend Request", Toast.LENGTH_LONG).show();
                    mChat.setEnabled(false);
                } else {
                    mChat.setEnabled(true);
                    mChat.setVisibility(VISIBLE);

                    Intent chat_page = new Intent(getBaseContext(), chat_activity.class);

                    Toast.makeText(hosted_main_activity.this, event_creator_id, Toast.LENGTH_SHORT).show();

                    chat_page.putExtra("event_creator_id", event_creator_id);
                    chat_page.putExtra("current_user_id", current_user_id);
                    chat_page.putExtra("surname", surname);
                    chat_page.putExtra("imageUrl", imageUrl);
                    chat_page.putExtra("middlename", middlename);
                    chat_page.putExtra("Facebook_id", user_facebook_uri);
                    chat_page.putExtra("Facebook_uri", user_facebook_uri);
                    chat_page.putExtra("user_email_id", user_email_id);
                    chat_page.putExtra("name_fb", name_fb);
                    chat_page.putExtra("event_id", event_id);


                    startActivity(chat_page);

                    overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_right, com.createfirst.company.foodpals.R.anim.slide_out_left);

                }
            }
        });

    }

//    }

    //Boolean methord to make the online icon
    public void setUserOnline(String online_status) {

        ImageView useronlineView = findViewById(com.createfirst.company.foodpals.R.id.hosted_online_icon);

        if (online_status.equals("true")) {

            useronlineView.setVisibility(VISIBLE);

        } else {

            useronlineView.setVisibility(View.INVISIBLE);

        }


    }

    //Navigation Back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Write your code here

        if (layoutToremove.isShown()) {
            layoutchange_back();
            Toast.makeText(hosted_main_activity.this, "INFLATOR", Toast.LENGTH_SHORT).show();


        } else {
            super.onBackPressed();

        }


        overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
    }


    private void friends_details() {

        mFriendDatabase.child(event_id).child(event_creator_id).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        //code to populate friends in Event layout

        mFriendDatabase.child(event_id).child(event_creator_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                friend_mNames.clear();
                friend_mProfilepic.clear();

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {

                    friend_user_id = childDataSnapshot.getKey().toString();

                    req_userDB.child(friend_user_id).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            friend_name = dataSnapshot.child("Full_name").getValue().toString();
                            friend_image = dataSnapshot.child("Profile_image").getValue().toString();


                            friend_mNames.add(friend_name);
                            friend_mProfilepic.add(friend_image);

                            initRecyclerView();

                        }


                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    //code to get Image and user name for Req user inflate layout
    private void req_get_sender_details() {


        mSenderDatabase.child(event_id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                req_mNames.clear();
                req_mProfilepic.clear();
                req_mUser_id.clear();

                if (dataSnapshot.getValue() == null) {

                    if (new_adapter == null) {

                    } else {
                        new_adapter.notifyDataSetChanged();
                    }


                }


                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {


                    first_username = postSnapshot.getKey().toString();

                    if (first_username == null) {
                        req_get_sender_details();
                    } else {


                        req_userDB.child(first_username).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                req_user_name = dataSnapshot.child("Full_name").getValue().toString();
                                req_profile_image = dataSnapshot.child("Profile_image").getValue().toString();
                                req_user_id = dataSnapshot.child("Firebase_id").getValue().toString();


                                req_mNames.add(req_user_name);
                                req_mProfilepic.add(req_profile_image);
                                req_mUser_id.add(req_user_id);


                                req_recycler_view();


                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }


                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    //Recycler View code for accepted friends

    private void initRecyclerView() {
        Log.d(TAG, "Init recycler view:initi Recyvlerview");

        LinearLayoutManager layoutmanager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(com.createfirst.company.foodpals.R.id.profile_image_holder);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
        recyclerView.setLayoutManager(gridLayoutManager);

        event_profile_adapter adapter = new event_profile_adapter(this, friend_mNames, friend_mProfilepic);
        recyclerView.setAdapter(adapter);
    }


    //Reycler View code for Req Recived users
    private void req_recycler_view() {
        Log.d(TAG, "Init recycler view:initi Recyvlerview");


        RecyclerView recyclerView = findViewById(com.createfirst.company.foodpals.R.id.req_reciever_recycler_view);
        LinearLayoutManager layoutmanager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutmanager);


        new_adapter = new req_reciever_adapter(this, req_mNames, req_mProfilepic, req_mUser_id);
        new_adapter.notifyDataSetChanged();
        recyclerView.setAdapter(new_adapter);


    }


    //code for accepting a user req
    @Override
    public void onClickInAdapter(final String content) {

        mFriendDatabase.child(event_id).child(current_user_id).child(content).setValue(ServerValue.TIMESTAMP).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {


                mFriendsReqDatabase.child(event_id).child(content).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        mSenderDatabase.child(event_id).child(content).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {


                            @Override
                            public void onComplete(@NonNull Task<Void> task) {

                                mProfileSendReqBtn.setEnabled(false);
                                mProfileSendReqBtn.setVisibility(View.INVISIBLE);
                                mCurrent_State = "friends";
                                getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                getmProfileDeclineReqBtn.setEnabled(false);


                                req_get_sender_details();

                                //  onRestart();


                                Toast.makeText(hosted_main_activity.this, "Req Accepted", Toast.LENGTH_SHORT).show();


                            }
                        });


                    }
                });

            }
        });
    }


    //code for rejecting a req for a user
    @Override
    public void onClickReject(final String content) {

        delete_req_sender(content);
        new_adapter.notifyDataSetChanged();


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    //Layout change after manage button click
    public void layoutchange() {

        layoutToAdd.setVisibility(View.GONE);
        layoutToremove.setVisibility(View.VISIBLE);

        req_get_sender_details();
    }

    //Layout change from inflator to back

    public void layoutchange_back() {

        layoutToAdd.setVisibility(View.VISIBLE);
        layoutToremove.setVisibility(View.GONE);

    }


    //code to remove sender id from freind req and sender db
    private boolean delete_req_sender(String id) {
        //getting the specified artist reference
        DatabaseReference dR = mSenderDatabase.child(event_id).child(id);


        //removing artist
        dR.removeValue().isSuccessful();

        //getting the tracks reference for the specified artist
        DatabaseReference drTracks = mFriendsReqDatabase.child(event_id).child(id);

        //removing all tracks
        drTracks.removeValue();
        Toast.makeText(getApplicationContext(), "User Database deleted", Toast.LENGTH_LONG).show();

        return true;
    }


    protected void onStart() {
        super.onStart();


    }


}


//  req_get_sender_details();

//        mFriendsReqDatabase.child(event_id).child(content).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//            @Override
//            public void onSuccess(Void aVoid) {
//
//
//
//                mSenderDatabase.child(event_id).child(content).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
//                    @Override
//                    public void onSuccess(Void aVoid) {
//
//                        mProfileSendReqBtn.setEnabled(true);
//                        mProfileSendReqBtn.setVisibility(View.VISIBLE);
//                        mCurrent_State = "not_friends";
//                        getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
//                        getmProfileDeclineReqBtn.setEnabled(false);
//
//
//                        req_get_sender_details();
//                        new_adapter.notifyDataSetChanged();
//
//                        Toast.makeText(hosted_main_activity.this, "Req Rejected", Toast.LENGTH_SHORT).show();
//
//
//
//                    }
//                });
//
//
//            }
//        });


