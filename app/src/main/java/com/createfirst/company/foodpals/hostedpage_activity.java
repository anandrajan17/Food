//package com.example.anandha.foodpals;
//
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.RecyclerView;
//import android.view.View;
//import android.widget.TextView;
//
//import com.firebase.ui.database.FirebaseRecyclerAdapter;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//
//public class hostedpage_activity extends AppCompatActivity {
//
//
//    private RecyclerView mUserList;
//    private DatabaseReference mUserDatabase;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_hostedpage_activity);
//
//        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("Event");
//
//        mUserList = (RecyclerView) findViewById(R.id.user_list);
//        mUserList.setHasFixedSize(true);
//        mUserList.setLayoutManager(new LinearLayoutManager(this));
//    }
//
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//
//
//        FirebaseRecyclerAdapter<Firebase_Event, UserViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Firebase_Event, UserViewHolder>(
//                Firebase_Event.class,
//                R.layout.activity_card_list,
//                UserViewHolder.class,
//                mUserDatabase
//        ) {
//            @Override
//            protected void populateViewHolder(UserViewHolder usersviewHolder, Firebase_Event firebase_event, int i) {
//
////setting it to the palceholder one by one
//                usersviewHolder.setName(firebase_event.getUser_facebook_id());
//
//
//                usersviewHolder.mview.setOnClickListener(new View.OnClickListener(){
//                    @Override
//                    public void onClick(View view){
//
//                    }
//                });
//
//
//            }
//        };
//
//        mUserList.setAdapter((firebaseRecyclerAdapter));
//    }
//
//
//    public static class UserViewHolder extends RecyclerView.ViewHolder {
//
//        View mview;
//
//        public UserViewHolder(View itemView) {
//            super(itemView);
//
//            mview = itemView;
//
//        }
////create method to set new value for the views
//        public void setName(String name){
//
//            TextView userNameView = (TextView) mview.findViewById(R.id.nameCardTextView);
//
//            userNameView.setText(name);
//
//        }
//    }
//
//}
