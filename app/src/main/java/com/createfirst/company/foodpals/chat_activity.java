package com.createfirst.company.foodpals;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.createfirst.company.foodpals.R.id.et_message;


public class chat_activity extends AppCompatActivity {

    private String event_creator_user_id;
    private FirebaseUser current_online_user_id;
    private Toolbar mChatToolbar;
    private String middlename;
    private String name_fb;
    private String surname;
    private String event_id_chat;
    private String event_creator_image;

    private RecyclerView mMessageslist;
    private SwipeRefreshLayout mRefershlayout;

    private EditText message_type_text;
    private TextView User_profile_name;
    private TextView online_status;

    private CircleImageView creator_profile_pic;
    private ImageButton message_send_button;
    private Button send_button;

    private FirebaseAuth mAuth;
    private DatabaseReference dCreator_user;
    private DatabaseReference dCurrent_user;
    private DatabaseReference dRootRef;
    private DatabaseReference dmessages;

    private String mcurrent_online_user_id;

    private final List<Messages> messagesList = new ArrayList<>();
    private final List<Messages> image_url_list = new ArrayList<>();
    private final List<Messages> sender_name_list = new ArrayList<>();
    private LinearLayoutManager mLinearLayout;
    private MessageAdapater mAdapter;

    private static final int TOTAL_ITMES_TO_LOAD = 10;

    private int mCurrentPage = 1;

    private int itemPos = 0;
    private int itemPos1 = 0;
    private int itemPos2 = 0;
    private String mLastkey = "";
    private String mPrevKey = "";
    private ProgressDialog mChatloading;


    //Funtions to send for chat database

    public String chat_username;
    public String chat_imageUrl;
    public String chat_last_seen_closed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.chat_activity);

       getWindow().setBackgroundDrawableResource(com.createfirst.company.foodpals.R.drawable.chat_bg);

        mChatToolbar = (Toolbar) findViewById(com.createfirst.company.foodpals.R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        Drawable backArrow = getResources().getDrawable(com.createfirst.company.foodpals.R.drawable.ic_arrow_back_black_24dp);
        backArrow.setColorFilter(getResources().getColor(com.createfirst.company.foodpals.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);


        mAuth = FirebaseAuth.getInstance();
        // current_online_user_id  = FirebaseAuth.getInstance().getCurrentUser();
        mcurrent_online_user_id = mAuth.getCurrentUser().getUid();

        event_creator_user_id = getIntent().getStringExtra("event_creator_id");
        event_id_chat = getIntent().getStringExtra("event_id");
        event_creator_image = getIntent().getStringExtra("imageUrl");

        middlename = getIntent().getStringExtra("middlename");
        surname = getIntent().getStringExtra("surname");
        name_fb = getIntent().getStringExtra("name_fb");


        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(com.createfirst.company.foodpals.R.layout.chat_custom_bar, null);
        actionBar.setCustomView(action_bar_view);

        // setting the button and text
        message_type_text = (EditText) findViewById(et_message);
        creator_profile_pic = (CircleImageView) findViewById(com.createfirst.company.foodpals.R.id.event_creator_chat_pic);
        //message_send_button = (ImageButton) findViewById(R.id.bt_attachment);
        User_profile_name = (TextView) findViewById(com.createfirst.company.foodpals.R.id.chat_creator_name);
        online_status = (TextView) findViewById(com.createfirst.company.foodpals.R.id.chat_creator_status);
        send_button = (Button) findViewById(com.createfirst.company.foodpals.R.id.bt_send);


        mAdapter = new MessageAdapater(messagesList, image_url_list, sender_name_list);
        mRefershlayout = (SwipeRefreshLayout) findViewById(com.createfirst.company.foodpals.R.id.swipe_message_swipelayout);
        mMessageslist = (RecyclerView) findViewById(com.createfirst.company.foodpals.R.id.mMessage_list);
        mLinearLayout = new LinearLayoutManager(this);
        mMessageslist.setHasFixedSize(true);
        mMessageslist.setLayoutManager(mLinearLayout);

        mMessageslist.setAdapter(mAdapter);


        //Setting prrofile image and name

        User_profile_name.setText("" + name_fb + " " + middlename + " " + surname);
        Picasso.with(chat_activity.this).load(event_creator_image).placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(creator_profile_pic);


        dCreator_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(event_creator_user_id);
        dCurrent_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mcurrent_online_user_id);
        dmessages = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("messages");

        dRootRef = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT);


        mChatloading = new ProgressDialog(this);
        mChatloading.setTitle("Loading Chat");
        mChatloading.setMessage("Please wait While we Load the Event");
        mChatloading.setCanceledOnTouchOutside(false);
        mChatloading.show();


        loadMessages();


        dCurrent_user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                chat_username = dataSnapshot.child("First_Name").getValue().toString();
                chat_last_seen_closed = dataSnapshot.child("last_seen_open").getValue().toString();
                chat_imageUrl = dataSnapshot.child("Profile_image").getValue().toString();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        dCreator_user.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String user_online = dataSnapshot.child("online").getValue().toString();
                String last_seen_closed = dataSnapshot.child("last_seen_open").getValue().toString();
                String Chat_profile_pic = dataSnapshot.child("Profile_image").getValue().toString();

                if (user_online.equals("true")) {

                    online_status.setText("Online");

                } else {

                    GetTimeago getTimeago = new GetTimeago();

                    long lastTime = Long.parseLong(last_seen_closed);
                    String lastSeenTime = getTimeago.getTimeAgo(lastTime, getApplicationContext());

                    online_status.setText(lastSeenTime);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        dRootRef.child("Chat").child(event_id_chat).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if (!dataSnapshot.hasChild(event_id_chat)) {

                    Map chatAddmap1 = new HashMap();
                    chatAddmap1.put("seen", false);
                    chatAddmap1.put("Creator", event_creator_user_id);
                    chatAddmap1.put("timestamp", ServerValue.TIMESTAMP);


                    Map chatAddmap2 = new HashMap();
                    chatAddmap2.put("seen", false);
                    chatAddmap2.put("Guest", mcurrent_online_user_id);
                    chatAddmap2.put("timestamp", ServerValue.TIMESTAMP);

                    Map chatUserMap = new HashMap();
                    chatUserMap.put("Chat/" + event_id_chat + "/" + event_creator_user_id, chatAddmap1);
                    chatUserMap.put("Chat/" + event_id_chat + "/" + mcurrent_online_user_id, chatAddmap2);

                    dRootRef.updateChildren(chatUserMap, new DatabaseReference.CompletionListener() {

                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if (databaseError != null) {

                                Log.d("CCHAT_LOG", databaseError.getMessage().toString());

                            }
                        }
                    });

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        send_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sendMessage();


            }
        });

        mRefershlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                mCurrentPage++;

                itemPos = 0 ;
                itemPos1 = 0;
                itemPos2 =0;

                loadMoreMessages();
            }
        });

    }

    private void loadMoreMessages() {

        DatabaseReference messageref = dRootRef.child("messages").child(event_id_chat);

        Query messsageQuery = messageref.orderByKey().endAt(mLastkey).limitToLast(TOTAL_ITMES_TO_LOAD);

        messsageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {




                String messageKey = dataSnapshot.getKey();

                Log.d("Swipping", "Last Key : " + mLastkey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);

                if(!mPrevKey.equals(messageKey)){

                    Messages message = dataSnapshot.getValue(Messages.class);
                    Messages imageUrl = dataSnapshot.getValue(Messages.class);
                    Messages sender_name = dataSnapshot.getValue(Messages.class);




                    image_url_list.add(itemPos++, imageUrl);
                    messagesList.add(itemPos1++, message);
                    sender_name_list.add(itemPos2++, sender_name);



                } else {

                    mPrevKey = mLastkey;

                }


                if (itemPos == 1) {

                    mLastkey = messageKey;

                }

                Log.d("TOTALKEYS", "Last Key : " + mLastkey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);


                mAdapter.notifyDataSetChanged();
                mRefershlayout.setRefreshing(false);
                mLinearLayout.scrollToPositionWithOffset(10, 0);

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    private void loadMessages() {

        DatabaseReference messageref = dRootRef.child("messages").child(event_id_chat);

        Query messsageQuery = messageref.limitToLast(mCurrentPage * TOTAL_ITMES_TO_LOAD);


        messsageQuery.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Messages message = dataSnapshot.getValue(Messages.class);
                Messages imageUrl = dataSnapshot.getValue(Messages.class);
                Messages sender_name = dataSnapshot.getValue(Messages.class);

                itemPos++;

                if (itemPos == 1) {

                    String messageKey = dataSnapshot.getKey();

                    mLastkey = messageKey;
                    mPrevKey = messageKey;

                    Log.d("TOTALKEYS", "Last Key : " + mLastkey + " | Prev Key : " + mPrevKey + " | Message Key : " + messageKey);

                }






                image_url_list.add(imageUrl);
                messagesList.add(message);
                sender_name_list.add(sender_name);

                mAdapter.notifyDataSetChanged();

                mMessageslist.scrollToPosition(messagesList.size() - 1);

                mRefershlayout.setRefreshing(false);


            }


            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mChatloading.dismiss();

    }


    private void sendMessage() {

        String message = message_type_text.getText().toString();

        Toast.makeText(chat_activity.this, mcurrent_online_user_id, Toast.LENGTH_SHORT).show();

        if (!TextUtils.isEmpty(message)) {

            // String current_user_ref = "messages/"+ event_id_chat; //+ "/" + event_creator_user_id;
            String chat_user_ref = "messages/" + event_id_chat; // + "/" + mcurrent_online_user_id;

            DatabaseReference user_message_push = dmessages.child(MainActivity.ENVIRONMENT).child(event_id_chat).child(mcurrent_online_user_id).push();

            String push_id = user_message_push.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("type", "text");
            messageMap.put("time", ServerValue.TIMESTAMP);
            messageMap.put("event_creator", event_creator_user_id);
            messageMap.put("from", mcurrent_online_user_id);
            messageMap.put("sender_name", chat_username);
            messageMap.put("imageUrl", chat_imageUrl);

            Map messageUserMap = new HashMap();

            // messageUserMap.put (current_user_ref+ "/" + push_id, messageMap);
            messageUserMap.put(chat_user_ref + "/" + push_id, messageMap);
            message_type_text.setText("");


            dRootRef.updateChildren(messageUserMap, new DatabaseReference.CompletionListener() {
                @Override
                public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                    if (databaseError != null) {
                        Log.d("CHAT_LOG", databaseError.getMessage().toString());
                    }

                }
            });


        }
    }

    //Navigation Back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Write your code here

        super.onBackPressed();
        overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
    }
}
