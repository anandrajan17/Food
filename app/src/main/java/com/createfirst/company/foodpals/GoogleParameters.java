package com.createfirst.company.foodpals;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shilpa on 09-06-2017.
 */

public class GoogleParameters implements Parcelable{

    public GoogleParameters(String name,String photo_reference,boolean open_now) {
        this.name = name;
        this.photo_reference = photo_reference;
        this.open_now = open_now;
    }

    public GoogleParameters() {
    }

    protected GoogleParameters(Parcel in) {
        photo_reference = in.readString();
        name = in.readString();
        open_now = in.readByte() != 0;
        address = in.readString();
        price_level = in.readInt();
    }

    public static final Creator<GoogleParameters> CREATOR = new Creator<GoogleParameters>() {
        @Override
        public GoogleParameters createFromParcel(Parcel in) {
            return new GoogleParameters(in);
        }

        @Override
        public GoogleParameters[] newArray(int size) {
            return new GoogleParameters[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getPhoto_reference() {
        return photo_reference;
    }

    public void setPhoto_reference(String photo_reference) {
        this.photo_reference = photo_reference;
    }

    public boolean isOpen_now() {
        return open_now;
    }

    public void setOpen_now(boolean open_now) {
        this.open_now = open_now;
    }

    public int getPrice_level() {
        return price_level;
    }

    public void setPrice_level(int price_level) {
        this.price_level = price_level;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    private String photo_reference;
    private String name;
    boolean open_now;
    Float rating;
    String address;
    int price_level;
    Double lat;
    Double lng;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(photo_reference);
        dest.writeString(name);
        dest.writeByte((byte) (open_now ? 1 : 0));
        dest.writeString(address);
        dest.writeInt(price_level);
    }
}
