package com.createfirst.company.foodpals;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Shilpa on 11-05-2017.
 */
/*public class CustomListAdapter extends ArrayAdapter<Product>*/

public class CustomListAdapter extends ArrayAdapter<GoogleParameters> {

    //ArrayList<Product> products;
    ArrayList<GoogleParameters> products;
    Context context;
    int resource;

    /*public CustomListAdapter(Context context, int resource, ArrayList<Product> products) {
        super(context, resource, products);
        this.products = products;
        this.context = context;
        this.resource = resource;
    }*/

    public CustomListAdapter(Context context, int resource, ArrayList<GoogleParameters> products) {
        super(context, resource, products);
        this.products = products;
        this.context = context;
        this.resource = resource;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) getContext()
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(com.createfirst.company.foodpals.R.layout.card_google, null, true);
        }

        //Product product = getItem(position);
        GoogleParameters product = getItem(position);

       /* ImageView imageView = (ImageView) convertView.findViewById(R.id.imageViewProduct);
        Picasso.with(context).load(product.getImage()).into(imageView);*/

        ImageView imageView = (ImageView) convertView.findViewById(com.createfirst.company.foodpals.R.id.restaurantImageView);
        Picasso.with(context).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+product.getPhoto_reference()+"&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q")
                .fit()
                .into(imageView);

        TextView txtName = (TextView) convertView.findViewById(com.createfirst.company.foodpals.R.id.txtName);
        txtName.setText(product.getName());
//        TextView mAdressTextView = (TextView) convertView.findViewById(R.id.txtLocation);
//        mAdressTextView.setText(product.getAddress());
//
//        TextView openNow = (TextView) convertView.findViewById(R.id.txtOpenNow);
//
//        if(product.isOpen_now()){
//            openNow.setText("Open Now");
//        }
//        else{
//            openNow.setText("Closed");
//        }
//        TextView mRating1 = (TextView) convertView.findViewById(R.id.rating1);
//        if(product.getRating()!=null) {
//            mRating1.setText(product.getRating().toString()+"/5");
//        }else {
//            mRating1.setText(0+"/5");
//        }
//        TextView mMoney = (TextView) convertView.findViewById(R.id.money);
//        mMoney.setText("");
//            /*Code for putting price level to show $ sign*/
//        if(product.getPrice_level()!=0) {
//            for(int i=0; i<product.getPrice_level();i++){
//                mMoney.append("$");
//            }
//        }else {
//            mMoney.setText("");
//        }
//
       return convertView;
   }

}
