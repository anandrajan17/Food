package com.createfirst.company.foodpals;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Created by Shilpa on 11-05-2017.
 */

public class Product {

    private int id;
    private String image;

    @SerializedName("name")
    @Expose
    private String name;
    private String location;

    public Product(int id,String image, String name, String location) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.location = location;
    }



    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getlocation() {
        return location;
    }

    public void setlocation(String location) {
        this.location = location;
    }
}
