package com.createfirst.company.foodpals;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.Calendar;


public class

hostinf_resturant extends AppCompatActivity {

    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private TextView timeView;
    private TextView h_resturant_name;
    private TextView h_resturant_address;
    private int year, month, day,hour,minute,currentHour,currentMinute;
    String amPm;



    EditText h_additional_info;
    private Button host_button;
    private ImageView h_imageview;
    private TextView venueset;




// Radio Button

    RadioGroup radioGroup;
    RadioButton radioButton;






//Firebase
    private Firebase mRootRef;
    DatabaseReference databaseEvent;

    //User for Firebases
    public  String name_fb;
    public  String surname ;
    public  String imageUrl ;
    public  String middlename ;
    public  String user_facebook_id ;
    public  String firebase_user_id ;
    public  String user_facebook_uri ;
    public  String user_email_id ;
    public  String userOnline;
    public  String event_id;
    public String resLat;
    public String resLong;



    //Resturant Details
    public String resturantImage;
    public String resturantName;
    public String resturantAddress;
    public String resturantRating;
    public String averageCostfortwo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.resturant_hosting_page);



        Toolbar mChatToolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mChatToolbar.setTitleTextColor(Color.WHITE);


        //Back arrow colour
        Drawable backArrow = getResources().getDrawable(com.createfirst.company.foodpals.R.drawable.ic_arrow_back_black_24dp);
        backArrow.setColorFilter(getResources().getColor(com.createfirst.company.foodpals.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);



//        setTheme(android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        if (getSupportActionBar() != null) {
//            getSupportActionBar().hide();
//        }

//        View decorView = getWindow().getDecorView();
//// Hide the status bar.
//        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
//        decorView.setSystemUiVisibility(uiOptions);
//// Remember that you should never show the action bar if the
//// status bar is hidden, so hide that too if necessary.
//        ActionBar actionBar = getActionBar();
//       // actionBar.hide();

        //Code for Tool bar and back button for Tool bar
//        Toolbar toolbar = (Toolbar) findViewById(R.id.chat_app_bar);
//        setSupportActionBar(toolbar);
//        ActionBar actionBar = getSupportActionBar();

//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowCustomEnabled(true);
//
//        Drawable backArrow = getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp);
//        backArrow.setColorFilter(getResources().getColor(R.color.bluemain), PorterDuff.Mode.SRC_ATOP);
//        getSupportActionBar().setHomeAsUpIndicator(backArrow);

      try {

          databaseEvent = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Event");

          Bundle main_data = getIntent().getExtras();

          surname = main_data.get("surname").toString();
          name_fb = main_data.get("name_fb").toString();
          imageUrl = main_data.get("imageUrl").toString();
          middlename = main_data.get("middlename").toString();
          user_facebook_id = main_data.get("Facebook_id").toString();
          firebase_user_id = main_data.get("Firebase_user_id").toString();
          user_facebook_uri = main_data.get("Facebook_uri").toString();
          user_email_id = main_data.get("user_email_id").toString();
          resLat = main_data.get("resLat").toString();
          resLong = main_data.get("resLong").toString();


          //Resturant Bundle
          resturantImage = main_data.get("resturant_image").toString();
          resturantName = main_data.get("resturant_name").toString();
          resturantAddress = main_data.get("resturant_address_new").toString();
          resturantRating = main_data.get("resturant_rating").toString();
          averageCostfortwo = main_data.get("resturant_pricerange").toString();


      }catch (NullPointerException e){
          e.printStackTrace();
      }





        //location_text = (EditText) findViewById(R.id.editText);

        // Location_bt = (ImageButton) findViewById(R.id.location_button);
        // location_manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        h_additional_info = (EditText) findViewById(com.createfirst.company.foodpals.R.id.additional_info);
        host_button = (Button) findViewById(com.createfirst.company.foodpals.R.id.h_host_button);
        dateView = (TextView) findViewById(com.createfirst.company.foodpals.R.id.h_event_setdate);
        timeView = (TextView) findViewById(com.createfirst.company.foodpals.R.id.h_event_settime);
        h_imageview= (ImageView) findViewById(com.createfirst.company.foodpals.R.id.h_imageview);
        h_resturant_name = (TextView) findViewById(com.createfirst.company.foodpals.R.id.h_resturant_name);
        h_resturant_address = findViewById(com.createfirst.company.foodpals.R.id.h_resturnat_address);
        radioGroup = findViewById(com.createfirst.company.foodpals.R.id.gender_group);



        // venueset = (EditText) findViewById(R.id.venue);

        mRootRef = new Firebase("https://foodpals-7d9b1.firebaseio.com/event");


//Setting Deatils from bundle to the app
        h_resturant_name.setText(resturantName);

        h_resturant_address.setText(resturantAddress);


    try {
        Picasso.with(hostinf_resturant.this).load(resturantImage)
                .fit()
                .centerCrop()
                .into(h_imageview);
        }catch (IllegalArgumentException e){
        e.printStackTrace();
    }



        dateView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              setDate(dateView);



            }
        });


        timeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                calendar = Calendar.getInstance();
                currentHour = calendar.get(Calendar.HOUR_OF_DAY);
                currentMinute = calendar.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(hostinf_resturant.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minutes) {

                        if (hourOfDay >= 12) {
                            amPm = "PM";
                        } else {
                            amPm = "AM";
                        }
                        timeView.setText(String.format("%02d:%02d", hourOfDay, minutes) + amPm);

                    }
                }, currentHour, currentMinute, false);


                timePickerDialog.show();
            }
        });



        host_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addEvent();


            }
        });








        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month+1, day);




    }


//Radio button code

    public void checkbutton(View v){

        int radioId = radioGroup.getCheckedRadioButtonId();

        radioButton= findViewById(radioId);

        Toast.makeText(hostinf_resturant.this, "Selected Radio Button: " + radioButton.getText(),
                Toast.LENGTH_SHORT).show();
    }


    private void addEvent(){

        int radioId = radioGroup.getCheckedRadioButtonId();

        radioButton = findViewById(radioId);

        //Putting the text from fields to a object
        String additional_details = h_additional_info.getText().toString().trim() ;
        String venue_details = h_additional_info.getText().toString();
        String date_details = dateView.getText().toString();
        String timing_details = timeView.getText().toString();
        String resturant_name = h_resturant_name.getText().toString();
        String resturant_address = h_resturant_address.getText().toString();
        String radio_button = radioButton.getText().toString();



        if(!TextUtils.isEmpty(additional_details)){
            String id = databaseEvent.push().getKey();


            //@shilpa change date_details to lat_long

                Firebase_Event event = new Firebase_Event(resturant_name,resturant_address,resLat,resLong,additional_details , venue_details , date_details , timing_details, radio_button , name_fb, surname , imageUrl , middlename , user_facebook_id , firebase_user_id , user_facebook_uri , user_email_id, id );

                databaseEvent.child(id).setValue(event);


            Intent eventpage_intent = new Intent(hostinf_resturant.this, hosted_main_activity.class);
            eventpage_intent.putExtra("event_id", id);
            startActivity(eventpage_intent);



                Toast.makeText(this, "Event Created is hosted successfully", Toast.LENGTH_LONG).show();



        }else{
            Toast.makeText(this, "Please fill all the details", Toast.LENGTH_LONG).show();
        }




//        //creating child in firebase
//        Firebase additional_detail_data = mRootRef.child("ADDITIONAL DETAILS");
//        Firebase date_deatil_data = mRootRef.child("DATE");
//        Firebase date_data = mRootRef.child("TIME");
//        Firebase venue_data = mRootRef.child("VENUE");
//
//
//        //Adding object to the
//        venue_data.push().setValue(venue_details);
//        additional_detail_data.push().setValue(additional_details);
//        date_data.push().setValue(date_details);

    }




    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca",
                Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this,
                    myDateListener, year, month, day);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new
            DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0,
                                      int arg1, int arg2, int arg3) {
                    // TODO Auto-generated method stub
                    // arg1 = yearß
                    // arg2 = month
                    // arg3 = day
                    showDate(arg1, arg2+1, arg3);
                }
            };

    private void showDate(int year, int month, int day) {
        dateView.setText(new StringBuilder().append(day).append("/")
                .append(month).append("/").append(year));


    }





    public void onTimeSet() {
        // Do something with the time chosen by the user
        timeView.setText(hour + ":" + minute);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();
                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Write your code here

        super.onBackPressed();
        overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
    }





}






//        location_listener = new LocationListener() {
//            @Override
//            public void onLocationChanged(Location location) {
//
//                Toast.makeText(hostinf_resturant.this, "LAT" + location.getLatitude(), Toast.LENGTH_LONG).show();
//                Additional_info.setText("LAT" + location.getLatitude() + "Long" + location.getLongitude());
//            }
//
//            @Override
//            public void onStatusChanged(String provider, int status, Bundle extras) {
//
//            }
//
//            @Override
//            public void onProviderEnabled(String provider) {
//
//            }
//
//            @Override
//            public void onProviderDisabled(String provider) {
//
//
//                Toast.makeText(hostinf_resturant.this, "Enable location", Toast.LENGTH_LONG).show();
//
//            }
//        };

//        Location_bt.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(hostinf_resturant.this, "Enable location", Toast.LENGTH_LONG).show();
//
//
//               /* if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//                    // TODO: Consider calling
//                    //    ActivityCompat#requestPermissions
//                    // here to request the missing permissions, and then overriding
//                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
//                    //                                          int[] grantResults)
//                    // to handle the case where the user grants the permission. See the documentation
//                    // for ActivityCompat#requestPermissions for more details.
//                    return;
//                }
//                location_manager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, location_listener);*/
//
//            }
//        });












//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


