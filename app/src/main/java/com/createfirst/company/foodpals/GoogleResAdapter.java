package com.createfirst.company.foodpals;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.createfirst.company.foodpals.mRecycler.CustomFilter;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Shilpa on 6/25/2017..
 */
public class GoogleResAdapter extends RecyclerView.Adapter<GoogleResAdapter.MyViewHolder>  /*implements Filterable*/ {

    private Context context;
    public List<GoogleParameters> albumList;
    CustomFilter filter;
    private DatabaseReference dCurrent_user;
    private FirebaseAuth mAuth;

    //User profile details
    public String name_fb;
    public String surname;
    public String imageUrl;
    public String middlename;
    public String Facebook_id;
    public String Firebase_user_id;
    public String Facebook_uri;
    public String user_email_id;
    public double resLat, resLong;
    public String Full_name;
    private final int VIEW_ITEM = 1;
    private final int VIEW_PROG = 0;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 4;
    private int lastVisibleItem, totalItemCount;
    public View viewCard;
    public View.OnClickListener onClickListener;




    private static final String LOG_TAG = "GoogleResAdapter";


    public static class MyViewHolder extends RecyclerView.ViewHolder{

        public TextView txtName, mAdressTextView, openNow, mRating1, mMoney, title, count;
        public ImageView imageView, thumbnail, overflow;
        public Button mHost;
        CardView googleCV;
        private ShimmerFrameLayout resimageshimmer;
        private DatabaseReference mdatabase;


        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(com.createfirst.company.foodpals.R.id.restaurantImageView);
            txtName = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.nameCardTextView);
            mAdressTextView = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.adressTextView);
            openNow = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.openNow);
            mRating1 = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.rating1);
            mMoney = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.money_text);
            mHost = (Button) view.findViewById(com.createfirst.company.foodpals.R.id.z_host_button1);
            googleCV = view.findViewById(com.createfirst.company.foodpals.R.id.card_view_google);
            resimageshimmer = view.findViewById(com.createfirst.company.foodpals.R.id.z_shimmer_image1);

        }


    }


    public GoogleResAdapter(Context context, List<GoogleParameters> albumList, RecyclerView recyclerView) {
        this.context = context;
        this.albumList = albumList;
        Log.i(LOG_TAG, String.format("GoogleResAdapter'%s'", albumList));
//        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
//
//            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
//                    .getLayoutManager();
//
//            recyclerView
//                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
//                        @Override
//                        public void onScrolled(RecyclerView recyclerView,
//                                               int dx, int dy) {
//                            super.onScrolled(recyclerView, dx, dy);
//
//                            totalItemCount = linearLayoutManager.getItemCount();
//                            lastVisibleItem = linearLayoutManager
//                                    .findLastVisibleItemPosition();
//                            if (!loading
//                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
//                                // End has been reached
//                                // Do something
//                                if (onLoadMoreListener != null) {
//                                    onLoadMoreListener.onLoadMore();
//                                }
//                                loading = true;
//                            }
//                        }
//                    });
//        }
    }

    @Override
    public int getItemViewType(int position) {
        return albumList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MyViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(com.createfirst.company.foodpals.R.layout.card_google, parent, false);
            vh = new MyViewHolder(itemView);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(
                    com.createfirst.company.foodpals.R.layout.progressbar, parent, false);
            vh = new ProgressViewHolder(v);
        }
        return vh;


    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final GoogleParameters product = albumList.get(position);

        if (holder instanceof MyViewHolder) {

            Picasso.with(context).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + product.getPhoto_reference() + "&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q")
                    .fit().centerCrop()
                    .placeholder(com.createfirst.company.foodpals.R.drawable.default_resturant_icon)
                    .into(holder.imageView);

            holder.txtName.setText(product.getName());


            // Log.i(LOG_TAG, String.format("ResturantAdapterName'%s'", product));

            holder.mAdressTextView.setText(product.getAddress());


            if (product.isOpen_now()) {
                holder.openNow.setText("Open Now");
            } else {

                holder.openNow.setText("Closed");
            }


            if (product.getRating() != null) {
                //holder.mRating1.setText(product.getRating().toString()+"/5");
                holder.mRating1.setText(product.getRating().toString());
            } else {
                holder.mRating1.setText(0 + "/5");
            }


            holder.mMoney.setText("");
            //Code for putting price level to show $ sign


            if (product.getPrice_level() >= 0) {
                holder.mMoney.append("Price Level" + " " + product.getPrice_level() + "/" + "5");
            } else if (product.getPrice_level() == 0) {
                holder.mMoney.setText("Price Level" + " " + product.getPrice_level() + "/" + "5");


            }

            if(product.getLat()!=null){
                resLat = product.getLat();
            }
            if(product.getLng()!=null){
                resLong = product.getLng();
            }
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }


       /* if (holder.mHost.isInTouchMode()) {
            viewCard = holder.mHost;
        }
        if (holder.googleCV.isClickable()){
            viewCard = holder.googleCV;
        }
        if (holder.mRating1.isClickable()){
            viewCard = holder.mRating1;
        }



        viewCard.setOnClickListener(new View.OnClickListener() {*/

        /*if (holder.googleCV.isClickable()) {
            //holder.viewCard = holder.googleCV;
            holder.googleCV.setOnClickListener(this.onClickListener);
        }
        if (holder.mHost.isClickable()){
            holder.mHost.setOnClickListener(this.onClickListener);
        }
        if (holder.mRating1.isClickable()){
            holder.mRating1.setOnClickListener(this.onClickListener);
        }*/


        holder.googleCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void
            onClick(View view) {

                Bundle bundle = new Bundle();
                String RestaurantName = holder.txtName.getText().toString();
                String ResImage = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + product.getPhoto_reference() + "&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q";
                String LocName = holder.mAdressTextView.getText().toString();
                String OpenNow = holder.openNow.getText().toString();
                String Ratings = holder.mRating1.getText().toString();
                String Money = holder.mMoney.getText().toString();

                bundle.putString("name", RestaurantName);
                bundle.putString("imageRes", ResImage);
                bundle.putString("locationname", LocName);
                bundle.putString("openNow", OpenNow);
                bundle.putString("ratings", Ratings);
                bundle.putString("money", Money);

                resturant_popup(bundle);
            }

            //Code to Trigger the custom pop up

            public void resturant_popup(Bundle bundle) {
                TextView txtclose;
                Button resturant_host;
                TextView resturant_name;
                TextView resturant_address;
                TextView resturant_;
                Button resturant_rating;
                TextView resturant_cost_for_two;
                TextView open_now;
                ImageView resturant_image;

               final String RestName = bundle.getString("name");
                final String RestRating = bundle.getString("ratings");
                final  String RestAddress = bundle.getString("locationname");
                final  String RestOpennow = bundle.getString("openNow");
                final String RestImage = bundle.getString("imageRes");
                final  String Restpricerange = bundle.getString("money");

                mAuth = FirebaseAuth.getInstance();
                // current_online_user_id  = FirebaseAuth.getInstance().getCurrentUser();
                String mcurrent_online_user_id = mAuth.getCurrentUser().getUid();
                dCurrent_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mcurrent_online_user_id);


                final Dialog myDialog = new Dialog(GoogleResAdapter.this.context);

                //Initializing the Pop up codes

                myDialog.setContentView(com.createfirst.company.foodpals.R.layout.resturant_custom_popup_google);
                txtclose = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.txtclose1);
                resturant_host = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_host1);
                resturant_name = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_name1);
                resturant_rating = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_rating1);
                resturant_image = (ImageView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_image1);
                resturant_address = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_address1);
                open_now = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.opennow1);
                resturant_cost_for_two = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cost_for_two1);


                dCurrent_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        name_fb = dataSnapshot.child("First_Name").getValue().toString();
                        surname = dataSnapshot.child("Last_name").getValue().toString();
                        imageUrl = dataSnapshot.child("Profile_image").getValue().toString();
                        middlename = dataSnapshot.child("Middle_Name").getValue().toString();
                        Facebook_id = dataSnapshot.child("Facebook_id").getValue().toString();
                        Firebase_user_id = dataSnapshot.child("Firebase_id").getValue().toString();
                        user_email_id = dataSnapshot.child("Email_id").getValue().toString();
                        Facebook_uri = dataSnapshot.child("Facebook_uri").getValue().toString();





                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                resturant_host.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent hosting_page = new Intent(GoogleResAdapter.this.context, hostinf_resturant.class);

                        //These are user details
                        hosting_page.putExtra("surname", surname);
                        hosting_page.putExtra("imageUrl", imageUrl);
                        hosting_page.putExtra("middlename", middlename);
                        hosting_page.putExtra("Facebook_id", Facebook_id);
                        hosting_page.putExtra("Firebase_user_id", Firebase_user_id);
                        hosting_page.putExtra("Facebook_uri", Facebook_uri);
                        hosting_page.putExtra("user_email_id", user_email_id);
                        hosting_page.putExtra("name_fb", name_fb);
                        hosting_page.putExtra("resLat",resLat);
                        hosting_page.putExtra("resLong",resLong);

                        //These are resturant details

                        hosting_page.putExtra("resturant_image", RestImage);
                        hosting_page.putExtra("resturant_name", RestName);
                        hosting_page.putExtra("resturant_cusine", RestAddress);
                        hosting_page.putExtra("resturant_address_new", RestAddress);
                        hosting_page.putExtra("resturant_pricerange", Restpricerange);
                        hosting_page.putExtra("resturant_rating", RestRating);



                        context.startActivity(hosting_page);

                        myDialog.dismiss();

                    }
                });

                //Setting the String values to pop up
                resturant_rating.setText(RestRating);
                resturant_name.setText(RestName);
                resturant_address.setText(RestAddress);
                open_now.setText(RestOpennow);
                resturant_cost_for_two.setText(Restpricerange);


                //Setting the image
                Picasso.with(GoogleResAdapter.this.context).load(RestImage)
                        .fit()
                        .centerCrop()
                        .into(resturant_image);


                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
                myDialog.show();
            }

       });

        holder.mHost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void
            onClick(View view) {

                Bundle bundle = new Bundle();
                String RestaurantName = holder.txtName.getText().toString();
                String ResImage = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + product.getPhoto_reference() + "&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q";
                String LocName = holder.mAdressTextView.getText().toString();
                String OpenNow = holder.openNow.getText().toString();
                String Ratings = holder.mRating1.getText().toString();
                String Money = holder.mMoney.getText().toString();

                bundle.putString("name", RestaurantName);
                bundle.putString("imageRes", ResImage);
                bundle.putString("locationname", LocName);
                bundle.putString("openNow", OpenNow);
                bundle.putString("ratings", Ratings);
                bundle.putString("money", Money);

                resturant_popup(bundle);
            }

            //Code to Trigger the custom pop up

            public void resturant_popup(Bundle bundle) {
                TextView txtclose;
                Button resturant_host;
                TextView resturant_name;
                TextView resturant_address;
                TextView resturant_;
                Button resturant_rating;
                TextView resturant_cost_for_two;
                TextView open_now;
                ImageView resturant_image;

                final String RestName = bundle.getString("name");
                final String RestRating = bundle.getString("ratings");
                final  String RestAddress = bundle.getString("locationname");
                final  String RestOpennow = bundle.getString("openNow");
                final String RestImage = bundle.getString("imageRes");
                final  String Restpricerange = bundle.getString("money");

                mAuth = FirebaseAuth.getInstance();
                // current_online_user_id  = FirebaseAuth.getInstance().getCurrentUser();
                String mcurrent_online_user_id = mAuth.getCurrentUser().getUid();
                dCurrent_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mcurrent_online_user_id);


                final Dialog myDialog = new Dialog(GoogleResAdapter.this.context);

                //Initializing the Pop up codes

                myDialog.setContentView(com.createfirst.company.foodpals.R.layout.resturant_custom_popup_google);
                txtclose = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.txtclose1);
                resturant_host = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_host1);
                resturant_name = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_name1);
                resturant_rating = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_rating1);
                resturant_image = (ImageView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_image1);
                resturant_address = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_address1);
                open_now = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.opennow1);
                resturant_cost_for_two = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cost_for_two1);


                dCurrent_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        name_fb = dataSnapshot.child("First_Name").getValue().toString();
                        surname = dataSnapshot.child("Last_name").getValue().toString();
                        imageUrl = dataSnapshot.child("Profile_image").getValue().toString();
                        middlename = dataSnapshot.child("Middle_Name").getValue().toString();
                        Facebook_id = dataSnapshot.child("Facebook_id").getValue().toString();
                        Firebase_user_id = dataSnapshot.child("Firebase_id").getValue().toString();
                        user_email_id = dataSnapshot.child("Email_id").getValue().toString();
                        Facebook_uri = dataSnapshot.child("Facebook_uri").getValue().toString();





                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                resturant_host.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent hosting_page = new Intent(GoogleResAdapter.this.context, hostinf_resturant.class);

                        //These are user details
                        hosting_page.putExtra("surname", surname);
                        hosting_page.putExtra("imageUrl", imageUrl);
                        hosting_page.putExtra("middlename", middlename);
                        hosting_page.putExtra("Facebook_id", Facebook_id);
                        hosting_page.putExtra("Firebase_user_id", Firebase_user_id);
                        hosting_page.putExtra("Facebook_uri", Facebook_uri);
                        hosting_page.putExtra("user_email_id", user_email_id);
                        hosting_page.putExtra("name_fb", name_fb);
                        hosting_page.putExtra("resLat",resLat);
                        hosting_page.putExtra("resLong",resLong);


                        //These are resturant details

                        hosting_page.putExtra("resturant_image", RestImage);
                        hosting_page.putExtra("resturant_name", RestName);
                        hosting_page.putExtra("resturant_cusine", RestAddress);
                        hosting_page.putExtra("resturant_address_new", RestAddress);
                        hosting_page.putExtra("resturant_pricerange", Restpricerange);
                        hosting_page.putExtra("resturant_rating", RestRating);



                        context.startActivity(hosting_page);

                        myDialog.dismiss();

                    }
                });

                //Setting the String values to pop up
                resturant_rating.setText(RestRating);
                resturant_name.setText(RestName);
                resturant_address.setText(RestAddress);
                open_now.setText(RestOpennow);
                resturant_cost_for_two.setText(Restpricerange);


                //Setting the image
                Picasso.with(GoogleResAdapter.this.context).load(RestImage)
                        .fit()
                        .centerCrop()
                        .into(resturant_image);


                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
                myDialog.show();
            }

        });



        holder.mRating1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void
            onClick(View view) {

                Bundle bundle = new Bundle();
                String RestaurantName = holder.txtName.getText().toString();
                String ResImage = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference=" + product.getPhoto_reference() + "&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q";
                String LocName = holder.mAdressTextView.getText().toString();
                String OpenNow = holder.openNow.getText().toString();
                String Ratings = holder.mRating1.getText().toString();
                String Money = holder.mMoney.getText().toString();

                bundle.putString("name", RestaurantName);
                bundle.putString("imageRes", ResImage);
                bundle.putString("locationname", LocName);
                bundle.putString("openNow", OpenNow);
                bundle.putString("ratings", Ratings);
                bundle.putString("money", Money);

                resturant_popup(bundle);
            }

            //Code to Trigger the custom pop up

            public void resturant_popup(Bundle bundle) {
                TextView txtclose;
                Button resturant_host;
                TextView resturant_name;
                TextView resturant_address;
                TextView resturant_;
                Button resturant_rating;
                TextView resturant_cost_for_two;
                TextView open_now;
                ImageView resturant_image;

                final String RestName = bundle.getString("name");
                final String RestRating = bundle.getString("ratings");
                final  String RestAddress = bundle.getString("locationname");
                final  String RestOpennow = bundle.getString("openNow");
                final String RestImage = bundle.getString("imageRes");
                final  String Restpricerange = bundle.getString("money");

                mAuth = FirebaseAuth.getInstance();
                // current_online_user_id  = FirebaseAuth.getInstance().getCurrentUser();
                String mcurrent_online_user_id = mAuth.getCurrentUser().getUid();
                dCurrent_user = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mcurrent_online_user_id);


                final Dialog myDialog = new Dialog(GoogleResAdapter.this.context);

                //Initializing the Pop up codes

                myDialog.setContentView(com.createfirst.company.foodpals.R.layout.resturant_custom_popup_google);
                txtclose = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.txtclose1);
                resturant_host = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_host1);
                resturant_name = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_name1);
                resturant_rating = (Button) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_rating1);
                resturant_image = (ImageView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.rest_image1);
                resturant_address = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.resturant_address1);
                open_now = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.opennow1);
                resturant_cost_for_two = (TextView) myDialog.findViewById(com.createfirst.company.foodpals.R.id.cost_for_two1);


                dCurrent_user.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        name_fb = dataSnapshot.child("First_Name").getValue().toString();
                        surname = dataSnapshot.child("Last_name").getValue().toString();
                        imageUrl = dataSnapshot.child("Profile_image").getValue().toString();
                        middlename = dataSnapshot.child("Middle_Name").getValue().toString();
                        Facebook_id = dataSnapshot.child("Facebook_id").getValue().toString();
                        Firebase_user_id = dataSnapshot.child("Firebase_id").getValue().toString();
                        user_email_id = dataSnapshot.child("Email_id").getValue().toString();
                        Facebook_uri = dataSnapshot.child("Facebook_uri").getValue().toString();





                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


                resturant_host.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Intent hosting_page = new Intent(GoogleResAdapter.this.context, hostinf_resturant.class);

                        //These are user details
                        hosting_page.putExtra("surname", surname);
                        hosting_page.putExtra("imageUrl", imageUrl);
                        hosting_page.putExtra("middlename", middlename);
                        hosting_page.putExtra("Facebook_id", Facebook_id);
                        hosting_page.putExtra("Firebase_user_id", Firebase_user_id);
                        hosting_page.putExtra("Facebook_uri", Facebook_uri);
                        hosting_page.putExtra("user_email_id", user_email_id);
                        hosting_page.putExtra("name_fb", name_fb);
                        hosting_page.putExtra("resLat",resLat);
                        hosting_page.putExtra("resLong",resLong);

                        //These are resturant details

                        hosting_page.putExtra("resturant_image", RestImage);
                        hosting_page.putExtra("resturant_name", RestName);
                        hosting_page.putExtra("resturant_cusine", RestAddress);
                        hosting_page.putExtra("resturant_address_new", RestAddress);
                        hosting_page.putExtra("resturant_pricerange", Restpricerange);
                        hosting_page.putExtra("resturant_rating", RestRating);



                        context.startActivity(hosting_page);

                        myDialog.dismiss();

                    }
                });

                //Setting the String values to pop up
                resturant_rating.setText(RestRating);
                resturant_name.setText(RestName);
                resturant_address.setText(RestAddress);
                open_now.setText(RestOpennow);
                resturant_cost_for_two.setText(Restpricerange);


                //Setting the image
                Picasso.with(GoogleResAdapter.this.context).load(RestImage)
                        .fit()
                        .centerCrop()
                        .into(resturant_image);


                txtclose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        myDialog.dismiss();
                    }
                });
                myDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                myDialog.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
                myDialog.show();
            }

        });

    }

   public void setLoaded() {
        loading = false;
    }

     /*public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }*/

    @Override
    public int getItemCount() {
        return albumList.size();
    }

    public static class ProgressViewHolder extends MyViewHolder{
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(com.createfirst.company.foodpals.R.id.progressBar1);
        }
    }
}

