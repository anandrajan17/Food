package com.createfirst.company.foodpals;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.createfirst.company.foodpals.mFragment.EventFragment;
import com.createfirst.company.foodpals.mFragment.ResturantFragment;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.firebase.client.Firebase;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import de.hdodenhof.circleimageview.CircleImageView;

//import com.firebase.ui.auth.AuthUI;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnConnectionFailedListener, ConnectivityReceiver.ConnectivityReceiverListener {

    static final int PLACE_PICKER_REQUEST = 1;

    String address;

    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
    Intent intent;
    private Firebase user_table;
    private DatabaseReference mdatabase;
    private DatabaseReference mdatabaseforrequsers;
    private DatabaseReference mSwitch;
    private DatabaseReference mEvent;
    private DatabaseReference mEvent1;


    private static final String LOG_TAG = "MainActivity";
    private static final int GOOGLE_API_CLIENT_ID = 0;
    private GoogleApiClient mGoogleApiClient;
    private static final int PERMISSION_REQUEST_CODE = 0;

    public static final String GOOGLE_KEY = "AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q";


    public static final int MULTIPLE_PERMISSIONS = 10; // code you want.
    int[] PERMISSION_GRANTED = {1};
    ListView lstPlaces;
    TextView loc_current;
    String current_loca;
    TextView curr_Place;

    Bundle bundle, bundle1, bundleUserLoc;
    LatLng latLng;

    private static double curret_longitude;
    private static double curret_latitude;


    public String res_loc_long;
    public String res_loc_lat;


    //User for Firebases
    public String name_fb;
    public String surname;
    public String imageUrl;
    public String middlename;
    public String Facebook_id;
    public String Firebase_user_id;
    public String Facebook_uri;
    public String user_email_id;
    public String Full_name;


    //Distance between
    public float distanceBet;
    public String distance;
    public Location userLoc, resLoc;

    //private TextView mTextMessage;

    //for the resturant list switch


    public static String resturant_switch;


    // private ShareDialog shareDialog;
    private static final int RC_SIGN_IN = 0;
    private FirebaseAuth auth;
    int year_x, month_x, day_x;
    static final int DILOG_ID = 0;
    private static final boolean Zomato = false;


    //ENVIRONMENT

    public static String STAGING = "STAGING";
    public static String PROD = "PROD";
    public static String ENVIRONMENT = STAGING;



    //declaring arraylist and listview for customlistview
    //ArrayList<Product> arrayList;
    ArrayList<GoogleParameters> arrayList;
    private List<GoogleParameters> GRList;
    private List<ZomatoParameter> GRList1;
    private List<GoogleNextPage> NextToken;
    String urlG;
    ListView lv;
    ScrollView sv;
    private RecyclerView recyclerView;
    //public static final Uri.Builder builder1 = new Uri.Builder().scheme("https").authority("maps.googleapis.com").path("maps/api/place/nearbysearch/json");

    //CallRestClient callRestClient;

    //public static final int MULTIPLE_PERMISSIONS = 10;
    //Permissions


    TextView current_location_header;

    public String[] permissions = new String[]{
            android.Manifest.permission.ACCESS_FINE_LOCATION,
            android.Manifest.permission.ACCESS_COARSE_LOCATION,

    };

    private ShimmerFrameLayout mShimmerViewContainer;


    private static final String TAG = MainActivity.class.getSimpleName();

    /**
     * Code used in requesting runtime permissions.
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;


    private boolean mAlreadyStartedService = false;


    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(com.createfirst.company.foodpals.R.layout.activity_main);


        resLoc = new Location("");
        userLoc = new Location("");


        mEvent = FirebaseDatabase.getInstance().getReference().child(ENVIRONMENT).child("Event").child("");
        mSwitch = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Switch");


        mSwitch.addValueEventListener(new ValueEventListener() {
            @Override

            public void onDataChange(DataSnapshot dataSnapshot) {
                resturant_switch = dataSnapshot.getValue().toString();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);

                        try {
                            List<Address> addresses = null;
                            Geocoder geo = new Geocoder(getApplicationContext(), Locale.getDefault());
                            addresses = geo.getFromLocation(Double.parseDouble(latitude), Double.parseDouble(longitude), 1);
                            if (addresses != null && addresses.size() > 0) {
                                Address address = addresses.get(0);
                                //for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                                String current_loca = address.getFeatureName() + "," + address.getLocality() + "," + address.getAdminArea() + "," + address.getCountryName();
                                // }
                                if (current_loca != null) {
                                    current_location(current_loca);
                                }

                                // current_location_header.setText(addresses.get(0).getFeatureName() + ", " + addresses.get(0).getLocality() + ", " + addresses.get(0).getAdminArea() + ", " + addresses.get(0).getCountryName());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    try {
                            if (latitude != null && longitude != null) {

                            userLoc.setLatitude(Double.parseDouble(latitude));
                            userLoc.setLongitude(Double.parseDouble(longitude));

                            if (resturant_switch.equals(null)) {
                                System.out.printf("Switch is not working ");
                            } else if (resturant_switch.equals("google")) {
                                if (userLoc != null) {
                                    new CallRestClient1().execute(userLoc);
                                }
                            } else if (resturant_switch.equals("zomato")) {
                                if (userLoc != null) {
                                    new CallRestClient().execute(userLoc);
                                }
                            }
                        }
                    } catch (NullPointerException e){
                            e.printStackTrace();
                    }


                        mEvent.addListenerForSingleValueEvent(new ValueEventListener() {

//                            Date date = new Date();
//                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                            String today_date = formatter.format(date);

                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {

                                    try {
                                    String key = childDataSnapshot.getKey();
                                    String d_event_date = childDataSnapshot.child("data_date").getValue().toString();


                                    if (dataSnapshot.exists()) {
//
//                                        String key = childDataSnapshot.getKey();

                                            if (key != null) {

                                                res_loc_lat = childDataSnapshot.child("data_resturant_lat").getValue().toString();
                                                res_loc_long = childDataSnapshot.child("data_resturant_long").getValue().toString();

                                                if (res_loc_long != null) {
                                                    resLoc.setLatitude(Double.parseDouble(res_loc_lat));
                                                    resLoc.setLongitude(Double.parseDouble(res_loc_long));
                                                    distanceBet = userLoc.distanceTo(resLoc) / 1000;

                                                    Log.i("lat", String.format("Distance fully '%s'", distanceBet));
                                                    if (d_event_date !=null ){

                                                        mEvent.child(key).child(Firebase_user_id).setValue(distanceBet);
                                                    }



                                                }
                                            }
                                    }

                                    Date date = new Date();
                                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                                    String today_date = formatter.format(date);

                                    try {
                                        Date today_date_1 = new SimpleDateFormat("dd/M/yyyy").parse(today_date);


                                        Date event_date__1 = new SimpleDateFormat("dd/M/yyyy").parse(d_event_date);

                                        long diffrence = (today_date_1.getTime() - event_date__1.getTime()) / 86400000;
                                        System.out.println(diffrence);

                                        if (diffrence == 1) {
                                            mEvent.child(key).removeValue();
                                        }
                                    } catch (ParseException e) {
                                        e.printStackTrace();}
                                    } catch (NullPointerException e) {
                                        e.printStackTrace();
                                    }

//                                    if (d_event_date.compareTo(today_date) >= 3) {
//
//                                        if (key1 == null) {
//
//                                        } else {
//                                            mEvent.child(key1).removeValue();
//
//
//                                        }
//
//                                        }
//                                    else if (d_event_date.compareTo(today_date) < 0) {
//
//                                        //mUserDatabase.child(current_event_key).getRef().setValue(null);
//                                        if (key1 == null) {
//
//                                        } else {
//                                            mEvent.child(key1).removeValue();
//                                        }
//                                    }else if (d_event_date.compareTo(today_date) == 0){
//
//
//                                    }

                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }


                        });

                    }
                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );


        Button currentButton = (Button) findViewById(com.createfirst.company.foodpals.R.id.current_button);
        lstPlaces = (ListView) findViewById(com.createfirst.company.foodpals.R.id.current_list);

        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                .build();
        mGoogleApiClient.connect();

        GRList = new ArrayList<>();
        GRList1 = new ArrayList<>();
        NextToken = new ArrayList<>();


        Toolbar toolbar = (Toolbar) findViewById(com.createfirst.company.foodpals.R.id.toolbar);
        setSupportActionBar(toolbar);


        BottomNavigationView navigation
                = (BottomNavigationView) findViewById(com.createfirst.company.foodpals.R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);


        mShimmerViewContainer = findViewById(com.createfirst.company.foodpals.R.id.shimmer_view_container);

        //Code to Inflate a new App bar inside the current Home screen
        ActionBar actionBar = getSupportActionBar();
        //actionBar.setTitle("Your Location");
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);


//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                Intent hosting_page = new Intent(getApplicationContext(), hostinf_resturant.class);
//                hosting_page.putExtra("surname", surname);
//                hosting_page.putExtra("imageUrl", imageUrl);
//                hosting_page.putExtra("middlename", middlename);
//                hosting_page.putExtra("Facebook_id", Facebook_id);
//                hosting_page.putExtra("Firebase_user_id", Firebase_user_id);
//                hosting_page.putExtra("Facebook_uri", Facebook_uri);
//                hosting_page.putExtra("user_email_id", user_email_id);
//                hosting_page.putExtra("name_fb", name_fb);
//
//                startActivity(hosting_page);
//                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
//
//            }
// /       });

        user_table = new Firebase("https://foodpals-7d9b1.firebaseio.com/users");

        DrawerLayout drawer = (DrawerLayout) findViewById(com.createfirst.company.foodpals.R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, com.createfirst.company.foodpals.R.string.navigation_drawer_open, com.createfirst.company.foodpals.R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // Slider View

        NavigationView navigationView = (NavigationView) findViewById(com.createfirst.company.foodpals.R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        TextView text = (TextView) header.findViewById(com.createfirst.company.foodpals.R.id.fb_username);
        CircleImageView profile_image = (CircleImageView) header.findViewById(com.createfirst.company.foodpals.R.id.profile_image);


        profile_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent profile_screen = new Intent(MainActivity.this, profile_settings_activity.class);
                startActivity(profile_screen);
                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_right, com.createfirst.company.foodpals.R.anim.slide_out_left);

            }
        });


        Bundle inBundle = getIntent().getExtras();
        name_fb = inBundle.get("name_fb").toString();
        surname = inBundle.get("surname").toString();
        imageUrl = inBundle.get("imageUrl").toString();
        middlename = inBundle.get("middlename").toString();
        Facebook_id = inBundle.get("userid").toString();
        Firebase_user_id = inBundle.get("Firebase_User_id").toString();
        Facebook_uri = inBundle.get("facebook_uri").toString();
        user_email_id = inBundle.get("Email_id").toString();
        Full_name = inBundle.get("Full_name").toString();
        String deviceToken_id = FirebaseInstanceId.getInstance().getToken();





        /*TextView nameView = (TextView)findViewById(R.id.fb_username);*/
        text.setText("" + name_fb + " " + middlename + " " + surname);

//        // sending User Data to firebase table

        mdatabase = FirebaseDatabase.getInstance().getReference().child(ENVIRONMENT).child("User").child(Firebase_user_id);
        mdatabaseforrequsers = FirebaseDatabase.getInstance().getReference().child(ENVIRONMENT).child("Request_user").child(Firebase_user_id);


        HashMap<String, String> reqMap = new HashMap<>();
        reqMap.put("First_Name", name_fb);
        reqMap.put("Middle_Name", middlename);
        reqMap.put("Last_name", surname);
        reqMap.put("Profile_image", imageUrl);
        reqMap.put("Firebase_id", Firebase_user_id);
        reqMap.put("Facebook_id", Facebook_id);
        reqMap.put("Facebook_uri", Facebook_uri);
        reqMap.put("Email_id", user_email_id);
        reqMap.put("Full_name", Full_name);
        mdatabaseforrequsers.setValue(reqMap);


        HashMap<String, String> userMap = new HashMap<>();
        userMap.put("First_Name", name_fb);
        userMap.put("Middle_Name", middlename);
        userMap.put("Last_name", surname);
        userMap.put("Profile_image", imageUrl);
        userMap.put("Firebase_id", Firebase_user_id);
        userMap.put("Facebook_id", Facebook_id);
        userMap.put("Facebook_uri", Facebook_uri);
        userMap.put("Email_id", user_email_id);
        userMap.put("device_token", deviceToken_id);
        userMap.put("Full_name", Full_name);
        mdatabase.setValue(userMap);



       /* //To get the current Location Permission
        if (checkPermissions()){
            callPlaceDetectionApi();

        }


        checkConnection();
*/


        new MainActivity.DownloadImage((CircleImageView) header.findViewById(com.createfirst.company.foodpals.R.id.profile_image)).execute(imageUrl);

        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.

    }


   /* public void callPlaceDetectionApi() throws SecurityException {
        // checkServiceEnable();
        PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(mGoogleApiClient, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void  onResult(PlaceLikelihoodBuffer likelyPlaces) {
                //Toast.makeText(MainActivity.this, "hioi", Toast.LENGTH_SHORT).show();
                ArrayAdapter myAdapter = new ArrayAdapter(getApplicationContext(), android.R.layout.simple_list_item_1);
                ArrayAdapter myAdapter1 = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_2);
                //ArrayAdapter myAdapter2 = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_2);
                for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                    //Toast.makeText(MainActivity.this, "Hoiha", Toast.LENGTH_SHORT).show();
                 *//*   if(placeLikelihood.getLikelihood()>0){*//*
                    myAdapter.add(placeLikelihood.getPlace().getAddress().toString());
                    current_loca =(String) myAdapter.getItem(0);

                    myAdapter1.add(placeLikelihood.getPlace().getLatLng());
                    latLng = (LatLng) myAdapter1.getItem(0);
                    curret_latitude = latLng.latitude;
                    //myAdapter1.add(placeLikelihood.getPlace().getLatLng().longitude);
                     curret_longitude = latLng.longitude;

                    Log.i(LOG_TAG, String.format("latlng'%s'", latLng));
                    Log.i(LOG_TAG, String.format("Place '%s' with " +  "likelihood: %g", placeLikelihood.getPlace().getAddress(), placeLikelihood.getLikelihood()));
                }


                if(resturant_switch.equals(null)){

                    System.out.printf("switch is fucled");

                }
                else if(resturant_switch.equals("google")){
                    if(latLng!=null){
                    new CallRestClient1().execute(latLng);
                }else if(latLng==null){
                        callPlaceDetectionApi();
                        new CallRestClient1().execute(latLng);
                    }
                      }
                else if(resturant_switch.equals("zomato")){
                    if(latLng!=null){
                        new CallRestClient().execute(latLng);
                    }else if(latLng==null){
                        callPlaceDetectionApi();
                        new CallRestClient().execute(latLng);
                    }
                }

                // curr_Place.setText(current_loca);
                //lstPlaces.setAdapter(myAdapter);
                likelyPlaces.release();
                current_location();
            }
        });

    }*/


    // Showing the status in Snackbar
    private void no_internet(boolean isConnected) {


        final Dialog no_internet_popup = new Dialog(MainActivity.this);
        no_internet_popup.setContentView(com.createfirst.company.foodpals.R.layout.nointernet_custom_popup);

        //Initializing the Pop up codes


        if (isConnected) {
//            message = "Good! Connected to Internet";
//            color = Color.WHITE;
        } else {


            no_internet_popup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            no_internet_popup.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
            no_internet_popup.show();
        }

    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: " + connectionResult.getErrorCode());
        Toast.makeText(this, "Google Places API connection failed with error code:" + connectionResult.getErrorCode(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        no_internet(isConnected);

    }


    public class CallRestClient extends AsyncTask<Location, Void, String> implements Serializable {

        final String GOOGLE_KEY = "AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q";
        final String ZOMATO_KEY = "51868734cdb381c3dd872167bcd82cb8";


        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected String doInBackground(Location... locations) {
            final Uri.Builder builder1 = new Uri.Builder().scheme("https").authority("developers.zomato.com").path("api/v2.1/search");

            builder1.appendQueryParameter("entity_id", "4")
                    .appendQueryParameter("entity_type", "city")
                    .appendQueryParameter("q", "food")
                    .appendQueryParameter("start", "0")
                    .appendQueryParameter("count", "20")
                    .appendQueryParameter("lat", String.valueOf(locations[0].getLatitude()))
                    .appendQueryParameter("lon", String.valueOf(locations[0].getLongitude()))
                    .appendQueryParameter("radius", "1000")
                    .appendQueryParameter("cuisines", "6%2C152%2C1%2C2%2C4%2C3%2C165%2C131%2C292%2C193%2C5%2C227%2C10%2C270%2C1013%2C7%2C133%2C247%2C168%2C22%2C30%2C121%2C994%2C18%2C25%2C161%2C35%2C100%2C260%2C38%2C40%2C271%2C45%2C134%2C47%2C156%2C181%2C48%2C143%2C49%2C233%2C148%2C114%2C140%2C218%2C55%2C60%2C265%2C164%2C65%2C178%2C62%2C63%2C67%2C66%2C157%2C102%2C69%2C72%2C70%2C73%2C137%2C1015%2C1018%2C74%2C147%2C75%2C166%2C117%2C231%2C50%2C278%2C269%2C290%2C82%2C87%2C88%2C320%2C27%2C1005%2C1023%2C84%2C998%2C304%2C83%2C993%2C119%2C461%2C972%2C85%2C89%2C141%2C90%2C177%2C212%2C163%2C150%2C95%2C978%2C1024%2C99%2C308%2C1034%2C142%2C93")
                    .appendQueryParameter("establishment_type", "16%2C18%2C21%2C31%2C23%2C5%2C7%2C1%2C20%2C6%2C8")
                    .appendQueryParameter("collection_id", "1%2C274852%2C29%2C283631%2C74045%2C2%2C267733%2C339%2C277744%2C269279%2C274826%2C86%2C3%2C277%2C13%2C19%2C20%2C75%2C9%2C51%2C438%2C12%2C337%2C225%2C58%2C48%2C40%2C10852%2C344%2C6%2C340%2C5%2C7%2C99")
                    .appendQueryParameter("category", "2%2C3")
                    .appendQueryParameter("sort", "real_distance")
                    .appendQueryParameter("order", "desc")
                    //.appendQueryParameter("key", ZOMATO_KEY)
                    .build();
            String url = builder1.toString();

            Log.i("URL", String.format("URL'%s'", url));
            return RestClient.makeHttpCall(url);
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (userLoc!= null) {
                GRList1 = RestClient.parseJsonInBO1(s);
                Log.i("Chutiya", String.format("restaurantList1'%s'", GRList1));
                bundle = new Bundle();
                bundle.putParcelableArrayList("restaurantList1", (ArrayList<ZomatoParameter>) GRList1);

                if (bundle != null) {
                    ResturantFragment resturantFragment = new ResturantFragment();
                    resturantFragment.setArguments(bundle);
                    getSupportFragmentManager().beginTransaction().replace(com.createfirst.company.foodpals.R.id.place_id, resturantFragment).commit();

                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    //current_location();
                } else {

                    Toast.makeText(MainActivity.this, "Bundle is not Set", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public class CallRestClient1 extends AsyncTask<Location, Void, String> implements Serializable {

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        protected String doInBackground(Location... locations) {
            final Uri.Builder builder1 = new Uri.Builder().scheme("https").authority("maps.googleapis.com").path("maps/api/place/nearbysearch/json");
                    /*.appendPath("maps").appendPath("api")
                    .appendPath("place").appendPath("nearbysearch").appendPath("json");
                    String locaLat = String.valueOf(locations[0].getLatitude()) + String.valueOf(locations[0].getLongitude());
                   */

            builder1.appendQueryParameter("location", String.valueOf(locations[0].getLatitude()) + "," + String.valueOf(locations[0].getLongitude()))/*"13.0171449,77.586097"*/
                    .appendQueryParameter("radius", "1000")
                    .appendQueryParameter("type", "restaurant,cusinie")
                    .appendQueryParameter("keyword", "cafe,restaurant")
                    .appendQueryParameter("key", GOOGLE_KEY);
            urlG = builder1.build().toString();


            //String url1 = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="+latLng[0].latitude+","+latLng[0].longitude+"&radius=500&type=restaurant,cusinie&keyword=food&pagetoken&key=" + GOOGLE_KEY;
            //String url = "https://developers.zomato.com/api/v2.1/search?entity_id=4&entity_type=subzone&q=food&start=0&count=20&lat="+latLng[0].latitude+"&lon="+latLng[0].longitude+"&radius=100&cuisines=6,152,1,2,4,3,165,131,292,193,5,227,10,270,1013,7,133,247,168,22,30,121,994,18,25,161,35,100,260,38,40,271,45,134,47,156,181,48,143,49,233,148,114,140,218,55,60,265,164,65,178,62,63,67,66,157,102,69,72,70,73,137,1015,1018,74,147,75,166,117,231,50,278,269,290,82,87,88,320,27,1005,1023,84,998,304,83,993,119,461,972,85,89,141,90,177,212,163,150,95,978,1024,99,308,1034,142,93&establishment_type=16,18,21,31,23,5,7,1,20,6,8&collection_id=1,274852,29,283631,74045,2,267733,339,277744,269279,274826,86,3,277,13,19,20,75,9,51,438,12,337,225,58,48,40,10852,344,6,340,5,7,99&category=2,3&sort=real_distance&order=desc";
            //String url = "https://developers.zomato.com/api/v2.1/search?entity_id=4&entity_type=subzone&q=food&start=0&count=100&"+latLng[0].latitude+"&lon="+latLng[0].longitude+"&radius=100&cuisines=Indian%252CChinese%252CMexican%252CArabian%252CEuropean&establishment_type=16&collection_id=1&category=2&sort=real_distance&order=desc";
            Log.i("URL", String.format("URL'%s'", urlG));
            return RestClient1.makeHttpCall(urlG, getApplicationContext());
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (userLoc != null) {
                GRList = RestClient1.parseJsonInBO1(s);
                NextToken = NextTokenRestClient.parseJsonNextToken(s);

                Log.i("jsonList", String.format("restaurantList'%s'", GRList));
                bundle1 = new Bundle();
                bundle1.putParcelableArrayList("restaurantList", (ArrayList<GoogleParameters>) GRList);

                bundle1.putParcelableArrayList("nextToken", (ArrayList<? extends Parcelable>) NextToken);
                //bundle1.putString("GoogleBuilder",String.valueOf(builder1));
                bundle1.putString("GoogleURL", urlG);
                bundle1.putParcelable("userLoca", userLoc);
                Log.i("NextToken", String.format("Next '%s'", NextToken));

                if (bundle1 != null) {
                    ResturantFragment resturantFragment = new ResturantFragment();
                    resturantFragment.setArguments(bundle1);

                    getSupportFragmentManager().beginTransaction().replace(com.createfirst.company.foodpals.R.id.place_id, resturantFragment).commit();
                    mShimmerViewContainer.stopShimmerAnimation();
                    mShimmerViewContainer.setVisibility(View.GONE);
                    //current_location();
                } else {
                    Toast.makeText(MainActivity.this, "Bundle is not Set", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        // = new BottomNavigationView.OnNavigationItemSelectedListener() {


        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            //sv = (ScrollView) findViewById(R.id.DBscrollview);
            switch (item.getItemId()) {
                case com.createfirst.company.foodpals.R.id.navigation_home:
//                    mShimmerViewContainer.setVisibility(View.VISIBLE);
//                    mShimmerViewContainer.startShimmerAnimation();
                    item.setIcon(com.createfirst.company.foodpals.R.drawable.ic_home_black_24dp);

                    //Log.i(LOG_TAG, String.format("restaurantList'%s'", bundle.toString()));
                    if (bundle != null) {
                        ResturantFragment resturantFragment = new ResturantFragment();
                        resturantFragment.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction().replace(com.createfirst.company.foodpals.R.id.place_id, resturantFragment).commit();
                    } else {
                        if (userLoc != null && resturant_switch.equals("google")) {
                            new CallRestClient1().execute(userLoc);
                            //current_location(address);
                        } else if (userLoc != null && resturant_switch.equals("zomato")) {
                            new CallRestClient().execute(userLoc);
                        }
                    }
                    return true;


                case com.createfirst.company.foodpals.R.id.navigation_dashboard:
//                    mShimmerViewContainer.setVisibility(View.VISIBLE);
//                    mShimmerViewContainer.startShimmerAnimation();

                    EventFragment eventFragment = new EventFragment();

                    bundleUserLoc = new Bundle();
                    bundleUserLoc.putDouble("userLat", userLoc.getLatitude());
                    bundleUserLoc.putDouble("userLong", userLoc.getLongitude());
                    if (bundleUserLoc != null) {
                        eventFragment.setArguments(bundleUserLoc);
                        getSupportFragmentManager().beginTransaction().replace(com.createfirst.company.foodpals.R.id.place_id, eventFragment).commit();
                    }

                    return true;
            }
            return false;
        }


    };


    public class DownloadImage extends AsyncTask<String, Void, Bitmap> {
        CircleImageView bmImage;

        public DownloadImage(CircleImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIconll = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIconll = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();

            }
            return mIconll;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(com.createfirst.company.foodpals.R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(com.createfirst.company.foodpals.R.menu.main, menu);
        // MenuItem currplace_item = menu.findItem(R.id.action_place);
        // curr_Place = (TextView) MenuItemCompat.getActionView(currplace_item);
        return true;
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        if (id == com.createfirst.company.foodpals.R.id.action_searchbar) {

            Toast.makeText(this, "search", Toast.LENGTH_SHORT).show();
            // Toast.makeText(this, "camera", Toast.LENGTH_SHORT).show();
            // Handle the camera action
        }
//        else if (id == R.id.action_searchbar) {
//
//
//        }
//        else if (id == R.id.action_notifcation) {
//            current_location();
//        }

        return super.onOptionsItemSelected(item);
    }

  /*  public boolean buildConnectionWithGoogleAPI() {
        Log.d("gpsI", "buildConnectionWithGoogleAPI i am getting called");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this)
                    .addApi(Places.PLACE_DETECTION_API)
                    .enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                    .build();
        }
        mGoogleApiClient.connect();
        return true;
    }*/

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void current_location(String current_loca) {
        ActionBar actionBar = getSupportActionBar();

        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View action_bar_view = inflater.inflate(com.createfirst.company.foodpals.R.layout.homescreen_custom_appbar, null);
        actionBar.setCustomView(action_bar_view);

        TextView your_location = (TextView) action_bar_view.findViewById(com.createfirst.company.foodpals.R.id.actionbarTitle);
        current_location_header = (TextView) action_bar_view.findViewById(com.createfirst.company.foodpals.R.id.subtitle_actionbar);
        current_location_header.setText(current_loca);
        Toast.makeText(MainActivity.this, current_loca, Toast.LENGTH_SHORT).show();

        LinearLayout checklinearLayout = (LinearLayout) action_bar_view.findViewById(com.createfirst.company.foodpals.R.id.appbar_linearlayout);


        checklinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(MainActivity.this, "hioi", Toast.LENGTH_SHORT).show();
                Log.w("MainActivity", "ActionBar's title clicked.");
                try {
                    startActivityForResult(builder.build(MainActivity.this), PLACE_PICKER_REQUEST);
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    //Get the current location name address from here
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                address = String.format("%s", place.getAddress());
                LatLng latlangObj = place.getLatLng();
                Location changed_location = new Location("");
                changed_location.setLatitude(latlangObj.latitude);
                changed_location.setLongitude(latlangObj.longitude);

                if (changed_location != null && resturant_switch.equals("google")) {
                    new CallRestClient1().execute(changed_location);
                    current_location_header.setText(address);
                } else if (changed_location != null && resturant_switch.equals("zomato")) {
                    new CallRestClient().execute(changed_location);
                    current_location_header.setText(address);
                }

            }

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == com.createfirst.company.foodpals.R.id.Resturants) {
//            Intent host = new Intent(MainActivity.this, hostinf_resturant.class);
//            host.putExtra("surname", surname);
//            host.putExtra("imageUrl", imageUrl);
//            host.putExtra("middlename", middlename);
//            host.putExtra("Facebook_id", Facebook_id);
//            host.putExtra("Firebase_user_id", Firebase_user_id);
//            host.putExtra("Facebook_uri", Facebook_uri);
//            host.putExtra("user_email_id", user_email_id);
//            host.putExtra("name_fb", name_fb);
//            startActivity(host);
            // Toast.makeText(this, "camera", Toast.LENGTH_SHORT).show();
            // Handle the camera action
        } else if (id == com.createfirst.company.foodpals.R.id.Meet) {
//            Toast.makeText(this, "annad", Toast.LENGTH_SHORT).show();
//            Intent host = new Intent(MainActivity.this, EventFragment.class);
//            startActivity(host);


            EventFragment eventFragment = new EventFragment();

            bundleUserLoc = new Bundle();
            bundleUserLoc.putDouble("userLat", userLoc.getLatitude());
            bundleUserLoc.putDouble("userLong", userLoc.getLongitude());
            if (bundleUserLoc != null) {
                eventFragment.setArguments(bundleUserLoc);
                getSupportFragmentManager().beginTransaction().replace(com.createfirst.company.foodpals.R.id.place_id, eventFragment).commit();
            }

        } else if (id == com.createfirst.company.foodpals.R.id.nav_share) {

            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);

        } else {
            if (id == com.createfirst.company.foodpals.R.id.logout_button) {
                LoginManager.getInstance().logOut();
                Intent login = new Intent(MainActivity.this, fb_log.class);
                startActivity(login);
                finish();

            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(com.createfirst.company.foodpals.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();


        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

        mdatabase.child("online").setValue("true");
        mdatabase.child("last_seen_open").setValue(ServerValue.TIMESTAMP);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client.connect();

        // checkServiceEnable();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.company.foodpals/http/host/path")
        );
//        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    protected void onResume() {
        super.onResume();

        ConnectivityChecker.getInstance().setConnectivityListener(this);
        mShimmerViewContainer.startShimmerAnimation();
        //  checkServiceEnable();
        startStep1();
    }


    /**
     * Step 1: Check Google Play services
     */
    private void startStep1() {

        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {

            //Passing null to indicate that it is executing for the first time.
            startStep2(null);

        } else {
            Toast.makeText(getApplicationContext(), com.createfirst.company.foodpals.R.string.no_google_playservice_available, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * Step 2: Check & Prompt Internet connection
     */
    private Boolean startStep2(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }


        if (dialog != null) {
            dialog.dismiss();
        }

        //Yes there is active internet connection. Next check Location is granted by user or not.

        if (checkPermissions()) { //Yes permissions are granted by the user. Go to the next step.
            startStep3();
        } else {  //No user has not granted the permissions yet. Request now.
            requestPermissions();
        }
        return true;
    }

    /**
     * Show A Dialog with button to refresh the internet state.
     */
    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(com.createfirst.company.foodpals.R.string.title_alert_no_intenet);
        builder.setMessage(com.createfirst.company.foodpals.R.string.msg_alert_no_internet);

        String positiveText = getString(com.createfirst.company.foodpals.R.string.btn_label_refresh);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        //Block the Application Execution until user grants the permissions
                        if (startStep2(dialog)) {

                            //Now make sure about location permission.
                            if (checkPermissions()) {

                                //Step 2: Start the Location Monitor Service
                                //Everything is there to start the service.
                                startStep3();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }

                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * Step 3: Start the Location Monitor Service
     */
    private void startStep3() {

        //And it will be keep running until you close the entire application from task manager.
        //This method will executed only once.

        if (!mAlreadyStartedService && userLoc != null) {

            //Start location sharing service to app server.........
            Intent intent = new Intent(this, LocationMonitoringService.class);
            startService(intent);

            mAlreadyStartedService = true;
            //Ends................................................
        }
    }

    /**
     * Return the availability of GooglePlayServices
     */
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    /**
     * Return the current state of the permissions needed.
     */
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    /**
     * Start permissions requests.
     */
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_COARSE_LOCATION);


        // Provide an additional rationale to the img_user. This would happen if the img_user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale || shouldProvideRationale2) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(com.createfirst.company.foodpals.R.string.permission_rationale,
                    android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MainActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the img_user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private void showSnackbar(final int mainTextStringId, final int actionStringId,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If img_user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                Log.i(TAG, "Permission granted, updates requested, starting location updates");
                startStep3();

            } else {
                // Permission denied.

                // Notify the img_user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the img_user for permission (device policy or "Never ask
                // again" prompts). Therefore, a img_user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(com.createfirst.company.foodpals.R.string.permission_denied_explanation,
                        com.createfirst.company.foodpals.R.string.settings, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }

    @Override
    public void onPause() {
        mShimmerViewContainer.stopShimmerAnimation();
        mSwitch.addValueEventListener(new ValueEventListener() {
            @Override

            public void onDataChange(DataSnapshot dataSnapshot) {
                resturant_switch = dataSnapshot.getValue().toString();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        super.onPause();

    }


    @Override
    public void onStop() {
        super.onStop();


        mdatabase.child("online").setValue(ServerValue.TIMESTAMP);


        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.example.company.foodpals/http/host/path")
        );
//        AppIndex.AppIndexApi.end(client, viewAction);
        //      client.disconnect();
    }

    public void onDestroy() {

        super.onDestroy();
        finish();

        Log.d("Destroy", "Destorying the Whole Activity");

    }


}













    /*class ReadJSON extends AsyncTask<String, Integer, String> implements Serializable {

        @Override
        protected String doInBackground(String... params) {
            return readURL(params[0]);
        }


        @Override
        protected void onPostExecute(String content) {
            try {
                JSONObject jsonObject = new JSONObject(content);
                JSONArray jsonArray =  jsonObject.getJSONArray("results");

                for(int i =0;i<jsonArray.length(); i++){
                    JSONObject gpObject = jsonArray.getJSONObject(i);
                    GoogleParameters restaurant = new GoogleParameters();
                    if(gpObject.has("name")) {
                        restaurant.setName(gpObject.get("name").toString());
                    }

                    if(gpObject.has("rating")) {
                        restaurant.setRating(Float.parseFloat(gpObject.get("rating").toString()));
                    }
                    else {
                        restaurant.setRating(Float.parseFloat("0"));
                    }
                    if(gpObject.has("price_level")) {
                        restaurant.setPrice_level(Integer.parseInt((gpObject.get("price_level").toString())));
                    }
                    if(gpObject.has("vicinity")) {
                        restaurant.setAddress(gpObject.get("vicinity").toString());
                    }
                    if(gpObject.has("geometry")) {
                        restaurant.setLat(Double.parseDouble(gpObject.getJSONObject("geometry").getJSONObject("location").get("lat").toString()));
                        restaurant.setLng(Double.parseDouble(gpObject.getJSONObject("geometry").getJSONObject("location").get("lng").toString()));
                    }

                    if(gpObject.has("photos")) {
                        restaurant.setPhoto_reference(gpObject.getJSONArray("photos").getJSONObject(0).getString("photo_reference").toString());
                    }
                    if(gpObject.has("opening_hours")){
                        restaurant.setOpen_now(gpObject.getJSONObject("opening_hours").getBoolean("open_now"));
                    }
//arrayList.add(restaurant);
                    GRList.add(restaurant);
                    bundle = new Bundle();
                    bundle.putParcelableArrayList("restaurantList",(ArrayList<GoogleParameters>) GRList);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
*//*final GoogleResAdapter adapter1 = new GoogleResAdapter(getApplicationContext(),GRList);
recyclerView = (RecyclerView) findViewById(R.id.
recycler_view_card);

final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
recyclerView.setLayoutManager(layoutManager);



recyclerView.setAdapter(adapter1);*//*

        }

    }


    private static String readURL(String theUrl) {
        StringBuilder content = new StringBuilder();
        try {
// create a url object
            URL url = new URL(theUrl);
// create a urlconnection object
            URLConnection urlConnection = url.openConnection();
// wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String line;
// read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return content.toString();
    }*/







       /* shareDialog = new ShareDialog(this);
        FloatingActionButton faab = (FloatingActionButton) findViewById(R.id.fab);
        faab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
      /*          ShareLinkContent content = new ShareLinkContent().Builder().build();
                shareDialog.show(content);

            }
        });*/

//   actionBar.setTitle(Html.fromHtml("<small>Your Location</small>"));
// actionBar.setTitle(Html.fromHtml("<small>Your Location </small>"));


//


//        currentButton.setOnClickListener(new View.OnClickListener() {
//            @RequiresApi(api = Build.VERSION_CODES.M)
//            @Override
//            public void onClick(View view) {
//                if (mGoogleApiClient.isConnected()) {
//                    int hasPermission = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);
//                    if (hasPermission != PackageManager.PERMISSION_GRANTED) {
//                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
//                    } else {
//                        callPlaceDetectionApi();
//                    }
//                }
//            }
//        });


//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


//lv = (ListView) findViewById(R.id.listView);
//URL for Fetchinh Resturant from Google PLaces
/*runOnUiThread(new Runnable() {
@Override
public void run() {

    *//*double an = 10000;

String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=12.9081,77.6476&radius="+ an +"&type=restaurant,cusinie&keyword=food&pagetoken&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q";
new ReadJSON().execute(url);*//*
    CallRestClient callRestClient = new CallRestClient();
    callRestClient.execute();

}

});*/
//setContentView(R.layout.nav_header_main);


//    public boolean checkServiceEnable() {
//
//        int PERMISSION_DENIED=0;
//        try {
//            if (Build.VERSION.SDK_INT >= 23) {
//                if (ContextCompat.checkSelfPermission(MainActivity.this,
//                        android.Manifest.permission.ACCESS_FINE_LOCATION)
//                        != PackageManager.PERMISSION_GRANTED) {
//
//                    // Should we show an explanation?
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
//                            android.Manifest.permission.ACCESS_FINE_LOCATION)) {
//
//                        ActivityCompat.requestPermissions(MainActivity.this,
//                                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
//                        // Show an explanation to the user *asynchronously* -- don't block
//                        // this thread waiting for the user's response! After the user
//                        // sees the explanation, try again to request the permission.
//
//                    } else {
//
//                        ActivityCompat.requestPermissions(MainActivity.this,
//                                new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
//                        //ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
//                        // app-defined int constant. The callback method gets the
//                        // result of the request.
//                    }
//                }else{
//                    return true;
//                }
//            }else{
//                return true;
//            }
//        }catch(Exception e){e.printStackTrace();}
//        onRequestPermissionsResult(100,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_GRANTED);
//        return true;
//    }
//
//    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        switch(requestCode){
//            case PERMISSION_REQUEST_CODE:
//                if((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)){
//                    //checkServiceEnable();
//                    callPlaceDetectionApi();
//                }else{
//                    Toast.makeText(MainActivity.this, "ACCESS_FINE_LOCATION Denied", Toast.LENGTH_SHORT)
//                            .show();
//                }
//                return;
//
//            default:
//                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        }
//    }


//        if(checkServiceEnable()){
//            onRequestPermissionsResult(100,new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},PERMISSION_GRANTED);
//        }
//        else{
//            callPlaceDetectionApi();
//        }

