package com.createfirst.company.foodpals;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class profile_settings_activity extends AppCompatActivity {

    //Firebase Data
    private DatabaseReference mUserDatabase;
    private FirebaseUser mCurrentUser;
    public String name_fb;
    public  String surname ;
    public  String imageUrl ;
    public  String middlename ;
    public  String user_facebook_id ;
    public  String firebase_user_id ;
    public  String user_facebook_uri ;
    public  String user_email_id ;


    //Androdid Layout
    private CircleImageView mDisplayImage;
    private TextView mName;
    private TextView mEmail;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.user_profilescreen);

        //To change the colour of the status bar
       // Utils.darkenStatusBar(this, R.drawable.side_nav_bar);

        Toolbar mChatToolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mChatToolbar.setTitleTextColor(Color.WHITE);


        //Back arrow colour
        Drawable backArrow = getResources().getDrawable(com.createfirst.company.foodpals.R.drawable.ic_arrow_back_black_24dp);
        backArrow.setColorFilter(getResources().getColor(com.createfirst.company.foodpals.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);

        mDisplayImage = (CircleImageView) findViewById(com.createfirst.company.foodpals.R.id.profile_image_screen) ;
        mName = (TextView) findViewById(com.createfirst.company.foodpals.R.id.profile_user_name);
        mEmail = (TextView) findViewById(com.createfirst.company.foodpals.R.id.user_email);


        mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();

        String current_user_id = mCurrentUser.getUid();

        // getting current user id and fetching it from db

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(current_user_id);

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                name_fb = dataSnapshot.child("First_Name").getValue().toString();
                surname = dataSnapshot.child("Last_name").getValue().toString();
                imageUrl = dataSnapshot.child("Profile_image").getValue().toString();
                middlename = dataSnapshot.child("Middle_Name").getValue().toString();
                user_facebook_id = dataSnapshot.child("Facebook_id").getValue().toString();
                firebase_user_id = dataSnapshot.child("Firebase_id").getValue().toString();
                user_facebook_uri = dataSnapshot.child("Facebook_uri").getValue().toString();
                user_email_id = dataSnapshot.child("Email_id").getValue().toString();


                mName.setText("" + name_fb + " " + middlename+ " " + surname);
                mEmail.setText(user_email_id);

                Picasso.with(profile_settings_activity.this).load(imageUrl).placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(mDisplayImage);







            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    //Navigation Back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
                return true;



        }

        return super.onOptionsItemSelected(item);
    }


}
