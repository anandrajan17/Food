package com.createfirst.company.foodpals;

import android.app.Application;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by anandhaa on 02/08/17.
 */

public class Firebase_FoodPals extends Application {

    private DatabaseReference mUserDatabase;
    private FirebaseAuth mAuth;



    public void onCreate() {

        super.onCreate();

        Firebase.setAndroidContext(this);


        FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        mAuth = FirebaseAuth.getInstance();

        if (mAuth.getCurrentUser() != null) {
            mUserDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User").child(mAuth.getCurrentUser().getUid());

        mUserDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                if(dataSnapshot != null){

                    mUserDatabase.child("online").onDisconnect().setValue(false);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        }
    }
}
