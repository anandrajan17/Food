package com.createfirst.company.foodpals;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by anandhaa on 10/03/18.
 */

public class tutorial_SliderAdapter extends PagerAdapter {

    Context context;

    LayoutInflater layoutInflater;

    public tutorial_SliderAdapter(Context context){

        this.context = context;
    }


    //Array

    public int[] tutorial_icon_slide = {

            com.createfirst.company.foodpals.R.drawable.meet_icon,
            com.createfirst.company.foodpals.R.drawable.eat_icon,
            com.createfirst.company.foodpals.R.drawable.split_icon
    };

    public String[] tutorial_heading_slide = {

            "MEET",
            "EAT",
            "SPLIT"

    };


    @Override
    public int getCount() {
        return tutorial_heading_slide.length ;
    }

    @Override
    public boolean isViewFromObject(View view, Object o) {
        return view == (RelativeLayout) o;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(com.createfirst.company.foodpals.R.layout.tutorial_slide_layout, container, false);

        ImageView icon_slide =  view.findViewById(com.createfirst.company.foodpals.R.id.tutorial_icon);
        TextView  heading_slide = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.tutorial_heading);


        icon_slide.setImageResource(tutorial_icon_slide[position]);
        heading_slide.setText(tutorial_heading_slide[position]);

        container.addView(view);
        return view;


    }


    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
       container.removeView((RelativeLayout)object);
    }
}
