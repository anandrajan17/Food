package com.createfirst.company.foodpals.mFragment;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.createfirst.company.foodpals.Firebase_Event;
import com.createfirst.company.foodpals.MainActivity;
import com.createfirst.company.foodpals.R;
import com.createfirst.company.foodpals.hosted_main_activity;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.firebase.geofire.GeoFire;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

//import android.support.v4.util.Pair;

public class EventFragment extends Fragment {

    private RecyclerView mUserList;
    private DatabaseReference mUserDatabase;

    public String current_event_key_1;
    public double userLat, userLong;
    public Location userLoc, resLoc;
    public ArrayList<String> resLat, resLong;
    public float distanceBet;
    public String cut_dis, distance;

    public ArrayList<Float> finaldistance;

    public List finalDis;

    public View mview;

    private ShimmerFrameLayout mShimmerViewContainer;

    GeoFire geoFire;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {



        mUserDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Event");

        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Event");
        geoFire = new GeoFire(ref);


        View rootView=inflater.inflate(R.layout.content_main,container,false);

        //Initialize the the activity elements here ####
        mUserList = (RecyclerView) rootView.findViewById(R.id.recycler_view_card);
        mUserList.setHasFixedSize(true);
        mUserList.setLayoutManager(new LinearLayoutManager(EventFragment.this.getActivity()));

        userLoc = new Location("");
        userLat = getArguments().getDouble("userLat");
        userLong = getArguments().getDouble("userLong");
        userLoc.setLatitude(userLat);
        userLoc.setLongitude(userLong);


       /* LocalBroadcastManager.getInstance(EventFragment.this.getActivity()).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        String latitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LATITUDE);
                        String longitude = intent.getStringExtra(LocationMonitoringService.EXTRA_LONGITUDE);

                        if (latitude != null && longitude != null) {
                            userLoc.setLatitude(Double.parseDouble(latitude));
                            userLoc.setLongitude(Double.parseDouble(longitude));
                        }
                    }
                }, new IntentFilter(LocationMonitoringService.ACTION_LOCATION_BROADCAST)
        );
*/
        return rootView;
    }


    public static class UserViewHolder extends RecyclerView.ViewHolder {

        View mview;
        public CircleImageView userprofilepic;
        public TextView userNameView;
        public TextView resturant_name;
        public TextView card_date;
        public TextView card_time;
        public TextView resturant_distance;

        public ShimmerFrameLayout eventshimmeranimation;

        public UserViewHolder(View itemView) {
            super(itemView);

            mview = itemView;

        }


        //create method to set new value for the views
        public void setName(String name){

            userNameView = (TextView) mview.findViewById(R.id.name_eventcard);

            userNameView.setText(name);

        }

        public void setProfilepic(String profileimageurl){

            userprofilepic = (CircleImageView) mview.findViewById(R.id.profile_pic_eventcard);



            Picasso.with(userprofilepic.getContext()).load(profileimageurl)
                    .placeholder(R.drawable.profile_image_new).into(userprofilepic);


        }

        public void setResturantname(String resturantname){

            resturant_name = (TextView) mview.findViewById(R.id.card_resturant_name);

            resturant_name.setText(resturantname);
        }

        public void setDistance(String resturantdistance){
            resturant_distance = (TextView) mview.findViewById(R.id.card_distance);

            resturant_distance.setText(resturantdistance);


        }


        public void setDate(String event_date){
            card_date = (TextView) mview.findViewById(R.id.card_date);
            card_date.setText(event_date);

        }

        public void setTime(String event_time){
            card_time = (TextView) mview.findViewById(R.id.card_time);
            card_time.setText(event_time);

        }

        public void start_shimmer_animation(){
            eventshimmeranimation = mview.findViewById(R.id.event_shimmer);
            eventshimmeranimation.startShimmerAnimation();
        }

        public void stop_shimmer_animation(){
            eventshimmeranimation = mview.findViewById(R.id.event_shimmer);
            eventshimmeranimation.stopShimmerAnimation();
        }





    }

    @Override
    public void onStart() {
        super.onStart();

        resLoc = new Location("");
        resLat = new ArrayList();
        resLong = new ArrayList<>();


         FirebaseAuth mAuth;

       String Firebase_user_id ;



        mAuth = FirebaseAuth.getInstance();

        FirebaseUser current_user = mAuth.getCurrentUser();
        Firebase_user_id = current_user.getUid().toString();







        final FirebaseRecyclerAdapter<Firebase_Event, EventFragment.UserViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Firebase_Event, EventFragment.UserViewHolder>(
                Firebase_Event.class,
                R.layout.event_list_card,
                EventFragment.UserViewHolder.class,
                mUserDatabase.orderByChild(Firebase_user_id)
                        //.child("distance").child(Firebase_user_id).orderByValue()
        ) {
            @Override
            protected void populateViewHolder(final UserViewHolder usersviewHolder, Firebase_Event firebase_event, int position) {

                //setting it to the palceholder one by one


                finaldistance = new ArrayList<Float>();

             try {

                 String current_loction_lat = firebase_event.getData_resturant_lat();

                 String current_loction_long = firebase_event.getData_resturant_long();

                 resLoc.setLatitude(Double.parseDouble(current_loction_lat));
                 resLoc.setLongitude(Double.parseDouble(current_loction_long));
                 distanceBet = userLoc.distanceTo(resLoc) / 1000;

                 if (distanceBet > 1) {
                     distance = String.valueOf(distanceBet).substring(0, 4).concat("km");
                 } else {
                     distance = String.valueOf(userLoc.distanceTo(resLoc)).substring(0, 3).concat("m");
                 }

                 finaldistance.add(distanceBet);

                 Collections.sort(finaldistance);

                 usersviewHolder.setDistance(distance);

                 usersviewHolder.start_shimmer_animation();
                 usersviewHolder.setName(firebase_event.getName_fb());
                 usersviewHolder.setProfilepic(firebase_event.getImageUrl());
                 usersviewHolder.setResturantname(firebase_event.getData_resturant_name());
                 usersviewHolder.setDate(firebase_event.getData_date());
                 usersviewHolder.setTime(firebase_event.getData_time());
                 String current_event_key = firebase_event.getData_event_id();
                 String d_event_date = firebase_event.getData_date();
                 String d_event_Time = firebase_event.getData_time();

                 //String event_date_time = d_event_date.concat(d_event_Time);
                 //System.out.println(event_date_time);

                 Date date = new Date();
                 SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                 String today_date = formatter.format(date);

//                 String td_date = String.valueOf(new Date());
//
                Date today_date_1 = new SimpleDateFormat("dd/M/yyyy").parse(today_date);

                 Date event_date__1 = new SimpleDateFormat("dd/M/yyyy").parse(d_event_date);
//
//                 System.out.println(today_date);

                // Date eDateTime = new SimpleDateFormat("dd/M/yyyy hh:mm").parse(event_date_time);


                 long diffrence = (today_date_1.getTime() - event_date__1.getTime())/86400000;
                 System.out.println(diffrence);
//
//                 if (diffrence == 2){
//                     mEvent.child(key1).removeValue();
//                 }

//                 if(d_event_date.compareTo(today_date) ==  ){
//                    System.out.println("true working");
//                 }
             }catch (NullPointerException e){
                 e.printStackTrace();
             } catch (ParseException e) {
                 e.printStackTrace();
             }

                /*SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String today_date = formatter.format(date);
                System.out.println(today_date);*/


//                if (d_event_date.compareTo(today_date) >= 1) {
//
//
//                }
//                else if (d_event_date.compareTo(today_date) < 0) {
//
//                    //mUserDatabase.child(current_event_key).getRef().setValue(null);
//
//                    if (current_event_key == null) {
//
//                    } else {
//                        mUserDatabase.child(current_event_key).removeValue();
//                    }
//                }else if (d_event_date.compareTo(today_date) == 0){
//                    if (current_event_key == null) {
//
//                    } else {
//                        mUserDatabase.child(current_event_key).removeValue();
//                    }
//                }


                usersviewHolder.stop_shimmer_animation();


                final String event_id = getRef(position).getKey();

                // final String current_firebase_user_id =  getRef(position).getKey("firebase_user_id");

                usersviewHolder.mview.setOnClickListener(new View.OnClickListener() {


                    public Activity mContext;

                    @Override
                    public void onClick(View view) {

                        Intent eventpage_intent = new Intent(EventFragment.this.getActivity(), hosted_main_activity.class);
                        eventpage_intent.putExtra("event_id", event_id);
                        startActivity(eventpage_intent);

                        mContext = (Activity) usersviewHolder.itemView.getContext();
                        mContext.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


                    }
                });






//
//                Location userLoc = new Location("");
//                userLoc.setLatitude(userLat);
//                userLoc.setLongitude(userLong);
//
//                Log.i("lat", String.format("UserLoc '%s'", userLoc));
//
//                Firebase_Event firebaseEvent = new Firebase_Event();
//                 Location resLoc = new Location("");
//
//
//                Log.i("size", String.format("size '%s'", size));
//                    distanceBet = new ArrayList<>();
//
//                for(int i=0; i<size; i++ ){
//
//                    resLoc.setLatitude(Double.parseDouble(resLat.get(i)));
//                    resLoc.setLongitude(Double.parseDouble(resLong.get(i)));
//                    distanceBet.add(i, userLoc.distanceTo(resLoc)/1000);
//                     }
//
//                Log.i("Distance list", String.format("distance '%s'", distanceBet));
            }




        };



        mUserList.setAdapter((firebaseRecyclerAdapter));




    }



        /*int size = resLat.size();
        for(int i=0; i<size ; i++){
            resLat.get(i);
        }*/














//
//       Collections.sort((List<Firebase_Event>) firebaseRecyclerAdapter, new Comparator<Firebase_Event>() {
//
//            @Override
//            public int compare(Firebase_Event o1, Firebase_Event o2) {
//
//
//
//                return 0;
//            }
//
//
//        }


//        Collections.sort((List<sorting>) firebaseRecyclerAdapter, new Comparator<sorting>() {
//            @Override
//            public int compare(sorting c1, sorting c2){
//                return new Float(c1.getDistance()).compareTo(new Float(c2.getDistance()));
//            }
//        });
//
//        firebaseRecyclerAdapter.notifyDataSetChanged();




















}




//  mShimmerViewContainer = rootView.findViewById(R.id.shimmer_view_container);



//code to hide the floating money
//        mUserList.addOnScrollListener(new RecyclerView.OnScrollListener() {
//            @Override
//            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
//                super.onScrollStateChanged(recyclerView, newState);
//            }
//
//            @Override
//            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                final FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
//
//                if (dy < 0) {
//                    if (fab.isShown()) {
//                        fab.hide();
//
//                        fab.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                fab.show();
//                            }
//                        },1000);
//                    }
//
//                } else if (dy > 0) {
//
//                    if (fab.isShown()) {
//                        fab.hide();
//                        fab.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                fab.show();
//                            }
//                        },1000);
//                    }
//                }
//
//            }
//        });






//resLat.add(current_loction_lat);



//resLong.add(current_loction_long);

//                int size = resLat.size();
//                Log.i("lat", String.format("NextREs '%s'", resLat));




//                Picasso.with(getContext()).load(firebase_event.getImageUrl())
//                        .fit()
//                        .into(profilepic);


//                Picasso.with(usersviewHolder.profilepic.getContext()).load(firebase_event.getImageUrl())
//                        .placeholder(R.drawable.profile_image_new).into(usersviewHolder.profilepic);


//                usersviewHolder.setProfilepic(firebase_event.getImageUrl());



//   finalDis.add(distanceBet);

               /*double distance =  MainActivity.haversine( userLat , userLong , curr_lat, curr_long);

               String dis = String.valueOf(distance);
                cut_dis = dis.substring(0,4);
*/