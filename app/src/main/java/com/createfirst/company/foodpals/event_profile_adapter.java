package com.createfirst.company.foodpals;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandhaa on 06/04/18.
 */

public class event_profile_adapter extends RecyclerView.Adapter<event_profile_adapter.ViewHolder> {


    private static  final String TAG = "event_profile_adapter";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mProfilepic = new ArrayList<>();
    private Context mContext;

    public event_profile_adapter( Context mContext,ArrayList<String> mNames, ArrayList<String> mProfilepic) {
        this.mNames = mNames;
        this.mProfilepic = mProfilepic;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(com.createfirst.company.foodpals.R.layout.event_player_profileimage,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Log.d(TAG, "onBindViewHolder: called");

        Picasso.with(mContext).load(mProfilepic.get(position)).placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(holder.profile_image);

        holder.event_name.setText(mNames.get(position));

//        holder.event_name.setText("Anand Rajan");

    }

    @Override
    public int getItemCount() {
        return mNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView profile_image;
        TextView event_name;

        public ViewHolder(View itemView) {
            super(itemView);
            profile_image =itemView.findViewById(com.createfirst.company.foodpals.R.id.event_image_view);
            event_name = itemView.findViewById(com.createfirst.company.foodpals.R.id.event_name);
        }
    }
}
