package com.createfirst.company.foodpals.mFragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.createfirst.company.foodpals.GoogleNextPage;
import com.createfirst.company.foodpals.GoogleParameters;
import com.createfirst.company.foodpals.GoogleResAdapter;
import com.createfirst.company.foodpals.MainActivity;
import com.createfirst.company.foodpals.R;
import com.createfirst.company.foodpals.ZomatoParameter;
import com.createfirst.company.foodpals.ZomatoResAdapter;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;


public class ResturantFragment extends Fragment {

    private static final String LOG_TAG = "ResturantFragment";
    private static ArrayList<ZomatoParameter> RestaurantList1;
    private ZomatoResAdapter albumsAdapter1;
    private static ArrayList<GoogleParameters> RestaurantList,RestaurantListNT;
    private ArrayList<GoogleNextPage> NextToken;
    private RecyclerView recyclerView;
    private GoogleResAdapter albumsAdapter;
    private SearchView sv;
    private MenuItem menuItem;
    String NTpage;
    public View mview;
    private SwipeRefreshLayout mRefershlayout;
    private ShimmerFrameLayout mShimmerViewContainer;
    private static final boolean Zomato = false;
    private DatabaseReference mSwitch_new;
    private LatLng latLng;
    private String resturant_switch_new;
    protected Handler handler;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        handler = new Handler();

        Log.i(LOG_TAG, String.format("ResturantFrangment'%s'", RestaurantList1));
        Log.i(LOG_TAG, String.format("ResturantFrangment'%s'", RestaurantList));

        this.setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.content_main, container, false);


        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_card);

        recyclerView.setLayoutManager(new LinearLayoutManager(ResturantFragment.this.getActivity()));

        //albumsAdapter1 = new ZomatoResAdapter(this.getActivity(), RestaurantList1);
        /*final ZomatoParameter adapter = RestaurantList1;
        Log.i("Name of the Resturant", String.format("ResturantFrangAdapter'%s'", adapter));*/
        if (MainActivity.resturant_switch.equals("zomato")) {
            RestaurantList1 = getArguments().getParcelableArrayList("restaurantList1");
            albumsAdapter1 = new ZomatoResAdapter(this.getActivity(), RestaurantList1);
            recyclerView.setAdapter(albumsAdapter1);
        } else if (MainActivity.resturant_switch.equals("google")) {
            RestaurantList = getArguments().getParcelableArrayList("restaurantList");
            albumsAdapter = new GoogleResAdapter(this.getActivity(), RestaurantList,recyclerView);
            recyclerView.setAdapter(albumsAdapter);

            /*albumsAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            RestaurantList.remove(RestaurantList.size() - 1);
                            albumsAdapter.notifyItemRemoved(RestaurantList.size());

                            ReadWriteJson readWriteJson = new ReadWriteJson();
                            NTpage = readWriteJson.readFromFile(ResturantFragment.this.getActivity());

                            RestaurantListNT = RestClient1.parseJsonInBO1(NTpage);
                            Log.i("NTData", NTpage);

                            albumsAdapter = new GoogleResAdapter(getContext(), RestaurantListNT,recyclerView);
                            recyclerView.setAdapter(albumsAdapter);
                            albumsAdapter.notifyItemInserted(RestaurantList.size());
                            albumsAdapter.notifyDataSetChanged();
                            albumsAdapter.setLoaded();
                        }
                    },4000);


                }

            });*/
        }

        updateUI();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        updateUI();


    }

    private void updateUI() {
        if (Zomato == true) {
            if (RestaurantList1 != null && RestaurantList1.size() != 0) {
                ArrayList<ZomatoParameter> restaurants = RestaurantList1;

                if (albumsAdapter1 == null) {
                    albumsAdapter1 = new ZomatoResAdapter(getContext(), restaurants);
                    recyclerView.setAdapter(albumsAdapter1);
                    albumsAdapter1.notifyDataSetChanged();

                } else {

                    albumsAdapter1.notifyDataSetChanged();

                }
            }
        } else if (Zomato) {
            if (RestaurantList != null && RestaurantList.size() != 0) {
                ArrayList<GoogleParameters> restaurants = RestaurantList;

                if (albumsAdapter == null) {
                    albumsAdapter = new GoogleResAdapter(getContext(), restaurants,recyclerView);
                    recyclerView.setAdapter(albumsAdapter);


                } else {

                    albumsAdapter.notifyDataSetChanged();

                }
            }
        }
    }


    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();

    }

}
