package com.createfirst.company.foodpals;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/**
 * Created by anandhaa on 07/06/18.
 */



public class NoInternet extends AppCompatActivity {

    private Context context;
    private Activity mActivity;
    Button b_retry ;

      private Dialog no_internet_popup ;

//    NoInternet(Activity a){
//        this.mActivity =a ;
//    }




public void check_internet(Context context3){
    no_internet_popup = new Dialog(context3);
    no_internet_popup.setContentView(com.createfirst.company.foodpals.R.layout.nointernet_custom_popup);
    no_internet_popup.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    no_internet_popup.getWindow().getAttributes().windowAnimations = com.createfirst.company.foodpals.R.style.DialogAnimation;
    no_internet_popup. setCanceledOnTouchOutside(false);
    no_internet_popup.create();

    b_retry = no_internet_popup.findViewById(com.createfirst.company.foodpals.R.id.no_internet_okay);

    checkConnection(context3);
}



    public void checkConnection(Context context) {
        boolean isConnected = ConnectivityReceiver.isConnected();

        no_internet(isConnected,no_internet_popup,context);

    }

    // Showing the status in Snackbar
    public void no_internet(final boolean isConnected, final Dialog no_internet_popup, final Context contex1) {
        //Initializing the Pop up codes


        if (isConnected) {

            if (no_internet_popup.isShowing()) {

                no_internet_popup.dismiss();
            }

            else{

            }


        } else {

            no_internet_popup.show();


            b_retry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkConnection(contex1);
//                    if(isConnected == true){
//                        no_internet_popup.dismiss();
//                    }
//                    else{
//                        checkConnection(context1);
//                    }

                }
            });
        }
    }




}









