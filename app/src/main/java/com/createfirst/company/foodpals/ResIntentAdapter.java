package com.createfirst.company.foodpals;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Shilpa on 16-11-2017.
 */

public class ResIntentAdapter extends RecyclerView.Adapter<ResIntentAdapter.ViewHolder>{

    private Context context;
    public List<GoogleParameters> albumList;
    public TextView txtName,mAdressTextView,openNow,mRating1,mMoney,title, count;
    public ImageView imageView, thumbnail, overflow;
    /*public class MyViewHolder extends RecyclerView.Adapter<MyAdapter.ViewHolder>  {
*/
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName, mAdressTextView, openNow, mRating1, mMoney, title, count;
        public ImageView imageView, thumbnail, overflow;

        public ViewHolder(View view) {
            super(view);

            imageView = (ImageView) view.findViewById(com.createfirst.company.foodpals.R.id.imageViewRes);
            txtName = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.txtNameRes);
            mAdressTextView = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.txtLocationRes);
            openNow = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.txtOpenNowRes);
            mRating1 = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.rating1Res);
            mMoney = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.price_levelRes);

        }
    }


    public ResIntentAdapter(Context context, List<GoogleParameters> albumList) {
        this.context = context;
        this.albumList = albumList;
    }

    @Override
    public ResIntentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(com.createfirst.company.foodpals.R.layout.restaurant_intent, parent, false);
        return new ResIntentAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GoogleParameters product = albumList.get(position);
        /*holder.title.setText(album.getName());
        holder.count.setText(album.getNumOfSongs() + " songs");*/
        Picasso.with(context).load("https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference="+product.getPhoto_reference()+"&key=AIzaSyC9UVWB_FiS8V-zM-ul1PxmtoVccKh-q5Q")
                .fit()
                .into(holder.imageView);

        holder.txtName.setText(product.getName());

        holder.mAdressTextView.setText(product.getAddress());

        if(product.isOpen_now()){
            holder.openNow.setText("Open Now");
        }
        else{

            holder.openNow.setText("Closed");
        }

        if(product.getRating()!=null) {
            holder.mRating1.setText(product.getRating().toString()+"/5");
        }else {
            holder.mRating1.setText(0+"/5");
        }

        holder.mMoney.setText("");
        //Code for putting price level to show $ sign
        if(product.getPrice_level()!=0) {
            for(int i=0; i<product.getPrice_level();i++){
                holder.mMoney.append("$");
            }
        }else {
            holder.mMoney.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return albumList.size();
    }

}
