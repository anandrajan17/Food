package com.createfirst.company.foodpals;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by anandhaa on 18/04/18.
 */

public class req_reciever_adapter extends RecyclerView.Adapter<req_reciever_adapter.ViewHolder> {


    private static  final String TAG = "req_reciever_adapter";

    //vars
    private ArrayList<String> req_mNames = new ArrayList<>();
    private ArrayList<String> req_mProfilepic = new ArrayList<>();
    private ArrayList<String> req_mUser_id = new ArrayList<>();
    private Context mContext;



    OnClickInAdapter onClickInAdapter;

    onClickReject onClickReject;

    public req_reciever_adapter( Context mContext,ArrayList<String> req_mNames, ArrayList<String> req_mProfilepic,ArrayList<String> req_mUser_id ) {
        this.req_mNames = req_mNames;
        this.req_mProfilepic = req_mProfilepic;
        this.req_mUser_id = req_mUser_id;
        this.mContext = mContext;
    }

    @Override
    public req_reciever_adapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(com.createfirst.company.foodpals.R.layout.req_recived_inflator,parent,false);
        return new ViewHolder(view);
    }






    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        Log.d(TAG, "onBindViewHolder: called");



        Picasso.with(mContext).load(req_mProfilepic.get(position)).placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(holder.req_profile_image);

        holder.req_user_name.setText(req_mNames.get(position));

        holder.user_nw_id = String.valueOf(req_mUser_id.get(position));

        holder.req_user_id.setText(req_mUser_id.get(position));







        //clicking and seding the sender position through interface
        holder.req_decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String user_id_new = holder.req_user_id.getText().toString();
                try {
                    onClickReject = (onClickReject) mContext;
                } catch (ClassCastException e) {
                    throw new ClassCastException(mContext.toString()
                            + " must implement onClickReject");
                }
                onClickReject.onClickReject(user_id_new);
            }
        });









        // Clicking on the button via intercafe from main acticty

        holder.req_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String user_id_new = holder.req_user_id.getText().toString();
                try {
                    onClickInAdapter = (OnClickInAdapter) mContext;
                } catch (ClassCastException e) {
                    throw new ClassCastException(mContext.toString()
                            + " must implement OnClickInAdapter");
                }
                onClickInAdapter.onClickInAdapter(user_id_new);

            }
        });


    }


//Interface for sedning the string
    public interface OnClickInAdapter {
         void onClickInAdapter(String content);
    }

    public interface onClickReject {
        void onClickReject(String content);
    }


    @Override
    public int getItemCount() {
        return req_mProfilepic.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CircleImageView req_profile_image;
        TextView req_user_name;
        TextView req_user_id;
        Button req_accept;
        Button req_decline;
        String user_nw_id;
        String recycler_position;

        public ViewHolder(View itemView) {
            super(itemView);
            req_profile_image =itemView.findViewById(com.createfirst.company.foodpals.R.id.req_image_view);
            req_user_name = itemView.findViewById(com.createfirst.company.foodpals.R.id.req_user_name);
            req_accept = itemView.findViewById(com.createfirst.company.foodpals.R.id.req_accept_button);
            req_decline = itemView.findViewById(com.createfirst.company.foodpals.R.id.req_reject_button);
            req_user_id = itemView.findViewById(com.createfirst.company.foodpals.R.id.req_reciver_user_id);








        }

    }


}
