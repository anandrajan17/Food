package com.createfirst.company.foodpals;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by anandhaa on 09/09/17.
 */

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        super.onMessageReceived(remoteMessage);


        String notification_title = remoteMessage.getNotification().getTitle();
        String notification_body = remoteMessage.getNotification().getBody();
        String click_action = remoteMessage.getNotification().getClickAction();
        String from_user_id  = remoteMessage.getData().get("from_user_id");


//        Notification mBuilder = new NotificationCompat.Builder(this)
//                .setContentTitle(notification_title)
//                .setContentText(notification_body)
//                .setSmallIcon(R.mipmap.ic_new_profile)
//                .build();


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(com.createfirst.company.foodpals.R.mipmap.ic_new_profile)
                        .setContentTitle(notification_title)
                        .setContentText(notification_body);


        Intent resultIntent = new Intent(click_action);
        resultIntent.putExtra(("user_id"), from_user_id);


        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);





        int mNotificationID = (int) System.currentTimeMillis();

        NotificationManager mNotifyMgr =  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        mNotifyMgr.notify(mNotificationID, mBuilder.build());


    }
}
