package com.createfirst.company.foodpals;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

/**
 * Created by anandhaa on 25/03/18.
 */

public class test extends AppCompatActivity {

    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mProfilepic = new ArrayList<>();

    private LinearLayout layoutToAdd;
    private LinearLayout layoutToremove;



    private static  final String TAG = "Event_Hosted_page";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.event_hosted_mainpage);


        Button button = (Button) findViewById(com.createfirst.company.foodpals.R.id.hosted_event_manage);

        layoutToremove = (LinearLayout) findViewById(com.createfirst.company.foodpals.R.id.replacing_inflator);
        layoutToAdd = (LinearLayout) findViewById(com.createfirst.company.foodpals.R.id.changing_layout);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LayoutInflater inflater = LayoutInflater
                        .from(getApplicationContext());
                View view = inflater.inflate(com.createfirst.company.foodpals.R.layout.req_recived_inflator, null);
                layoutToAdd.setVisibility(View.GONE);
                layoutToremove.addView(view);
            }
        });


        //Code to hide the layput

//        LinearLayout roaster_layput = (LinearLayout) findViewById(R.id.roaster_layout);
//        roaster_layput.setVisibility(View.GONE);

        getImages();
    }


    private void getImages(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mProfilepic.add("https://c1.staticflickr.com/5/4636/25316407448_de5fbf183d_o.jpg");
        mNames.add("Havasu Falls");

        mProfilepic.add("https://i.redd.it/tpsnoz5bzo501.jpg");
        mNames.add("Trondheim");

        mProfilepic.add("https://i.redd.it/qn7f9oqu7o501.jpg");
        mNames.add("Portugal");

        mProfilepic.add("https://i.redd.it/j6myfqglup501.jpg");
        mNames.add("Rocky Mountain National Park");


        mProfilepic.add("https://i.redd.it/0h2gm1ix6p501.jpg");
        mNames.add("Mahahual");

        mProfilepic.add("https://i.redd.it/k98uzl68eh501.jpg");
        mNames.add("Frozen Lake");


        mProfilepic.add("https://i.redd.it/glin0nwndo501.jpg");
        mNames.add("White Sands Desert");

        mProfilepic.add("https://i.redd.it/obx4zydshg601.jpg");
        mNames.add("Austrailia");

        mProfilepic.add("https://i.imgur.com/ZcLLrkY.jpg");
        mNames.add("Washington");

        initRecyclerView();

    }

    private void initRecyclerView(){
        Log.d(TAG, "Init recycler view:initi Recyvlerview");

        LinearLayoutManager layoutmanager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        RecyclerView recyclerView = findViewById(com.createfirst.company.foodpals.R.id.profile_image_holder);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(),3);
        recyclerView.setLayoutManager(gridLayoutManager);

        event_profile_adapter adapter = new event_profile_adapter(this, mNames, mProfilepic);
        recyclerView.setAdapter(adapter);
    }
}
