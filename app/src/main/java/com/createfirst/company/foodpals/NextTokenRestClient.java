package com.createfirst.company.foodpals;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Shilpa on 02-06-2018.
 */

public class NextTokenRestClient {
    public static ArrayList<GoogleNextPage> parseJsonNextToken(String content){

        ArrayList<GoogleNextPage> NextTokenList = new ArrayList<GoogleNextPage>();

        try {
            GoogleNextPage NextToken = new GoogleNextPage();
            JSONObject jsonObject = new JSONObject(content);
           // JSONArray jsonArray =  jsonObject.get("next_page_token");
            if(jsonObject.has("next_page_token")) {
                NextToken.setNext_page_token(jsonObject.get("next_page_token").toString());
            }

            NextTokenList.add(NextToken);
            }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return NextTokenList;
    }


}
