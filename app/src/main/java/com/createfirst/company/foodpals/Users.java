package com.createfirst.company.foodpals;

/**
 * Created by anandhaa on 25/08/17.
 */

public class Users {

    public String name_fb;
    public  String surname ;
    public  String imageUrl ;
    public  String middlename ;
    public  String user_facebook_id ;

    public  String firebase_user_id ;
    public  String user_facebook_uri ;
    public  String user_email_id ;



   public  Users(){

    }

    public String getName_fb() {
        return name_fb;
    }

    public void setName_fb(String name_fb) {
        this.name_fb = name_fb;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getUser_facebook_id() {
        return user_facebook_id;
    }

    public void setUser_facebook_id(String user_facebook_id) {
        this.user_facebook_id = user_facebook_id;
    }

    public String getFirebase_user_id() {
        return firebase_user_id;
    }

    public void setFirebase_user_id(String firebase_user_id) {
        this.firebase_user_id = firebase_user_id;
    }

    public String getUser_facebook_uri() {
        return user_facebook_uri;
    }

    public void setUser_facebook_uri(String user_facebook_uri) {
        this.user_facebook_uri = user_facebook_uri;
    }

    public String getUser_email_id() {
        return user_email_id;
    }

    public void setUser_email_id(String user_email_id) {
        this.user_email_id = user_email_id;
    }


    public Users(String name_fb, String surname, String imageUrl, String middlename, String user_facebook_id, String firebase_user_id, String user_facebook_uri, String user_email_id) {
        this.name_fb = name_fb;
        this.surname = surname;
        this.imageUrl = imageUrl;
        this.middlename = middlename;
        this.user_facebook_id = user_facebook_id;
        this.firebase_user_id = firebase_user_id;
        this.user_facebook_uri = user_facebook_uri;
        this.user_email_id = user_email_id;
    }
}
