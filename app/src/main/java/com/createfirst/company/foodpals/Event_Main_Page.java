package com.createfirst.company.foodpals;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.ServerValue;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by anandhaa on 26/08/17.
 */
public class Event_Main_Page extends AppCompatActivity {

    //User for Firebases
    public String name_fb;
    public String surname;
    public String imageUrl;
    public String middlename;
    public String user_facebook_id;
    public String event_creator_id;
    public String user_facebook_uri;
    public String user_email_id;
    public String data_Venue;
    public String data_additional_detail;
    public String data_date;


    private TextView mProfilefirstname, mProfilesurname, mProfilemiddlename;
    private CircleImageView mProfileImage;
    private Button mProfileSendReqBtn, getmProfileDeclineReqBtn;

    private ProgressDialog mProgressDialogue;


    private FirebaseUser mCurrent_user;
    private FirebaseAuth mAuth;


    private DatabaseReference mFriendsReqDatabase;
    private DatabaseReference mFriendDatabase;

    private DatabaseReference mUsersDatabase;
    private DatabaseReference mSenderDatabase;
    private DatabaseReference mNotificationDatabase;
    private DatabaseReference mRootref;
    private DatabaseReference mcurrent_user_online;
    private DatabaseReference mfriend_user_online;
    private DatabaseReference mUserDatabase;

    private String mCurrent_State;

    public String event_id;
    public String current_user_id;
    public String  sent_user_id;


    public View mview;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.activity_eventmainpage);



        event_id = getIntent().getStringExtra("event_id");
        mAuth = FirebaseAuth.getInstance();

        Toolbar mChatToolbar = (Toolbar) findViewById(com.createfirst.company.foodpals.R.id.chat_app_bar);
        setSupportActionBar(mChatToolbar);

        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);

        mChatToolbar.setTitleTextColor(Color.WHITE);


        //Back arrow colour
        Drawable backArrow = getResources().getDrawable(com.createfirst.company.foodpals.R.drawable.ic_arrow_back_black_24dp);
        backArrow.setColorFilter(getResources().getColor(com.createfirst.company.foodpals.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(backArrow);


        FloatingActionButton share_button = (FloatingActionButton) findViewById(com.createfirst.company.foodpals.R.id.share);




        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

            }
        });


// Data base pointing
        mRootref = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT);


            mUsersDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Event").child(event_id);
            mFriendsReqDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Friend_req");
            mFriendDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Friend");
            mSenderDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Sender");
            mNotificationDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("notifications");
            mCurrent_user = FirebaseAuth.getInstance().getCurrentUser();
            mfriend_user_online = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User");
            mfriend_user_online = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("User");
            //   mcurrent_user_online = FirebaseDatabase.getInstance().getReference().child("User").child(user_id);

            //Declaring the ids

            current_user_id = mCurrent_user.getUid().toString();


            //Declaring buttons and text fields

            mProfilefirstname = (TextView) findViewById(com.createfirst.company.foodpals.R.id.profile_event_username);
            mProfileImage = (CircleImageView) findViewById(com.createfirst.company.foodpals.R.id.profile_event_image);
            mProfileSendReqBtn = (Button) findViewById(com.createfirst.company.foodpals.R.id.profile_sendreq_button);
            getmProfileDeclineReqBtn = (Button) findViewById(com.createfirst.company.foodpals.R.id.profile_declinereq_button);

            getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
            getmProfileDeclineReqBtn.setEnabled(false);


            //First state
            mCurrent_State = "not_friends";


            mProgressDialogue = new ProgressDialog(this);
            mProgressDialogue.setTitle("Loading User Data");
            mProgressDialogue.setMessage("Please wait While we Load the Event");
            mProgressDialogue.setCanceledOnTouchOutside(false);
            mProgressDialogue.show();


        FloatingActionButton mChat = (FloatingActionButton) findViewById(com.createfirst.company.foodpals.R.id.chat);




        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                name_fb = dataSnapshot.child("name_fb").getValue().toString();
                surname = dataSnapshot.child("surname").getValue().toString();
                imageUrl = dataSnapshot.child("imageUrl").getValue().toString();
                middlename = dataSnapshot.child("middlename").getValue().toString();
                user_facebook_id = dataSnapshot.child("user_facebook_id").getValue().toString();
                event_creator_id = dataSnapshot.child("firebase_user_id").getValue().toString();
                user_facebook_uri = dataSnapshot.child("user_facebook_uri").getValue().toString();
                user_email_id = dataSnapshot.child("user_email_id").getValue().toString();
                data_Venue = dataSnapshot.child("data_Venue").getValue().toString();
                data_additional_detail = dataSnapshot.child("data_additional_detail").getValue().toString();
                data_date = dataSnapshot.child("data_date").getValue().toString();





                mfriend_user_online.child(event_creator_id).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {


                        if (dataSnapshot.hasChild("online")){
                            String userOnline =  dataSnapshot.child("online").getValue().toString();
                            setUserOnline(userOnline);

                        }
                        else
                        {

                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                //Setting User Profile name
                mProfilefirstname.setText("" + name_fb + " " + middlename + " " + surname);

                //setting user profile picture
                Picasso.with(Event_Main_Page.this).load(imageUrl).placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(mProfileImage);



                //-------------------------------- Friends List/Request Feature ---------------------------------------- checks up on oncreate

                mFriendsReqDatabase.child(event_id).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if (dataSnapshot.hasChild(current_user_id)) {
                            String request_type = dataSnapshot.child(current_user_id).child("request_type").getValue().toString();

                            if(request_type.equals("received")) {
                                mCurrent_State = "req_received";
                                mProfileSendReqBtn.setText("Accept Friend Request");

                                getmProfileDeclineReqBtn.setVisibility(View.VISIBLE);
                                getmProfileDeclineReqBtn.setEnabled(true);


                            } else if (request_type.equals("sent")) {

                                mCurrent_State = "req_sent";
                                mProfileSendReqBtn.setText("Cancel Friend Request");

                                getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                getmProfileDeclineReqBtn.setEnabled(false);
                            }
                            mProgressDialogue.dismiss();
                        }else{
                            mFriendDatabase.child(event_id).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.hasChild(current_user_id)){

                                        mCurrent_State = "friends";
                                        mProfileSendReqBtn.setText("UnFriend the Person");
                                        getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                        getmProfileDeclineReqBtn.setEnabled(false);
                                    }
                                    mProgressDialogue.dismiss();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }

                        mProgressDialogue.dismiss();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

            }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
        });


        mProfileSendReqBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProfileSendReqBtn.setEnabled(false);

                // Not Friends State

                if (mCurrent_State.equals("not_friends")) {

                    DatabaseReference newNotficationref = mRootref.child("notifications").child(event_id).push();
                    String newNofticaionID = newNotficationref.getKey();
                    HashMap<String, String> notificationData = new HashMap<String, String>();
                    notificationData.put("from", current_user_id);
                    notificationData.put("type", "request");

                    Map requestMap = new HashMap();
                    requestMap.put("Friend_req/" +event_id  + "/" + current_user_id+ "/request_type", "sent");
                    requestMap.put("Friend_req/" + event_id  + "/" + event_creator_id + "/request_type", "received");
                    requestMap.put("Sender/" + event_id+ "/"+ "/Sender", current_user_id );
                    requestMap.put("notifications/" + event_id + "/" + newNofticaionID, notificationData);

                    mRootref.updateChildren(requestMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                            mProfileSendReqBtn.setEnabled(true);
                            mCurrent_State = "req_sent";
                            mProfileSendReqBtn.setText("Cancel Friend Request");
                            getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);


                            if(databaseError != null){
                                Toast.makeText(Event_Main_Page.this, "Error sending Request", Toast.LENGTH_SHORT);

                            }

                        }


                });

                }


                // Cancel Request sent
                if (mCurrent_State.equals("req_sent")) {
                    mFriendsReqDatabase.child(event_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {

                            mFriendsReqDatabase.child(event_id).child(current_user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    mProfileSendReqBtn.setEnabled(true);
                                    mCurrent_State = "not_friends";
                                    mProfileSendReqBtn.setText("Send Friend Request");
                                    getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                    getmProfileDeclineReqBtn.setEnabled(false);
                                }
                            });

                        }
                    });
                }




                //------------------------- REQUEST RECEVIED STATE--------------------------------------------------------

                if(mCurrent_State.equals("req_received")){
                    mUserDatabase = FirebaseDatabase.getInstance().getReference().child(MainActivity.ENVIRONMENT).child("Sender").child(event_id);


//                    Map friendsMap = new HashMap();
//                    friendsMap.put("Friends/"+ user_id + "/" + mCurrent_user.getUid()+ "/"+ sent_user_id +"/date" , ServerValue.TIMESTAMP);





                    mUserDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            String  sent_user_id = dataSnapshot.child("Sender").getValue().toString();


                            mFriendDatabase.child(event_id).child(current_user_id).child(sent_user_id).setValue(ServerValue.TIMESTAMP).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {

                                    mFriendsReqDatabase.child(event_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {

                                            mFriendsReqDatabase.child(event_id).child("sender").child(current_user_id).removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {

                                                    mSenderDatabase.child(event_id).child("Sender").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                                                        @Override
                                                        public void onSuccess(Void aVoid) {

                                                            mProfileSendReqBtn.setEnabled(true);
                                                            mCurrent_State = "friends";
                                                            mProfileSendReqBtn.setText("UnFriend the Person");
                                                            getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                                            getmProfileDeclineReqBtn.setEnabled(false);
                                                        }
                                                    });
                                                }
                                            });

                                        }
                                    });



                                }
                            });
                            // ...
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            // ...
                        }
                    });

                }
                if(mCurrent_State.equals("friends")){

                    Map unfriendMap = new HashMap();
                    unfriendMap.put("Friend/" +event_id+ "/" + current_user_id, null);
                    unfriendMap.put("Friend/" +current_user_id+ "/" + event_id, null);

                  try {

                      mRootref.updateChildren(unfriendMap, new DatabaseReference.CompletionListener() {
                          @Override
                          public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {

                              if (databaseError == null) {


                                  mCurrent_State = "not_friends";
                                  mProfileSendReqBtn.setText("Send Friend Request");

                                  getmProfileDeclineReqBtn.setVisibility(View.INVISIBLE);
                                  getmProfileDeclineReqBtn.setEnabled(false);

                              } else {
                                  String error = databaseError.getMessage();
                                  Toast.makeText(Event_Main_Page.this, error, Toast.LENGTH_SHORT).show();
                              }
                              mProfileSendReqBtn.setEnabled(true);

                          }
                      });
                  }catch (NullPointerException e){
                      e.printStackTrace();
                  }

                }

            }
        });



        mChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent chat_page = new Intent( getBaseContext() ,chat_activity.class);

                Toast.makeText(Event_Main_Page.this, event_creator_id, Toast.LENGTH_SHORT).show();

                chat_page.putExtra("event_creator_id", event_creator_id);
                chat_page.putExtra("current_user_id", current_user_id);
                chat_page.putExtra("surname", surname);
                chat_page.putExtra("imageUrl", imageUrl);
                chat_page.putExtra("middlename", middlename);
                chat_page.putExtra("Facebook_id", user_facebook_uri);
                chat_page.putExtra("Facebook_uri", user_facebook_uri);
                chat_page.putExtra("user_email_id", user_email_id);
                chat_page.putExtra("name_fb", name_fb);
                chat_page.putExtra("event_id", event_id);




                startActivity(chat_page);

                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_right, com.createfirst.company.foodpals.R.anim.slide_out_left);



            }
        });

       }



       //Boolean methord to make the online icon
    public void setUserOnline(String online_status) {

        ImageView useronlineView = (ImageView)findViewById(com.createfirst.company.foodpals.R.id.profile_online_icon);

        if(online_status.equals("true")){

            useronlineView.setVisibility(View.VISIBLE);

        } else {

            useronlineView.setVisibility(View.INVISIBLE);
        }

    }

    //Navigation Back button
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:




                onBackPressed();
                overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
                return true;


        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        // Write your code here

        super.onBackPressed();
        overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_left, com.createfirst.company.foodpals.R.anim.slide_out_right);
    }




}


