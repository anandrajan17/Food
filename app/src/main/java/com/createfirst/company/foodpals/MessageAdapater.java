package com.createfirst.company.foodpals;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.createfirst.company.foodpals.R.id.image_message_profile;

/**
 * Created by anandhaa on 04/02/18.
 */

public class MessageAdapater extends RecyclerView.Adapter<MessageAdapater.MessageViewHolder> {


    private List<Messages> messagesList;
    private List<Messages> image_url_list;
    private List<Messages> sender_name_list;
    private FirebaseAuth authentication;
    private DatabaseReference mUserDatabase;

    public String name_fb;
    public String chat_user_image;


    public MessageAdapater(List<Messages> messagesList, List<Messages> image_url_list, List<Messages> sender_name_list) {
        this.messagesList = messagesList;
        this.image_url_list = image_url_list;
        this.sender_name_list = sender_name_list;
    }


    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(com.createfirst.company.foodpals.R.layout.item_message_received, parent, false);

        return new MessageViewHolder(v);

    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {

        public TextView chat_message_holder_leftaligned;
        public TextView chat_message_holder_rightaligned;

        public CircleImageView profileImage;
        public TextView user_name;
        public TextView name_fb;
//        public TextView middlename;
//        public TextView surname;

        public MessageViewHolder(View view) {
            super(view);

            chat_message_holder_leftaligned = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.text_message_body_left);
            chat_message_holder_rightaligned = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.text_message_body_right);
            profileImage = (CircleImageView) view.findViewById(image_message_profile);
            user_name = (TextView) view.findViewById(com.createfirst.company.foodpals.R.id.text_message_name);
        }

    }

    @Override
    public void onBindViewHolder(final MessageViewHolder viewHolder, int i) {

        authentication = FirebaseAuth.getInstance();
        final String current_user_id_this = authentication.getCurrentUser().getUid();

        Messages c = messagesList.get(i);
        Messages img = image_url_list.get(i);
        Messages sender_name = sender_name_list.get(i);

        String from_user = c.getFrom();
        // String imageUrl = c.getImageUrl();

        mUserDatabase = FirebaseDatabase.getInstance().getReference().child("User").child(from_user);


//        mUserDatabase.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//
//                name_fb = dataSnapshot.child("First_Name").getValue().toString();
//               // String middlename = dataSnapshot.child("middlename").getValue().toString();
//            //    String surname = dataSnapshot.child("surname").getValue().toString();
//                chat_user_image  = dataSnapshot.child("Profile_image").getValue().toString();
//
//                // viewHolder.displayName.setText(name);
//
//              // viewHolder.user_name.setText(name_fb);
//
//               // Picasso.with(viewHolder.profileImage.getContext()).load(image)
//                       // .placeholder(R.drawable.profile_image_new).into(viewHolder.profileImage);
//
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//
//            }
//        });

        if (from_user.equals(current_user_id_this)) {

            viewHolder.chat_message_holder_leftaligned.setVisibility(View.INVISIBLE);
            viewHolder.chat_message_holder_rightaligned.setVisibility(View.VISIBLE);
            viewHolder.profileImage.setVisibility(View.INVISIBLE);
            viewHolder.user_name.setVisibility(View.INVISIBLE);
            // viewHolder.user_name.setText(name_fb);
            viewHolder.chat_message_holder_rightaligned.setBackgroundResource(com.createfirst.company.foodpals.R.drawable.message_text_background);
            viewHolder.chat_message_holder_rightaligned.setTextColor(Color.BLACK);
            viewHolder.chat_message_holder_rightaligned.setText(c.getMessage());
            // viewHolder.chat_message_holder_rightaligned.setGravity(Gravity.LEFT | Gravity.START);

        } else {
            viewHolder.chat_message_holder_leftaligned.setVisibility(View.VISIBLE);
            viewHolder.chat_message_holder_rightaligned.setVisibility(View.INVISIBLE);

            viewHolder.user_name.setText(sender_name.getSender_name());
            viewHolder.chat_message_holder_leftaligned.setText(c.getMessage());
            Picasso.with(viewHolder.profileImage.getContext()).load(img.getImageUrl())
                    .placeholder(com.createfirst.company.foodpals.R.drawable.profile_image_new).into(viewHolder.profileImage);
            viewHolder.chat_message_holder_leftaligned.setBackgroundResource(com.createfirst.company.foodpals.R.drawable.rounded_rectangle_orange);
            //viewHolder.chat_message_holder_leftaligned.setTextColor(Color.WHITE);

            viewHolder.chat_message_holder_leftaligned.setGravity(Gravity.LEFT | Gravity.START);
            //  viewHolder.chat_message_holder.setGravity(Gravity.RIGHT);

        }


        // Messages c = messagesList.get(i);
        // viewHolder.chat_message_holder.setText(c.getMessage());


    }


    @Override
    public int getItemCount() {
        return messagesList.size();
    }
}

