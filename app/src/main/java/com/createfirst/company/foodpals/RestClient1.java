package com.createfirst.company.foodpals;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;


/**
 * Created by Shilpa on 11-05-2018.
 */

public class RestClient1 {
    public static String makeHttpCall(String url1,Context context){
       URL url = null;
        HttpsURLConnection urlConnection=null;
        try {
            url = new URL(url1);
            urlConnection = (HttpsURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return convertStreamToString(in,context);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }

        return null;
    }
    public static String convertStreamToString(InputStream is,Context context) throws InterruptedException {
        List<GoogleNextPage> NextToken;
        String NTpage,NTpage1,NTData;
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        NextToken = NextTokenRestClient.parseJsonNextToken(sb.toString());
        final GoogleNextPage NP_token = NextToken.get(0);
        String next_token_page = NP_token.getNext_page_token();
        StringBuilder sbTemp = new StringBuilder();

        //RestClient1 NtRc = new RestClient1();
        if(next_token_page!=null){
            Thread.sleep(2000);
            final Uri.Builder builderNT = new Uri.Builder().scheme("https").authority("maps.googleapis.com").path("maps/api/place/nearbysearch/json");
            builderNT.appendQueryParameter("pagetoken", next_token_page)
                    .appendQueryParameter("key", (MainActivity.GOOGLE_KEY).toString());
            String urlNT = builderNT.build().toString();
            Log.i("NextToken URL", urlNT);

            NTpage = RestClient1.makeHttpCall(urlNT,context);

            sbTemp.append(NTpage);

            NextToken = NextTokenRestClient.parseJsonNextToken(sbTemp.toString());
            final GoogleNextPage NP_token1 = NextToken.get(0);
            String next_token_page1 = NP_token1.getNext_page_token();
            StringBuilder sbTemp1 = new StringBuilder();
            if(next_token_page1!=null){
                Thread.sleep(2000);
                final Uri.Builder builderNT1 = new Uri.Builder().scheme("https").authority("maps.googleapis.com").path("maps/api/place/nearbysearch/json");
                builderNT.appendQueryParameter("pagetoken", next_token_page)
                        .appendQueryParameter("key", (MainActivity.GOOGLE_KEY).toString());
                String urlNT1 = builderNT1.build().toString();

                Log.i("NextToken URL q1", urlNT1);

                NTpage1 = RestClient1.makeHttpCall(urlNT1,context);
                sbTemp1.append(NTpage1);
                sbTemp.append(sbTemp1);
            }
            NTData =sbTemp.toString();
            ReadWriteJson readWriteJson = new ReadWriteJson();
            readWriteJson.writeToFile(NTData,context);
        }

       // Log.i("GoogleResult", String.valueOf(sb));
        return sb.toString();
    }

    public static ArrayList<GoogleParameters> parseJsonInBO1(String content){

        ArrayList<GoogleParameters> restaurantList = new ArrayList<GoogleParameters>();

        try {
            JSONObject jsonObject = new JSONObject(content);
            JSONArray jsonArray = (JSONArray) jsonObject.get("results");
            for(int i=0;i<jsonArray.length();i++) {
                JSONObject gpObject = jsonArray.getJSONObject(i);
                GoogleParameters restaurant = new GoogleParameters();
                if(gpObject.has("name")) {
                    restaurant.setName(gpObject.get("name").toString());
                }

                if(gpObject.has("rating")) {
                    restaurant.setRating(Float.parseFloat(gpObject.get("rating").toString()));
                }
                else {
                    restaurant.setRating(Float.parseFloat("0"));
                }
                if(gpObject.has("price_level")) {
                    restaurant.setPrice_level(Integer.parseInt((gpObject.get("price_level").toString())));
                }
                if(gpObject.has("vicinity")) {
                    restaurant.setAddress(gpObject.get("vicinity").toString());
                }
                if(gpObject.has("geometry")) {
                    restaurant.setLat(Double.parseDouble(gpObject.getJSONObject("geometry").getJSONObject("location").get("lat").toString()));
                    restaurant.setLng(Double.parseDouble(gpObject.getJSONObject("geometry").getJSONObject("location").get("lng").toString()));
                }

                if(gpObject.has("photos")) {
                    restaurant.setPhoto_reference(gpObject.getJSONArray("photos").getJSONObject(0).getString("photo_reference").toString());
                }
                if(gpObject.has("opening_hours")){
                    restaurant.setOpen_now(gpObject.getJSONObject("opening_hours").getBoolean("open_now"));
                }
//arrayList.add(restaurant);
                restaurantList.add(restaurant);
            }
            /*bundle = new Bundle();
            bundle.putParcelableArrayList("restaurantList",(ArrayList<GoogleParameters>)restaurantList);*/
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return restaurantList;
    }/*Intent ResList = new Intent(RestClient.this, MainActivity.class);
            ResList.putExtra("restaurantList", restaurantList);
    startActivity(ResList);*/
}
