package com.createfirst.company.foodpals;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class tutorial extends AppCompatActivity {

    private ViewPager mSlideViewpager;
    private LinearLayout mDotLayout;

    private TextView[] mDots;

    private tutorial_SliderAdapter sliderAdapter;

    private Button mNextbutton;
    private Button mPrevbutton;

    private int mCurrentpage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(com.createfirst.company.foodpals.R.layout.activity_tutorial);

        if(!isFirstTimeStartapp()){

            startfbActivity();
            finish();
        }






        mSlideViewpager = (ViewPager) findViewById(com.createfirst.company.foodpals.R.id.slideviewpager);
        mDotLayout = (LinearLayout) findViewById(com.createfirst.company.foodpals.R.id.dotlayout);
        mNextbutton = (Button) findViewById(com.createfirst.company.foodpals.R.id.nextbutton);
        mPrevbutton = (Button)findViewById(com.createfirst.company.foodpals.R.id.prevbutton);


        sliderAdapter = new tutorial_SliderAdapter(this);

        mSlideViewpager.setAdapter(sliderAdapter);

        addDotIndicator(0);

        mSlideViewpager.addOnPageChangeListener(viewLister);

        mNextbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


               // mSlideViewpager.setCurrentItem(mCurrentpage +1 );

                int currentpage = mSlideViewpager.getCurrentItem()+1;

                if(currentpage < mDots.length){
                    mSlideViewpager.setCurrentItem(currentpage);
                }
                else{
                    startfbActivity();
                }
            }
        });


        mPrevbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSlideViewpager.setCurrentItem(mCurrentpage - 1);
            }
        });
    }


private boolean  isFirstTimeStartapp(){
    SharedPreferences ref = getApplication().getSharedPreferences("tutorialSlider", Context.MODE_PRIVATE);
    return ref.getBoolean("FirestTimeStartFlag", true);

}

private void setFirstTimeStartStatus(boolean stt){

    SharedPreferences ref = getApplication().getSharedPreferences("tutorialSlider", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = ref.edit();
    editor.putBoolean("FirestTimeStartFlag", stt);
    editor.commit();


}

private void startfbActivity()
    {
        setFirstTimeStartStatus(false);
        Intent main = new Intent(tutorial.this, fb_log.class);
        startActivity(main);
        overridePendingTransition(com.createfirst.company.foodpals.R.anim.slide_in_right, com.createfirst.company.foodpals.R.anim.slide_out_left);
    }


    public void addDotIndicator(int position){
        mDots = new TextView[3];
        mDotLayout.removeAllViews();

        for (int i=0; i<mDots.length; i++){
            mDots[i] = new TextView(this);
            mDots[i].setText(Html.fromHtml("&#8226"));
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(getResources().getColor(com.createfirst.company.foodpals.R.color.colorTextHint));

            mDotLayout.addView(mDots[i]);
        }

        if (mDots.length > 0 ){

            mDots[position].setTextColor(getResources().getColor(com.createfirst.company.foodpals.R.color.com_facebook_messenger_blue));
        }

    }



    ViewPager.OnPageChangeListener viewLister = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int i) {

            addDotIndicator(i);
            mCurrentpage = i;

            if(i ==0 ){

                mNextbutton.setEnabled(true);
                mPrevbutton.setEnabled(false);
                mPrevbutton.setVisibility(View.INVISIBLE);

                mNextbutton.setText("Next");

            }else if(i == mDots.length - 1 ){

                mNextbutton.setEnabled(true);
                mPrevbutton.setEnabled(true);
                mPrevbutton.setVisibility(View.VISIBLE);

                mNextbutton.setText("Finish");
                mPrevbutton.setText("Back");
            }else{

                mNextbutton.setEnabled(true);
                mPrevbutton.setEnabled(true);
                mPrevbutton.setVisibility(View.VISIBLE);

                mNextbutton.setText("Next");
                mPrevbutton.setText("Back");
            }

        }



        @Override
        public void onPageScrollStateChanged(int state) {

        }



    };


    @Override
    public void onStart() {
        super.onStart();

        if(!isFirstTimeStartapp()){

            startfbActivity();
            finish();
        }
    }

}
