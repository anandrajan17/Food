package com.createfirst.company.foodpals;

/**
 * Created by Shilpa on 31-05-2018.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}