package com.createfirst.company.foodpals;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Shilpa on 25-03-2018.
 */

public class ZomatoParameter implements Parcelable {

    public ZomatoParameter(String name, String locality, String cuisines,
                           int average_cost_for_two,
                           Float aggregate_rating,
                           String featured_image, Double lat, Double lng) {
        this.name = name;
        this.locality = locality;
        this.cuisines = cuisines;
        this.average_cost_for_two = average_cost_for_two;
        this.aggregate_rating = aggregate_rating;
        this.featured_image = featured_image;
        this.lat = lat;
        this.lng = lng;
    }

    public ZomatoParameter() {
    }

    protected ZomatoParameter(Parcel in) {
        name = in.readString();
        locality = in.readString();
        cuisines = in.readString();
        average_cost_for_two = in.readInt();
        aggregate_rating = in.readFloat();
        featured_image = in.readString();
    }

    public static final Creator<ZomatoParameter> CREATOR = new Creator<ZomatoParameter>() {
        @Override
        public ZomatoParameter createFromParcel(Parcel in) {
            return new ZomatoParameter(in);
        }

        @Override
        public ZomatoParameter[] newArray(int size) {
            return new ZomatoParameter[size];
        }
    };


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getCuisines() {
        return cuisines;
    }

    public void setCuisines(String cuisines) {
        this.cuisines = cuisines;
    }

    public int getAverage_cost_for_two() {
        return average_cost_for_two;
    }

    public void setAverage_cost_for_two(int average_cost_for_two) {
        this.average_cost_for_two = average_cost_for_two;
    }

    public Float getAggregate_rating() {
        return aggregate_rating;
    }

    public void setAggregate_rating(Float aggregate_rating) {
        this.aggregate_rating = aggregate_rating;
    }

    public String getFeatured_image() {
        return featured_image;
    }

    public void setFeatured_image(String featured_image) {
        this.featured_image = featured_image;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    private String name;
    String locality;
    String cuisines;
    int average_cost_for_two;
    Float aggregate_rating;
    String featured_image;

    Double lat;
    Double lng;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(locality);
        dest.writeString(cuisines);
        dest.writeInt(average_cost_for_two);
        dest.writeString(featured_image);
    }
}
