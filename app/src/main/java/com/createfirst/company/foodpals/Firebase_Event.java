package com.createfirst.company.foodpals;

/**
 * Created by anandhaa on 08/08/17.
 */

public class Firebase_Event {

    String data_additional_detail;
    String data_Venue;
    String data_date;
    String data_time;
    String data_gender;
    String name_fb;
    String surname ;
    String imageUrl ;
    String middlename ;
    String user_facebook_id ;
    String firebase_user_id ;
    String user_facebook_uri ;
    String user_email_id ;
    String data_resturant_name;
    String data_resturant_address;
    String data_resturant_lat;
    String data_resturant_long;
    String data_event_id;




    public  Firebase_Event(){

  }
    public Firebase_Event(String data_resturant_name, String data_resturant_address, String data_resturant_lat, String data_resturant_long,String data_additional_detail, String data_Venue, String data_date, String data_time, String data_gender, String name_fb, String surname, String imageUrl, String middlename, String user_facebook_id, String firebase_user_id, String  user_facebook_uri, String user_email_id,String data_event_id ) {
        this.data_additional_detail = data_additional_detail;
        this.data_Venue = data_Venue;
        this.data_date = data_date;
        this.data_time = data_time;
        this.data_gender = data_gender;
        this.name_fb = name_fb;
        this.surname = surname;
        this.imageUrl = imageUrl;
        this.middlename = middlename;
        this.user_facebook_id = user_facebook_id;
        this.firebase_user_id = firebase_user_id;
        this.user_facebook_uri = user_facebook_uri;
        this.user_email_id = user_email_id;
        this.data_resturant_name = data_resturant_name;
        this.data_resturant_address = data_resturant_address;
        this.data_resturant_lat = data_resturant_lat;
        this.data_resturant_long = data_resturant_long;
        this.data_event_id = data_event_id;
    }




    public String getData_resturant_name() {
        return data_resturant_name;
    }

    public void setData_resturant_name(String data_resturant_name) {
        this.data_resturant_name = data_resturant_name;
    }

    public String getData_resturant_address() {
        return data_resturant_address;
    }

    public void setData_resturant_address(String data_resturant_address) {
        this.data_resturant_address = data_resturant_address;
    }

    public String getData_resturant_lat() {
        return data_resturant_lat;
    }

    public void setData_resturant_lat(String data_resturant_lat) {
        this.data_resturant_lat = data_resturant_lat;
    }

    public String getData_resturant_long() {
        return data_resturant_long;
    }

    public void setData_resturant_long(String data_resturant_long) {
        this.data_resturant_long = data_resturant_long;
    }
    public String getData_additional_detail() {
        return data_additional_detail;
    }

    public String getData_Venue() {
        return data_Venue;
    }

    public String getData_date() {
        return data_date;
    }

    public String getData_time() {
        return data_time;
    }

    public String getData_gender() {
        return data_gender;
    }


    public String getName_fb() {
        return name_fb;
    }

    public String getSurname() {
        return surname;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getMiddlename() {
        return middlename;
    }

    public String getUser_facebook_id() {
        return user_facebook_id;
    }

    public String getFirebase_user_id() {
        return firebase_user_id;
    }

    public String getUser_facebook_uri() {
        return user_facebook_uri;
    }

    public String getUser_email_id() {
        return user_email_id;
    }

    public String getData_event_id() {
        return data_event_id;
    }


}



