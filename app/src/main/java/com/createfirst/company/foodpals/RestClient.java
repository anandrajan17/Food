package com.createfirst.company.foodpals;


import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Parteek on 7/18/2016.
 */
public class RestClient extends AppCompatActivity {

    Bundle bundle;
    public static String makeHttpCall(String url1){
        URL url = null;
        final String ZOMATO_KEY = "51868734cdb381c3dd872167bcd82cb8";

        HttpsURLConnection urlConnection=null;
        try {
            url = new URL(url1);
            urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setRequestProperty("user-key",ZOMATO_KEY);
            urlConnection.connect();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return convertStreamToString(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            urlConnection.disconnect();
        }

        return null;
    }

    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Log.d("result",sb.toString());
        return sb.toString();
    }

    public static ArrayList<ZomatoParameter> parseJsonInBO1(String content){

        ArrayList<ZomatoParameter> restaurantList = new ArrayList<ZomatoParameter>();

        try {
            //JSONObject json = new JSONObject (result);

            //Log.d("result12", content);
            JSONObject jsonObject = new JSONObject(content);
            JSONArray jsonArray = (JSONArray) jsonObject.get("restaurants");
            // Log.d("jsonarraylenght",Integer.toString(jsonArr.length()));
            for(int i=0;i<jsonArray.length();i++) {JSONObject gpObject = jsonArray.getJSONObject(i);
                ZomatoParameter restaurant = new ZomatoParameter();
                //JSONObject k = jsonArray.getJSONObject(i);
                JSONObject p = gpObject.getJSONObject("restaurant");
                /*String name = p.getString("name");
                String feature_image = p.getString("featured_image");
                String thumb = p.getString("thumb");
                JSONObject m = p.getJSONObject("location");
                String locality = m.getString("locality");
                String address = m.getString("address");
                String latitude = m.getString("latitude");
                String longitude = m.getString("longitude");


                ZomatoParameter rest = new ZomatoParameter(name,feature_image);
                rest.setName(name);
                rest.setFeatured_image(feature_image);*/
//                rest.setAddress(address);
//                rest.setLocality(locality);
//                rest.setImageUrl(thumb);
//                rest.setLatitude(latitude);
//                rest.setLongitude(longitude);
//



                if(p.has("name")) {
                    restaurant.setName(p.get("name").toString());
                    Log.i("First 1", String.format("white '%s'", restaurant));
                }
                else{
                    Log.i("First 1", String.format("white '%s'", restaurant));
                }

                //JSONObject location=p.getJSONObject("location");

                if(p.has("location")) {
                    restaurant.setLocality(p.getJSONObject("location").getString("locality").toString());

                    restaurant.setLat(p.getJSONObject("location").getDouble("latitude"));
                    restaurant.setLng(p.getJSONObject("location").getDouble("longitude"));
                    Log.i("First 2", String.format("white '%s'", restaurant));
                }
                if(p.has("cuisines")) {
                    Log.i("First 1", String.format("white '%s'", restaurant));
                    restaurant.setCuisines(p.get("cuisines").toString());
                }
                if(p.has("average_cost_for_two")) {
                    restaurant.setAverage_cost_for_two(Integer.parseInt(p.get("average_cost_for_two").toString()));
                }
                if(p.has("user_rating")) {
                    restaurant.setAggregate_rating(Float.parseFloat(p.getJSONObject("user_rating").get("aggregate_rating").toString()));
                }

                if(p.has("featured_image")) {
                    restaurant.setFeatured_image(p.get("featured_image").toString());
                }

                /*if(p.has("latitude")){
                    restaurant.setLat(p.getJSONObject("location").getDouble("latitude"));
                }

                if(p.has("longitude")){
                    restaurant.setLng(p.getJSONObject("location").getDouble("longitude"));
                }*/
                restaurantList.add(restaurant);
            }

        }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return restaurantList;
    }






   /* Bundle bundle;
    public static String makeHttpCall(String url1){
        URL url = null;
        HttpsURLConnection urlConnection=null;
        try {
            url = new URL(url1);
             urlConnection = (HttpsURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return convertStreamToString(in);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            urlConnection.disconnect();
        }

        return null;

    }
    public static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.d("result",sb.toString());
        return sb.toString();
    }

    public static ArrayList<GoogleParameters> parseJsonInBO(String content){

        ArrayList<GoogleParameters> restaurantList = new ArrayList<GoogleParameters>();

        try {
            //JSONObject json = new JSONObject (result);

            //Log.d("result12", content);
            JSONObject jsonObject = new JSONObject(content);
            JSONArray jsonArray = (JSONArray) jsonObject.get("results");
           // Log.d("jsonarraylenght",Integer.toString(jsonArr.length()));
            for(int i=0;i<jsonArray.length();i++) {
                JSONObject gpObject = jsonArray.getJSONObject(i);
                GoogleParameters restaurant = new GoogleParameters();
                if(gpObject.has("name")) {
                    restaurant.setName(gpObject.get("name").toString());
                }

                if(gpObject.has("rating")) {
                    restaurant.setRating(Float.parseFloat(gpObject.get("rating").toString()));
                }
                else {
                    restaurant.setRating(Float.parseFloat("0"));
                }
                if(gpObject.has("price_level")) {
                    restaurant.setPrice_level(Integer.parseInt((gpObject.get("price_level").toString())));
                }
                if(gpObject.has("vicinity")) {
                    restaurant.setAddress(gpObject.get("vicinity").toString());
                }
                if(gpObject.has("geometry")) {
                    restaurant.setLat(Double.parseDouble(gpObject.getJSONObject("geometry").getJSONObject("location").get("lat").toString()));
                    restaurant.setLng(Double.parseDouble(gpObject.getJSONObject("geometry").getJSONObject("location").get("lng").toString()));
                }

                if(gpObject.has("photos")) {
                    restaurant.setPhoto_reference(gpObject.getJSONArray("photos").getJSONObject(0).getString("photo_reference").toString());
                }
                if(gpObject.has("opening_hours")){
                    restaurant.setOpen_now(gpObject.getJSONObject("opening_hours").getBoolean("open_now"));
                }
//arrayList.add(restaurant);
                restaurantList.add(restaurant);
            }
            *//*bundle = new Bundle();
            bundle.putParcelableArrayList("restaurantList",(ArrayList<GoogleParameters>)restaurantList);*//*
            }
        catch (Exception ex){
            ex.printStackTrace();
        }

        return restaurantList;
    }*//*Intent ResList = new Intent(RestClient.this, MainActivity.class);
        ResList.putExtra("restaurantList", restaurantList);
        startActivity(ResList);*/
}
