package com.createfirst.company.foodpals;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Shilpa on 13-12-2018.
 */

public class PlacePick extends AppCompatActivity {

    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
    static final int PLACE_PICKER_REQUEST =1;
    TextView current_location_header;

    public void selectPlace(Context context) {

        try {
            current_location_header = (TextView) findViewById(com.createfirst.company.foodpals.R.id.subtitle_actionbar);

            startActivityForResult(builder.build((Activity) context), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }


    protected void onActivityResult(int requestCode,int resultCode,Intent data) {
        if(requestCode == PLACE_PICKER_REQUEST)
        {
            if(resultCode==RESULT_OK)
            {
                Place place = PlacePicker.getPlace(this,data);
                String address =String.format("%s",place.getAddress());
                LatLng latlangObj = place.getLatLng();
                Location changed_location = null;
                changed_location.setLatitude(latlangObj.latitude);
                changed_location.setLongitude(latlangObj.longitude);

                current_location_header.setText(address);

                /*if(changed_location!=null && MainActivity.resturant_switch.equals("google")){
                    mainActivity.CallRestClient1().execute(changed_location);
                }
                else if(changed_location!=null && MainActivity.resturant_switch.equals("zomato")){
                    new MainActivity.CallRestClient().execute(changed_location);
                }*/
            }

        }
    }



}
