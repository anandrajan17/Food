package com.createfirst.company.foodpals;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Handler;

import com.facebook.shimmer.ShimmerFrameLayout;

public class splash_screen extends AppCompatActivity {


    private ShimmerFrameLayout app_logo_shimmer;

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.createfirst.company.foodpals.R.layout.activity_splash_screen);

        app_logo_shimmer = findViewById(com.createfirst.company.foodpals.R.id.app_icon_shimmer);
       // app_logo_shimmer.startShimmerAnimation();

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(splash_screen.this, tutorial.class);
                startActivity(i);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }



}

