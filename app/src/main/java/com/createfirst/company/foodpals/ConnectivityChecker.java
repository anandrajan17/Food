package com.createfirst.company.foodpals;

/**
 * Created by anandhaa on 04/06/18.
 */

public class ConnectivityChecker extends Firebase_FoodPals {

    private static ConnectivityChecker mInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
    }

    public static synchronized ConnectivityChecker getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
